// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TDS : ModuleRules
{
	public TDS(ReadOnlyTargetRules Target) : base(Target)
	{
		PrivateDependencyModuleNames.AddRange(new string[] {});
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", 
	        "Niagara", "EnhancedInput", "UMG", "AnimGraphRuntime", "PhysicsCore", "SlateCore", "GameplayTags", "AIModule", "GameplayTasks" });
        
        PublicIncludePaths.AddRange(new string[]
        {
	        "TDS/",
	        "TDS/Public/AI",
	        "TDS/Public/AI/Tasks",
	        "TDS/Public/AI/Services",
	        "TDS/Public/Base",
	        "TDS/Public/Base/Actors",
	        "TDS/Public/Base/Components",
	        "TDS/Public/Base/Effects",
	        "TDS/Public/Base/Items",
	        "TDS/Public/Components",
	        "TDS/Public/Components/PlayerController",
	        "TDS/Public/Components/PlayerCharacter",
	        "TDS/Public/Components/Weapon",
	        "TDS/Public/Core",
	        "TDS/Public/Effects",
	        "TDS/Public/Effects/Buffs",
	        "TDS/Public/Effects/Debuffs",
	        "TDS/Public/Input",
	        "TDS/Public/Interfaces",
	        "TDS/Public/DataAssets",
	        "TDS/Public/DataAssets/Effects",
	        "TDS/Public/DataAssets/Enemies",
	        "TDS/Public/DataAssets/Weapons",
	        "TDS/Public/Libraries",
	        "TDS/Public/Pickups",
	        "TDS/Public/Player",
	        "TDS/Public/Projectiles",
	        "TDS/Public/UI",
	        "TDS/Public/UI/Objects",
	        "TDS/Public/Weapons"
        });
    }
}
