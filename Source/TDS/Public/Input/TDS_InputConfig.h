﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputAction.h"
#include "InputMappingContext.h"
#include "Engine/DataAsset.h"
#include "TDS_InputConfig.generated.h"

UCLASS()
class TDS_API UTDS_InputConfig : public UDataAsset
{
	GENERATED_BODY()

public:
	UTDS_InputConfig();
	
	UPROPERTY(EditDefaultsOnly, Category= "Context Settings")
	UInputMappingContext* IMC_BaseContext;
	UPROPERTY(EditDefaultsOnly, Category= "Context Settings")
	int32 ContextPriority;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_Move;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_SwitchWeapon;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_Fire;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_Reload;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_Aim;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_ZoomIn;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_ZoomOut;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_Interact;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_Inventory;
	UPROPERTY(EditDefaultsOnly, Category= "Input Actions")
	UInputAction* IA_Health;
};
