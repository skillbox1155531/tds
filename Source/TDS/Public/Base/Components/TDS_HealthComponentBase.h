﻿#pragma once

#include "CoreMinimal.h"
#include "TDS_HealthComponentBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAttributeChanged, const int32&, CurrentValue, const float&, Delta);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDS_HealthComponentBase : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTDS_HealthComponentBase();
	void SpawnDamageVisual(const float& Amount, const bool& bIsShielded);

	UFUNCTION(BlueprintCallable)
	virtual float DecreaseHealth(const float& Amount);
	virtual bool IncreaseHealthPoints(const float& Amount, const bool& bIsOverTime = false);
	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth() const;
	UFUNCTION(BlueprintCallable)
	float GetMaxHealth() const;
	UFUNCTION(BlueprintCallable)
	bool GetIsAlive() const;
	
protected:
	virtual void OnRegister() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = Parameters)
	float MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = Parameters)
	float CurrentHealth;

public:
	UPROPERTY(BlueprintAssignable)
	FOnAttributeChanged OnShieldChanged_Delegate;
	UPROPERTY(BlueprintAssignable)
	FOnAttributeChanged OnHealthChanged_Delegate;
};