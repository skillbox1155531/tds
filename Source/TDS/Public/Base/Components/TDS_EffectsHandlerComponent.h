﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "TDS_EnumLibrary.h"
#include "Components/ActorComponent.h"
#include "TDS_EffectsHandlerComponent.generated.h"

class UTDS_BaseEffect;
enum class EEffectType : uint8;
class ATDS_CharacterBase;
struct FEffectInfo;
class ATDS_EffectActorBase;
class UWTDS_EffectsPanel;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDS_EffectsHandlerComponent : public UActorComponent
{
	GENERATED_BODY()
	UTDS_EffectsHandlerComponent();
	
public:
	void InitComponent(const TWeakObjectPtr<UWTDS_EffectsPanel>& Panel);

	//New Logic
	void AddEffect(AActor* Instigator, const FEffectInfo* EffectInfo, TSubclassOf<UTDS_BaseEffect> EffectClass);
	void RestartEffect(AActor* Instigator, UTDS_BaseEffect* Effect);
	void ModifyEffect(AActor* Instigator, UTDS_BaseEffect* Effect);
	void RemoveEffect(UTDS_BaseEffect* Effect);
	void InterruptAllEffects();
	void AddTags(const FGameplayTagContainer& Tags);
	void RemoveTags(const FGameplayTagContainer& Tags);
	bool HasTags(const FGameplayTagContainer& Tags);
	bool HasTag(FGameplayTag Tag);
	void EnableIcon(const FEffectInfo& Info, UTexture2D* Texture);
	UTDS_BaseEffect* FindEffect(const FGuid& ID);
	static TWeakObjectPtr<UTDS_EffectsHandlerComponent> GetEffectsHandlerComponent(AActor* Actor);
	
protected:
	virtual void OnRegister() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
private:
	TWeakObjectPtr<ATDS_CharacterBase> CharacterPtr;
	TWeakObjectPtr<UWTDS_EffectsPanel> EffectsPanel;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Tags", meta=(AllowPrivateAccess="true"))
	TArray<UTDS_BaseEffect*> EffectsArr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Tags", meta=(AllowPrivateAccess="true"))
	FGameplayTagContainer ActiveGameplayTags;
};
