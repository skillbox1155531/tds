﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "TDS_StructLibrary.h"
#include "UObject/Object.h"
#include "TDS_BaseEffect.generated.h"

class UTDS_EffectsHandlerComponent;

UCLASS(Abstract, Blueprintable)
class TDS_API UTDS_BaseEffect : public UObject
{
	GENERATED_BODY()

public:
	UTDS_BaseEffect();
	
	bool CanStart(AActor* Instigator);
	virtual void StartEffect(AActor* Instigator, FEffectInfo EffectInfo);
	virtual void RestartEffect(AActor* Instigator);
	virtual void ModifyEffect(AActor* Instigator);
	virtual void StopEffect(AActor* Instigator);
	virtual void InterruptEffect(AActor* Instigator);
	bool GetIsActive() const;
	bool GetIsAutoStart() const;
	TWeakObjectPtr<UTDS_EffectsHandlerComponent> GetOwningComponent() const;
	virtual UWorld* GetWorld() const override;
	FEffectInfo GetEffectInfo() const;
	UTexture2D* GetIcon() const;
	FGameplayTagContainer GetGrantTags() const;
	FGameplayTagContainer GetBlockTags() const;
protected:
	virtual void OnEffectDataLoaded(const FEffectInfo EffectInfo, FPrimaryAssetId Data, AActor* Instigator);
	virtual void ApplyInstantEffect(AActor* Instigator);
	virtual void ApplyPeriodicEffect(AActor* Instigator);
	void SpawnEffectVFX(AActor* Instigator, FName Socket);
	
private:
	void SetEffectTimers();
	void ClearEffectTimers(AActor* Instigator);
	
protected:
	UPROPERTY()
	AActor* InstigatorPtr;
	UPROPERTY()
	bool bIsActive;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Parameters")
	bool bIsAutoStart;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Parameters")
	FGameplayTagContainer GrantTags;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = "Parameters")
	FGameplayTagContainer BlockTags;
	UPROPERTY()
	FEffectInfo Info;
	UPROPERTY()
	UNiagaraSystem* EffectVFXSystem;
	UPROPERTY()
	UNiagaraComponent* EffectVFX;
	UPROPERTY()
	UTexture2D* EffectIcon;

	FTimerHandle EffectDuration_TimerHandle;
	FTimerHandle EffectRate_TimerHandle;
};