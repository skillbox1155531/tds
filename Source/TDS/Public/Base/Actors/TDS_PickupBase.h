﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NiagaraComponent.h"
#include "TDS_IPickup.h"
#include "TDS_StructLibrary.h"
#include "Components/WidgetComponent.h"
#include "GameFramework/Actor.h"
#include "TDS_PickupBase.generated.h"

class UTDS_BaseEffect;
struct FStreamableManager;
class UWTDS_PickupIcon;
class UWidgetComponent;
class UBoxComponent;
enum class EPickupType : uint8;

UCLASS()
class TDS_API ATDS_PickupBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_PickupBase();

private:
	void SetupComponents();
	void OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh,  TSoftObjectPtr<UNiagaraSystem> NiagaraSystem, TSoftObjectPtr<UTexture2D> Texture2D);
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	
	UFUNCTION()
	virtual void HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	virtual void InitComponents();

protected:
	UPROPERTY(EditAnywhere)
	USceneComponent* Root;
	UPROPERTY(EditAnywhere)
	UBoxComponent* CollisionComponent;
	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* StaticMeshComponent;
	UPROPERTY(EditAnywhere)
	UNiagaraComponent* NiagaraComponent;
	UPROPERTY(EditAnywhere)
	UDecalComponent* DecalComponent;
	UPROPERTY(EditAnywhere)
	UWidgetComponent* IconWidgetComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Data)
	FPickupInfo PickupInfo;
};
