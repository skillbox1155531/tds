﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_DamageVisualBase.generated.h"

class UWidgetComponent;
class UWTDS_DamageVisual;

UCLASS()
class TDS_API ATDS_DamageVisualBase : public AActor
{
	GENERATED_BODY()

private:
	void Deactivate();
	virtual void BeginPlay() override;
	
public:
	ATDS_DamageVisualBase();
	void SetupWidgetComponent();
	UFUNCTION(BlueprintCallable)
	void Activate(const float& Value, const bool& bIsShielded = false);

private:
	UPROPERTY(EditDefaultsOnly)
	UWidgetComponent* DamageWidgetComponent;
	TWeakObjectPtr<UWTDS_DamageVisual> DamageVisualWidget;
};
