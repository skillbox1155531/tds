﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_DelegateLibrary.h"
#include "TDS_StructLibrary.h"
#include "TDS_WeaponData.h"
#include "TDS_ProjectileBase.generated.h"

class UTDS_EffectBase;
class UBoxComponent;
class UNiagaraComponent;
class UAudioComponent;
struct FWeaponFX;

UCLASS()
class TDS_API ATDS_ProjectileBase : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_ProjectileBase();

	virtual void SetupProjectileParameters(const FWeaponParameters& WeaponParameters, const UTDS_WeaponData* WeaponData);
	void Launch(const FVector& ShotDirection);
	void SetActive(const bool& bIsActive);
	void OnEnableEffect(const FEffectInfo& Info, TSubclassOf<UTDS_BaseEffect> Effect);
	void OnDisableEffect();
	
protected:
	virtual void SetupComponents();
	virtual void PostInitializeComponents() override;
	void SetProjectileVariables();
	virtual float TrackProjectile();
	virtual void Activate(const FVector& ShotDirection);
	void Deactivate();
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	void PlayProjectileFX(const TEnumAsByte<EPhysicalSurface>& SurfaceType, const FHitResult& HitResult);

	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UBoxComponent* BoxComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UNiagaraComponent* TrailComponent;
	UPROPERTY(VisibleAnywhere)
	FEffectInfo EffectInfo;
	UPROPERTY(VisibleAnywhere)
	TSubclassOf<UTDS_BaseEffect> EffectClass;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category= "Parameters | General")
	bool bIsHit;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category= "Parameters | General")
	float InitialXVelocity;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category= "Parameters | General")
	float InitialZVelocity;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category= "Parameters | General")
	float Damage;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category= "Parameters | General")
	float MaxRange;

	FVector CachedLaunchPosition;
	FTimerHandle ProjectileTrackerTimerHandle;
	
public:
	FOnExecuteProjectileHitAction OnGetProjectileHitFX;
	FOnExecuteProjectilePoolAction OnDeactivateProjectile;
};
