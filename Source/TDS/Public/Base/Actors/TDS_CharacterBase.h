﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_EnumLibrary.h"
#include "TDS_StructLibrary.h"
#include "GameFramework/Character.h"
#include "TDS_CharacterBase.generated.h"

class UBoxComponent;
class UTDS_EffectsHandlerComponent;
class UWTDS_PlayerHUD;
struct FEffectInfo;
enum class EMovementState : uint8;

UCLASS()
class TDS_API ATDS_CharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	ATDS_CharacterBase();

	EMovementState GetMovementState() const;
	void SetMovementState(const EMovementState& NewMovementState);
	TWeakObjectPtr<UTDS_EffectsHandlerComponent> GetEffectsHandlerComponent() const;
	TWeakObjectPtr<UCharacterMovementComponent> GetCharacterMovementComponent() const;
	void SetHitBoxPhysMaterial(const TWeakObjectPtr<UPhysicalMaterial>& Material);
protected:
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;
	virtual void InitComponents(const TWeakObjectPtr<UWTDS_PlayerHUD>& PlayerHUD);
	
private:
	void SetupCollisionComponents() const;
	void SetupMovementComponent() const;
	void SetupHitBoxComponent();
	void SetupEffectsHandlerComponent();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UBoxComponent* HitBoxComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UTDS_EffectsHandlerComponent* EffectsHandlerComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Movement Parameters")
	EMovementState CurrentMovementState;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category = "Movement Parameters")
	FMovementParameters MovementParameters;
};
