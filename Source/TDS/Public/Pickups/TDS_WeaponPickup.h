﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_PickupBase.h"
#include "TDS_WeaponPickup.generated.h"

UCLASS()
class TDS_API ATDS_WeaponPickup : public ATDS_PickupBase
{
	GENERATED_BODY()

public:
	ATDS_WeaponPickup();

protected:
	virtual void PostInitializeComponents() override;
	virtual void InitComponents() override;
	virtual void HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	void OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh);

private:
	FWeaponInfo WeaponPickupInfo;
};
