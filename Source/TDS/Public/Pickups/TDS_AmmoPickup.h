﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_PickupBase.h"
#include "TDS_AmmoPickup.generated.h"

UCLASS()
class TDS_API ATDS_AmmoPickup : public ATDS_PickupBase
{
	GENERATED_BODY()

public:
	ATDS_AmmoPickup();
	void InitComponents(FAmmoInfo* AmmoInfo, TSoftObjectPtr<UStaticMesh> PickupMesh, TSoftObjectPtr<UMaterialInstance> PickupDecal);
	
protected:
	virtual void PostInitializeComponents() override;
	virtual void InitComponents() override;
	virtual void HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	void OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh);
	void OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh, TSoftObjectPtr<UMaterialInstance> Decal);

private:
	UPROPERTY(VisibleAnywhere)
	FAmmoInfo AmmoPickupInfo;
};
