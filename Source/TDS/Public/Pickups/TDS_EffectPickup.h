﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_PickupBase.h"
#include "TDS_EffectPickup.generated.h"

UCLASS()
class TDS_API ATDS_EffectPickup : public ATDS_PickupBase
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_EffectPickup();

protected:
	virtual void PostInitializeComponents() override;
	virtual void InitComponents() override;
	virtual void HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	void OnDataLoaded(TSoftObjectPtr<UNiagaraSystem> NiagaraSystem);

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Data)
	TSubclassOf<UTDS_BaseEffect> BaseEffectClass;
	FEffectInfo EffectInfo;
};
