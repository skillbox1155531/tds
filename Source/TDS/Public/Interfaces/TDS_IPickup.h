﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_IPickup.generated.h"

class ATDS_PickupBase;
enum class EPickupType : uint8;

UINTERFACE()
class UTDS_Pickup : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API ITDS_Pickup
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	virtual TWeakObjectPtr<ATDS_PickupBase> StartInteract() = 0;
	virtual bool Interact(const TWeakObjectPtr<AActor>& Actor) = 0;
	virtual void EndInteract() = 0;
	virtual EPickupType GetPickupType() const = 0;
};
