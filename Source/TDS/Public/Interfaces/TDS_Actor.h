﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS_Actor.generated.h"
enum class EMovementState : uint8;
// This class does not need to be modified.
UINTERFACE()
class UTDS_Actor : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API ITDS_Actor
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void SetActorMovementStateInt(const EMovementState& State);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	FVector GetActorLocationInt();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	bool GetIsAliveInt();
};
