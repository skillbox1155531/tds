﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS_EnemyCombat.generated.h"

// This class does not need to be modified.
UINTERFACE()
class UTDS_EnemyCombat : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TDS_API ITDS_EnemyCombat
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void StartAttackTarget(AActor* TargetActor);
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void InterruptAttackTarget();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void StopAttackTarget(UAnimMontage* Montage, bool bInterrupted);
};
