﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_ProjectileBase.h"
#include "TDS_StructLibrary.h"
#include "TDS_WeaponData.h"
#include "GameFramework/Actor.h"
#include "TDS_WeaponBase.generated.h"

class ATDS_EffectActorBase;
class UWTDS_WeaponPanel;
class UTDS_ProjectilePoolComponent;

UCLASS()
class TDS_API ATDS_WeaponBase : public AActor
{
	GENERATED_BODY()

public:
	ATDS_WeaponBase();

	void InitWeapon(const FWeaponInfo& WeaponInfo, UTDS_WeaponData* WeaponData);
	void ResetWeapon();
	void InitWeaponData(const UTDS_WeaponData* WeaponData);
	void HandleStartFire(const bool& bFire);
	void HandleStopFire();
	void HandleReload();
	void AddAmmoToReserve(const int32& AmmoAmount);
	void AddAmmoToClip(const int32& AmmoAmount);
	
	TWeakObjectPtr<UAnimMontage> GetFireMontage() const;
	TWeakObjectPtr<UAnimMontage> GetReloadMontage() const;
	FWeaponParameters GetWeaponParameters() const;
	bool GetIsReloading() const;
	float GetReloadTime() const;
	FGuid GetID() const;
	TWeakObjectPtr<UTDS_ProjectilePoolComponent> GetProjectilePoolComponent() const;
private:
	void SetupComponents();
	void SetDefaultWeaponVariables();
	void SetWeaponParameters(const FWeaponInfo& WeaponInfo, const UTDS_WeaponData* WeaponData);
	void SetAutoFireTimer();
	void SetSingleFireTimer();
	void SetSingleRoundFireTimer();
	void ExecuteFire();
	bool LaunchProjectile(TWeakObjectPtr<ATDS_ProjectileBase> Projectile);
	void ExecuteReload();
	void AddDispersion();
	void ReduceDispersion();
	void SetDispersionReductionTimer();
	void PlayFX();
	void LaunchStaticMesh(const TWeakObjectPtr<UStaticMesh>& StaticMesh, const FName& Socket, const FName& CollisionProfile);
	void SetProjectileHitFX(const TEnumAsByte<EPhysicalSurface>& Surface, FProjectileHitFX& HitFX);
	bool GetCanFire() const;

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	USceneComponent* Root;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UStaticMeshComponent* StaticMeshComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UNiagaraComponent* MuzzleFlashComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UAudioComponent* AudioComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UTDS_ProjectilePoolComponent* ProjectilePoolComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | Conditions")
	bool bIsReadyToFire;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | Conditions")
	bool bIsReloading;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | General")
	FGuid ID;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | General")
	FWeaponParameters WeaponParameters;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | General")
	FWeaponDispersionParameters WeaponDispersionParameters;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | General")
	float ReloadTime;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | Dispersion")
	float CurrentDispersion;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | FX")
	TWeakObjectPtr<UStaticMesh> ShellMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | FX")
	TWeakObjectPtr<UStaticMesh> ClipMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | Animation")
	TWeakObjectPtr<UAnimMontage> FireMontage;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | Animation")
	TWeakObjectPtr<UAnimMontage> ReloadMontage;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = "Parameters | Animation")
	TMap<TEnumAsByte<EPhysicalSurface>, FProjectileHitFX> ProjectileHitFXDic;
	TWeakObjectPtr<ATDS_EffectActorBase> EffectActor;
	FTimerHandle FireTimerHandle;
	FTimerHandle ReloadTimerHandle;
	FTimerHandle DispersionFallTimerHandle;

public:
	FOnExecuteWeapontAction OnStartFireWeapon;
	FOnExecuteWeapontAction OnStopFireWeapon;
	FOnExecuteWeapontAction OnStartReloadWeapon;
	FOnExecuteWeapontAction OnStopReloadWeapon;
	FOnExecuteWeaponUIAction OnUpdateWeaponInfo;
};