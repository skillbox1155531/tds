﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_ProjectileBase.h"
#include "TDS_ProjectileGrenade.generated.h"

UCLASS()
class TDS_API ATDS_ProjectileGrenade : public ATDS_ProjectileBase
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_ProjectileGrenade();

protected:
	virtual void SetupComponents() override;
	virtual void SetupProjectileParameters(const FWeaponParameters& WeaponParameters, const UTDS_WeaponData* WeaponData) override;
	virtual float TrackProjectile() override;
	void ApplyRadialEffect(AActor* OtherActor);
	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float MinDamage;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float DamageFallOff;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float DamageInnerRadius;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float DamageOuterRadius;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TEnumAsByte<ECollisionChannel> QueryChannel;
};
