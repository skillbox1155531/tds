﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_ProjectileBase.h"
#include "TDS_ProjectileBullet.generated.h"

UCLASS()
class TDS_API ATDS_ProjectileBullet : public ATDS_ProjectileBase
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_ProjectileBullet();
	
private:
	virtual void SetupComponents() override;
	virtual void Activate(const FVector& ShotDirection) override;
	virtual float TrackProjectile() override;
	UFUNCTION()
	virtual void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;
};
