﻿#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "NiagaraSystem.h"
#include "Sound/SoundCue.h"
#include "Animation/AnimMontage.h"
#include "TDS_EnumLibrary.h"
#include "TDS_StructLibrary.generated.h"

class UTDS_BaseEffect;
class UNiagaraSystem;
struct FDataTableRowHandle;

USTRUCT(BlueprintType)
struct FMovementParameters
{
	GENERATED_BODY()
	
	FMovementParameters() : WalkSpeed(400.f), SprintSpeed(700.f), SlowedSpeed(175.f), IdleSpeed(0.f), AimSpeed(250.f)
	{}

	 float GetWalkSpeed() const { return WalkSpeed; }
	 float GetSprintSpeed() const { return SprintSpeed; }
	 float GetSlowedSpeed() const { return SlowedSpeed; }
	 float GetIdleSpeed() const { return IdleSpeed; }
	 float GetAimSpeed() const { return AimSpeed; }
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float WalkSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float SprintSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float SlowedSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float IdleSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float AimSpeed;
};

USTRUCT(BlueprintType)
struct FWeaponDispersionParameters
{
	GENERATED_BODY()

	FWeaponDispersionParameters() : MinDispersion(0.f), MaxDispersion(0.f), DispersionGrowCoefficient(0.f),
	                                DispersionFallCoefficient(0.f) {}

	 float GetMinDispersion() const { return MinDispersion; }
	 float GetMaxDispersion() const { return MaxDispersion; }
	 float GetDispersionGrowCoefficient() const { return DispersionGrowCoefficient; }
	 float GetDispersionFallCoefficient() const { return DispersionFallCoefficient; }
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float MinDispersion;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float MaxDispersion;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float DispersionGrowCoefficient;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float DispersionFallCoefficient;
};

USTRUCT(BlueprintType)
struct FWeaponParameters
{
	GENERATED_BODY()
	
	FWeaponParameters() : WeaponFireType(EWeaponFireType::WFT_None), WeaponDamageType(EWeaponDamageType::WDT_None), WeaponAmmoType(EWeaponAmmoType::WAT_None),
	                      RateOfFire(0.f), RoundsPerShot(0), MaxRoundsInClip(0), CurrentRoundsInClip(0),
							MaxRoundsInReserve(0), CurrentRoundsInReserve(0),
	                      Damage(0.f), MinDamage(0), DamageFallOff(0), DamageInnerRadius(0), DamageOuterRadius(0),
	                      MaxFireDistance(0.f), InitialProjectileXVelocity(0.f), InitialProjectileZVelocity(0)
	{
	}

	 EWeaponFireType GetWeaponFireType() const { return WeaponFireType; }
	 EWeaponAmmoType GetWeaponAmmoType() const { return WeaponAmmoType; }
	 float GetRateOfFire() const { return RateOfFire; }
	 int32 GetRoundsPerShot() const { return RoundsPerShot; }
	 int32 GetMaxRoundsInClip() const { return MaxRoundsInClip; }
	 int32 GetCurrentRoundsInClip() const { return CurrentRoundsInClip; }
	 int32 GetMaxRoundsInReserve() const { return MaxRoundsInReserve; }
	 int32 GetCurrentRoundsInReserve() const { return CurrentRoundsInReserve; }
	 float GetDamage() const { return Damage; }
	 float GetMinDamage() const { return MinDamage; }
	 float GetDamageFallOff() const { return DamageFallOff; }
	 float GetDamageInnerRadius() const { return DamageInnerRadius; }
	 float GetDamageOuterRadius() const { return DamageOuterRadius; }
	 float GetMaxFireDistance() const { return MaxFireDistance; }
	 float GetInitialProjectileXVelocity() const { return InitialProjectileXVelocity; }
	 float GetInitialProjectileZVelocity() const { return InitialProjectileZVelocity; }

	 void SetCurrentRoundsInClip(const float& Value) { CurrentRoundsInClip = Value; }
	 void SetCurrentRoundsInReserve(const float& Value) { CurrentRoundsInReserve = Value; }
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	EWeaponFireType WeaponFireType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	EWeaponDamageType WeaponDamageType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	EWeaponAmmoType WeaponAmmoType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float RateOfFire;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	int32 RoundsPerShot;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	int32 MaxRoundsInClip;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	int32 CurrentRoundsInClip;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	int32 MaxRoundsInReserve;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	int32 CurrentRoundsInReserve;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true", EditCondition = "WeaponDamageType == EWeaponDamageType::WDT_Radial", EditConditionHides), Category=Parameters)
	float MinDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true", EditCondition = "WeaponDamageType == EWeaponDamageType::WDT_Radial", EditConditionHides), Category=Parameters)
	float DamageFallOff;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true", EditCondition = "WeaponDamageType == EWeaponDamageType::WDT_Radial", EditConditionHides), Category=Parameters)
	float DamageInnerRadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true", EditCondition = "WeaponDamageType == EWeaponDamageType::WDT_Radial", EditConditionHides), Category=Parameters)
	float DamageOuterRadius;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float MaxFireDistance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float InitialProjectileXVelocity;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float InitialProjectileZVelocity;
};

USTRUCT(BlueprintType)
struct FProjectileHitFX
{
	GENERATED_BODY()
	FProjectileHitFX() : HitSoundFX(nullptr), HitVisualFX(nullptr), HitDecalFX(nullptr) {}

	TWeakObjectPtr<USoundCue> GetHitSoundFX() const { return TWeakObjectPtr<USoundCue>(HitSoundFX); }
	TWeakObjectPtr<UNiagaraSystem> GetHitVisualFX() const { return TWeakObjectPtr<UNiagaraSystem>(HitVisualFX); }
	TWeakObjectPtr<UMaterialInstance> GetHitDecalFX() const { return TWeakObjectPtr<UMaterialInstance>(HitDecalFX); }
	TArray<UMaterialInstance*> GetHitDecalFXArr() const { return HitDecalFXArr; } 

	void SetHitSoundFX(const TWeakObjectPtr<USoundCue>& SoundFX) { HitSoundFX = SoundFX.Get(); }
	void SetHitVisualFX(const TWeakObjectPtr<UNiagaraSystem>& VisualFX) { HitVisualFX = VisualFX.Get(); }
	void SetHitDecalFX(const TWeakObjectPtr<UMaterialInstance>& DecalFX) { HitDecalFX = DecalFX.Get(); }

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Sound")
	USoundCue* HitSoundFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Niagara")
	UNiagaraSystem* HitVisualFX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category="Parameters | Decal")
	UMaterialInstance* HitDecalFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Decal")
	TArray<UMaterialInstance*> HitDecalFXArr;
};

USTRUCT(BlueprintType)
struct FWeaponFX
{
	GENERATED_BODY()

	FWeaponFX() : WeaponStaticMesh(nullptr), WeaponShellMeshFX(nullptr), WeaponClipMeshFX(nullptr),
	              WeaponMuzzleFlashFX(nullptr), ProjectileTrailFX(nullptr), WeaponFireSoundFX(nullptr){}

	const TSoftObjectPtr<UStaticMesh>& GetWeaponStaticMesh() const { return WeaponStaticMesh; }
	TWeakObjectPtr<UStaticMesh> GetWeaponShellMeshFX() const { return TWeakObjectPtr<UStaticMesh>(WeaponShellMeshFX); }
	TWeakObjectPtr<UStaticMesh> GetWeaponClipMeshFX() const { return TWeakObjectPtr<UStaticMesh>(WeaponClipMeshFX); }
	TWeakObjectPtr<UNiagaraSystem> GetWeaponMuzzleFlashFX() const { return TWeakObjectPtr<UNiagaraSystem>(WeaponMuzzleFlashFX); }
	TWeakObjectPtr<UNiagaraSystem> GetProjectileTrailFX() const { return TWeakObjectPtr<UNiagaraSystem>(ProjectileTrailFX); }
	TWeakObjectPtr<USoundCue> GetWeaponFireSoundFX() const { return TWeakObjectPtr<USoundCue>(WeaponFireSoundFX); }
	TMap<TEnumAsByte<EPhysicalSurface>, FProjectileHitFX> GetProjectileHitFXDic() const { return ProjectileHitFXDic; }
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Mesh")
	TSoftObjectPtr<UStaticMesh> WeaponStaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Mesh")
	UStaticMesh* WeaponShellMeshFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Mesh")
	UStaticMesh* WeaponClipMeshFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Niagara")
	UNiagaraSystem* WeaponMuzzleFlashFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Niagara")
	UNiagaraSystem* ProjectileTrailFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Sound")
	USoundCue* WeaponFireSoundFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Sound")
	TMap<TEnumAsByte<EPhysicalSurface>, FProjectileHitFX> ProjectileHitFXDic;
};

USTRUCT(BlueprintType)
struct FWeaponAnimations
{
	GENERATED_BODY()

	FWeaponAnimations() : WeaponFireAnim(nullptr), WeaponReloadAnim(nullptr) {}

	TWeakObjectPtr<UAnimMontage> GetWeaponFireAnim() const { return TWeakObjectPtr<UAnimMontage>(WeaponFireAnim); }
	TWeakObjectPtr<UAnimMontage> GetWeaponReloadAnim() const { return TWeakObjectPtr<UAnimMontage>(WeaponReloadAnim); }

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Animation")
	UAnimMontage* WeaponFireAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters | Animation")
	UAnimMontage* WeaponReloadAnim;
};

USTRUCT(BlueprintType)
struct FWeaponUI
{
	GENERATED_BODY()

	FWeaponUI() : WeaponThumbnail(nullptr), AmmoThumbnail(nullptr) {}

	TWeakObjectPtr<UTexture2D> GetWeaponThumbnail() const { return TWeakObjectPtr<UTexture2D>(WeaponThumbnail); }
	TWeakObjectPtr<UTexture2D> GetAmmoThumbnail() const { return TWeakObjectPtr<UTexture2D>(AmmoThumbnail); }

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters")
	UTexture2D* WeaponThumbnail;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Parameters")
	UTexture2D* AmmoThumbnail;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	FWeaponInfo() : ID(FGuid::NewGuid()), WeaponParameters(FWeaponParameters()), WeaponDispersionParameters(FWeaponDispersionParameters()) {}

	FGuid GetID() const { return ID; }
	FPrimaryAssetId GetWeaponData() const { return WeaponData; }
	FWeaponParameters GetWeaponParameters() const { return WeaponParameters; }
	FWeaponDispersionParameters GetWeaponDispersionParameters() const { return WeaponDispersionParameters; }

	friend bool operator==(const FWeaponInfo& first, const FWeaponInfo& second)
	{
		return (first.GetID() == second.GetID());
	}
	
	friend uint32 GetTypeHash (const FWeaponInfo& Other)
	{
		return GetTypeHash(Other.GetID());
	}
	
	//void SetID(const int32& NewID) { ID = NewID; }
	void SetWeaponParameters(const FWeaponParameters& NewWeaponParameters) { WeaponParameters = NewWeaponParameters; }
	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FGuid ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FPrimaryAssetId WeaponData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FWeaponParameters WeaponParameters;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FWeaponDispersionParameters WeaponDispersionParameters;
};

USTRUCT(BlueprintType)
struct FAmmoInfo : public FTableRowBase
{
	GENERATED_BODY()

	FAmmoInfo() : AmmoType(EWeaponAmmoType::WAT_None), AmmoBoxMesh(nullptr), AmmoIconTexture(nullptr), AmmoAmount(0) {}

	EWeaponAmmoType GetAmmoType() const { return AmmoType; }
	int32 GetAmmoAmount() const { return AmmoAmount; }
	TSoftObjectPtr<UStaticMesh> GetAmmoBoxMesh() const { return AmmoBoxMesh; }
	TSoftObjectPtr<UTexture2D> GetAmmoIconTexture() const { return AmmoIconTexture; }
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	EWeaponAmmoType AmmoType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSoftObjectPtr<UStaticMesh> AmmoBoxMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSoftObjectPtr<UTexture2D> AmmoIconTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	int32 AmmoAmount;
};

USTRUCT(BlueprintType)
struct FHealthInfo : public FTableRowBase
{
	GENERATED_BODY()

	FHealthInfo() : HealthType(EHealthType::HT_None), HealthBoxMesh(nullptr), HealthPickupEmitter(nullptr), HealthIconTexture(nullptr), Amount(0) {}

	EHealthType GetHealthType() const { return HealthType; }
	int32 GetHealthAmount() const { return Amount; }
	TSoftObjectPtr<UStaticMesh> GetHealthBoxMesh() const { return HealthBoxMesh; }
	TSoftObjectPtr<UNiagaraSystem> GetHealthPickupEmitter() const { return HealthPickupEmitter; }
	TSoftObjectPtr<UTexture2D> GetHealthIconTexture() const { return HealthIconTexture; }
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	EHealthType HealthType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSoftObjectPtr<UStaticMesh> HealthBoxMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSoftObjectPtr<UNiagaraSystem> HealthPickupEmitter;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSoftObjectPtr<UTexture2D> HealthIconTexture;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	int32 Amount;
};

USTRUCT()
struct FEffectInfo : public FTableRowBase
{
	GENERATED_BODY()
	
public:
	FEffectInfo() : ID(FGuid::NewGuid()), Effect(EEffect::E_Buff), bIsStackable(false), EffectPickupEmitter(nullptr),
	                EffectDuration(0.f), EffectRate(0.f), EffectValue(0.f), CurrentStacks(1), MaxStacks(0),
	                EffectStackMultiplier(0)
	{
	}

	explicit FEffectInfo(const EEffect Value) : ID(FGuid::NewGuid()), Effect(Value),
	                                            bIsStackable(false),
	                                            EffectPickupEmitter(nullptr),
	                                            EffectDuration(0.f), EffectRate(0.f), EffectValue(0.f),
	                                            CurrentStacks(1), MaxStacks(0),
	                                            EffectStackMultiplier(0)
	{
	}

	FGuid GetID() const { return ID; }
	EEffect GetEffect() const { return Effect; }
	bool GetIsStackable() const { return bIsStackable; } 
	TSoftObjectPtr<UNiagaraSystem> GetEffectPickupEmitter() const { return EffectPickupEmitter; }
	FPrimaryAssetId GetEffectData() const { return EffectData; }
	float GetEffectDuration() const { return EffectDuration; }
	float GetEffectRate() const { return EffectRate; }
	float GetEffectValue() const { return EffectValue; }
	int32 GetCurrentStacks() const { return CurrentStacks; }
	int32 GetMaxStacks() const { return MaxStacks; }
	float GetEffectStackMultiplier() const { return EffectStackMultiplier; }

	void SetCurrentStacks(const int32& Value) { CurrentStacks = Value; }
	void SetEffectValue(const float& Value) { EffectValue = Value; }
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FGuid ID;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	EEffect Effect;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	bool bIsStackable;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSoftObjectPtr<UNiagaraSystem> EffectPickupEmitter;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FPrimaryAssetId EffectData;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float EffectDuration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true", EditConditionHides), Category=Parameters)
	float EffectRate;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float EffectValue;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	int32 CurrentStacks;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true", EditCondition = "bIsStackable == true", EditConditionHides), Category=Parameters)
	int32 MaxStacks;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true", EditCondition = "bIsStackable == true", EditConditionHides), Category=Parameters)
	float EffectStackMultiplier;
};

USTRUCT(BlueprintType)
struct FBuffInfo : public FEffectInfo
{
	GENERATED_BODY()
	FBuffInfo() : FEffectInfo(EEffect::E_Buff) {}
};

USTRUCT(BlueprintType)
struct FDebuffInfo : public FEffectInfo
{
	GENERATED_BODY()
	FDebuffInfo() : FEffectInfo(EEffect::E_Debuff) {}
};

USTRUCT(BlueprintType)
struct FPickupInfo
{
	GENERATED_BODY()

	FDataTableRowHandle GetInfo() const { return Info; }
	TSoftObjectPtr<UStaticMesh> GetPickupMesh() const { return PickupMesh; }
	TSoftObjectPtr<UMaterialInstance> GetPickupDecal() const { return PickupDecal; }
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FDataTableRowHandle Info;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSoftObjectPtr<UStaticMesh> PickupMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
    TSoftObjectPtr<UMaterialInstance> PickupDecal;
};

USTRUCT(BlueprintType)
struct FWeaponEffectInfo : public FTableRowBase
{
	GENERATED_BODY()
	FWeaponEffectInfo() : ID(FGuid::NewGuid()), EffectDuration(0.f) {}
	FGuid GetID() const { return ID; };
	FDataTableRowHandle GetInfo() const { return EffectInfo; }
	TSubclassOf<UTDS_BaseEffect> GetEffectClass() const { return BaseEffectClass; }
	float GetEffectDuration() const { return EffectDuration; }
	TSoftObjectPtr<UTexture2D> GetEffectIcon() const { return EffectIcon; }
	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FGuid ID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	FDataTableRowHandle EffectInfo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSubclassOf<UTDS_BaseEffect> BaseEffectClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	float EffectDuration;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category=Parameters)
	TSoftObjectPtr<UTexture2D> EffectIcon;
};
