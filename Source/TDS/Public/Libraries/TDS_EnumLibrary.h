﻿#pragma once

#include "CoreMinimal.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	MS_Idle UMETA(DisplayName = "Idle"),
	MS_Walk UMETA(DisplayName = "Walk"),
	MS_Sprint UMETA(DisplayName = "Sprint"),
	MS_Slowed UMETA(DisplayName = "Slowed"),
	MS_Stunned UMETA(DisplayName = "Stunned"),
	MS_Aim UMETA(DisplayName = "Aim"),
	MS_None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EWeaponFireType : uint8
{
	WFT_Auto UMETA(DisplayName = "Auto"),
	WFT_Single UMETA(DisplayName = "Single"),
	WFT_SingleRound UMETA(DisplayName = "SingleRound"),
	WFT_None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EWeaponDamageType : uint8
{
	WDT_Point UMETA(DisplayName = "Point"),
	WDT_Radial UMETA(DisplayName = "Radial"),
	WDT_None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EWeaponAmmoType : uint8
{
	WAT_Primary UMETA(DisplayName = "Primary"),
	WAT_Special UMETA(DisplayName = "Special"),
	WAT_Heavy UMETA(DisplayName = "Heavy"),
	WAT_None UMETA(DisplayName = "None")
};


UENUM(BlueprintType)
enum class EHealthType : uint8
{
	HT_Health UMETA(DisplayName = "Health"),
	HT_Shield UMETA(DisplayName = "Shield"),
	HT_None UMETA(DisplayName = "None")
};

UENUM(BlueprintType)
enum class EEffect : uint8
{
	E_Buff UMETA(DisplayName = "Buff"),
	E_Debuff UMETA(DisplayName = "Debuff"),
	E_None UMETA(DisplayName = "None")
};