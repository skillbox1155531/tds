﻿#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "TDS_WeaponData.h"

class UTDS_BaseEffect;
struct FWeaponInfo;
struct FEffectInfo;
class UWTDS_CustomButton;
class ATDS_ProjectileBase;
class UAnimMontage;
class UNiagaraSystem;
class USoundCue;
struct FProjectileHitFX;
struct FWeaponParameters;

//Input
DECLARE_DELEGATE_OneParam(FOnExecuteInputAction, const FInputActionValue&);
DECLARE_DELEGATE_OneParam(FOnChangeInputMode, const bool&);

//Weapon
DECLARE_MULTICAST_DELEGATE_OneParam(FOnExecuteWeapontAction, const TWeakObjectPtr<UAnimMontage>&);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnExecuteWeaponUIAction, const FGuid&, const FWeaponParameters&);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnExecuteProjectilePoolAction, const TWeakObjectPtr<ATDS_ProjectileBase>);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnExecuteProjectileHitAction, const TEnumAsByte<EPhysicalSurface>&, FProjectileHitFX&);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnEnableEffect, const FEffectInfo&, TSubclassOf<UTDS_BaseEffect>);
DECLARE_MULTICAST_DELEGATE(FOnDisableEffect);

//Effects
DECLARE_DELEGATE(FOnEffectFinished);
DECLARE_MULTICAST_DELEGATE_OneParam(FOnHandleEffectRemoval, const FGuid&);

//Global


//UI
DECLARE_MULTICAST_DELEGATE_OneParam(FOnUIButtonClicked, const TWeakObjectPtr<UWTDS_CustomButton>&);
typedef TTuple<FWeaponInfo, UTDS_WeaponData*> FWeaponData;
typedef TTuple<FWeaponData, FWeaponData> FWeaponsInfo;
DECLARE_DELEGATE_OneParam(FOnWeaponsInfoSwapCompleted, const FWeaponsInfo&);
DECLARE_DELEGATE(FOnWeaponsInfoSwapCanceled);
DECLARE_DELEGATE(FOnDamageVisualCompleted);
