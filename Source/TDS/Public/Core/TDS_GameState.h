﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "TDS_GameState.generated.h"

class ATDS_PlayerCharacter;

DECLARE_DELEGATE_TwoParams(FOnEnemyKilled, const FGuid&, const int32&);
DECLARE_DELEGATE_TwoParams(FOnPlayerKilled, AActor*, APlayerController*);
DECLARE_DELEGATE(FOnGameOver);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnGamePhaseChanged, int32, NewPhase);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnBossPhaseStarted);

USTRUCT()
struct FGamePhase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	int32 PhaseIndex;
	UPROPERTY(EditAnywhere)
	int32 Score;
};

UCLASS()
class TDS_API ATDS_GameState : public AGameState
{
	GENERATED_BODY()

public:
	void IncreasePlayerScore(const AActor* Enemy, AActor* Player);
	void HandleRespawnPlayer(AActor* Player);
	UFUNCTION(BlueprintCallable)
	void OnActorKilled(AActor* Victim, AActor* Killer);
	UFUNCTION(BlueprintCallable)
	TArray<APawn*> GetPlayerCharacters();
	void SetBossSpawnDelay(const int32& NewBossSpawnDelay);
protected:
	virtual void AddPlayerState(APlayerState* PlayerState) override;
	virtual void RemovePlayerState(APlayerState* PlayerState) override;
	virtual void BeginPlay() override;

private:
	UFUNCTION()
	void OnNewPawnSetToPlayerState(APlayerState* Player, APawn* NewPawn, APawn* OldPawn);
	void HandleGamePhase(const int32& PhaseIndex, const int32& CurrentScore);
private:
	UPROPERTY(EditDefaultsOnly)
	UCurveFloat* EnemiesSpawnWeightCurve;
	UPROPERTY()
	TArray<APawn*> PlayerPawnsArr;
	UPROPERTY(EditAnywhere)
	TArray<FGamePhase> LevelPhasesArr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	int32 CurrentPhaseIndex;
	bool bIsBossPhaseStarted;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	float BossSpawnDelay;
	
public:
	FOnEnemyKilled OnEnemyKilled_Delegate;
	FOnPlayerKilled OnPlayerKilled_Delegate;
	FOnGameOver OnGameOver_Delegate;
	UPROPERTY(BlueprintAssignable)
	FOnGamePhaseChanged OnGamePhaseChanged_Delegate;
	UPROPERTY(BlueprintAssignable)
	FOnBossPhaseStarted OnBossPhaseStarted_Delegate;
};