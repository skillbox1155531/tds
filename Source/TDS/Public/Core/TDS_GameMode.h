﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "WTDS_RespawnCountDown.h"
#include "GameFramework/GameMode.h"
#include "TDS_GameMode.generated.h"

class UEnvQueryInstanceBlueprintWrapper;

namespace EEnvQueryStatus
{
	enum Type : int;
}

class ATDS_EnemyCharacter;
class UEnvQuery;
class ATDS_PlayerStart;

USTRUCT(BlueprintType)
struct FEnemyInfo : public FTableRowBase
{
	GENERATED_BODY()

	FEnemyInfo(): PhaseIndex(0), bIsBoss(false), Weight(0), KillPoints(0) {}

public:
	UPROPERTY(EditDefaultsOnly)
	int32 PhaseIndex;
	UPROPERTY(EditDefaultsOnly)
	bool bIsBoss;
	UPROPERTY(EditDefaultsOnly)
	FPrimaryAssetId EnemyData;
	UPROPERTY(EditDefaultsOnly)
	int32 Weight;
	UPROPERTY(EditDefaultsOnly)
	int32 KillPoints;
};

USTRUCT(BlueprintType)
struct FPhaseInfo: public FTableRowBase
{
	GENERATED_BODY()

	FPhaseInfo() : PhaseIndex(-1) {}

public:
	UPROPERTY(EditDefaultsOnly)
	int32 PhaseIndex;
	UPROPERTY(EditDefaultsOnly)
	TArray<FDataTableRowHandle> Enemies;
};

UCLASS()
class TDS_API ATDS_GameMode : public AGameMode
{
	GENERATED_BODY()

	ATDS_GameMode();

public:
	void RespawnPlayer(AActor* Player, APlayerController* PlayerController);
	void GameOver();
	void CreateGameOverMenu();
	void SetCurrentProgressIndex(const int32& Index);
	void OnEnemyBeingKilled(const FGuid& ID, const int32& NewEnemiesSpawnMaxWeight);
	void OnEnemiesSpawnMaxWeightUpdated(const int32& NewWeight);
	int32 GetCurrentProgressIndex() const;
	TWeakObjectPtr<ATDS_PlayerStart> GetCheckpoint() const;
	UFUNCTION()
	void SetCurrentPhase(int32 NewPhase);
protected:
	virtual void HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer) override;
	virtual void StartPlay() override;
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	
private:
	bool InitializeEnemiesData();
	void SetEnemySpawnTimer();
	void SpawnEnemies();
	UFUNCTION()
	void SetSpawnBossTimer();
	void SpawnBossTimerElapsed();
	UFUNCTION()
	void OnEnemyQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus);
	UFUNCTION()
	void OnBossQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus);
	void OnEnemiesDataLoaded(TArray<FPrimaryAssetId> EnemyAssetIDs, TArray<FVector> LocationsArr);
	void OnBossDataLoaded(FPrimaryAssetId Id, TArray<FVector> Locations);

	bool DespawnNonBossEnemies();
	void CreateWinMenu();
	FEnemyInfo GetEnemyInfo(FPrimaryAssetId AssetID);
	UWTDS_RespawnCountDown* SetupRespawnCountDownTimer();
private:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> GameOverMenuClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> WinMenuClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> RespawnCountDownClass;
	UPROPERTY(EditDefaultsOnly)
	float RespawnDelay;
	UPROPERTY(EditDefaultsOnly, Category=AI)
	UEnvQuery* SpawnEnemyQuery;
	UPROPERTY(EditDefaultsOnly, Category=AI)
	UDataTable* EnemiesDataTable;
	UPROPERTY(EditDefaultsOnly, Category=AI)
	int32 CurrentEnemiesWeight;
	UPROPERTY(EditDefaultsOnly, Category=AI)
	int32 MaxEnemiesWeight;
	UPROPERTY(VisibleAnywhere, Category=AI)
	TArray<FEnemyInfo> EnemiesInfoArr;
	UPROPERTY(EditDefaultsOnly, Category=AI)
	FDataTableRowHandle BossInfo;
	UPROPERTY(EditDefaultsOnly, Category=AI)
	float BossSpawnDelay;
	UPROPERTY(VisibleAnywhere, Category=AI)
	bool bIsBossPhase;
	UPROPERTY(VisibleAnywhere, Category=AI)
	TMap<FGuid, ATDS_EnemyCharacter*> SpawnedEnemiesDic;
	UPROPERTY()
	int32 CurrentCheckpointIndex;
	UPROPERTY()
	TMap<int32, TWeakObjectPtr<ATDS_PlayerStart>> CheckpointsArr;
	FTimerHandle EnemySpawn_TimerHandle;
	FTimerHandle BossSpawn_TimerHandle;
	FTimerHandle WinMenu_TimerHandle;
	UPROPERTY(VisibleAnywhere)
	int32 CurrentPhaseIndex;
};