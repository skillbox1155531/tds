﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_GameMode.h"
#include "GameFramework/PlayerStart.h"
#include "TDS_PlayerStart.generated.h"

class UBoxComponent;
class USoundCue;

UCLASS()
class TDS_API ATDS_PlayerStart : public APlayerStart
{
	GENERATED_BODY()
	// Sets default values for this actor's properties
	ATDS_PlayerStart(const FObjectInitializer& ObjectInitializer);
	
public:
	virtual void PostInitializeComponents() override;
	UFUNCTION()
	void HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	int32 GetProgressIndex() const { return ProgressIndex; }
	void PlayCheckpointSound(ATDS_GameMode* GM, AActor* OtherActor);
private:
	UPROPERTY(VisibleAnywhere)
	UBoxComponent* BoxComponent;
	UPROPERTY(VisibleAnywhere)
	UDecalComponent* DecalComponent;
	UPROPERTY(EditAnywhere, Category = Settings)
	int32 ProgressIndex;
	UPROPERTY(EditAnywhere, Category = Settings)
	TSoftObjectPtr<USoundCue> CheckpointSound;
};
