﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"
#include "Components/ActorComponent.h"
#include "TDS_InputHandlerComponent.generated.h"

class ATDS_PlayerController;
class UTDS_InputConfig;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDS_InputHandlerComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UTDS_InputHandlerComponent();

private:
	void SetupInput();
	void AddMappingContext();
	void BindMovement(const TWeakObjectPtr<UEnhancedInputComponent>& EnhancedInputComponent);
	void BindInputDelegates();
	void HandleMove(const FInputActionValue& InputActionValue);
	void HandleAim(const FInputActionValue& InputActionValue);
	void HandleFire(const FInputActionValue& InputActionValue);
	void HandleReload(const FInputActionValue& InputActionValue);
	void HandleSwitchWeapon(const FInputActionValue& InputActionValue);
	void HandleHealth(const FInputActionValue& InputActionValue);
protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void OnRegister() override;

private:
	TWeakObjectPtr<ATDS_PlayerController> PlayerControllerPtr;
	UPROPERTY(EditDefaultsOnly, Category="Input Config")
	UTDS_InputConfig* InputConfig;
	bool bIsAiming;
};