﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_DelegateLibrary.h"
#include "TDS_StructLibrary.h"
#include "TDS_WeaponData.h"
#include "Components/ActorComponent.h"
#include "TDS_ProjectilePoolComponent.generated.h"

class ATDS_ProjectileBase;

USTRUCT(BlueprintType)
struct FEffectData
{
	GENERATED_BODY()

public:
	UPROPERTY(VisibleAnywhere)
	FEffectInfo EffectData;
	UPROPERTY(VisibleAnywhere)
	TSubclassOf<UTDS_BaseEffect> EffectClass;
};

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDS_ProjectilePoolComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UTDS_ProjectilePoolComponent();
	
	void SpawnProjectilePool(const FWeaponInfo& WeaponInfo, UTDS_WeaponData* WeaponData, const FVector& ProjectileSpawnPosition);
	void ResetProjectilePool();
	TWeakObjectPtr<ATDS_ProjectileBase> GetProjectileFromPool();
	void AddProjectileToPool(const TWeakObjectPtr<ATDS_ProjectileBase> Projectile);
	void EnableEffect(const FWeaponEffectInfo& WeaponEffectInfo, const FEffectInfo* EffectInfo, TSubclassOf<UTDS_BaseEffect> Effect);
	void DisableEffect();
	bool IsEffectEnabled() const;
private:
	FWeaponInfo Info;
	UPROPERTY()
	UTDS_WeaponData* WeaponDataPtr;
	UPROPERTY(VisibleAnywhere)
	FEffectData EffectData;
	TArray<TWeakObjectPtr<ATDS_ProjectileBase>> InactiveProjectileArr;
	TArray<TWeakObjectPtr<ATDS_ProjectileBase>> ActiveProjectileArr;
	FTimerHandle EffectTimerElapsed_TimerHandle;
public:
	FOnEnableEffect OnEnableEffect;
	FOnDisableEffect OnDisableEffect;
}; 