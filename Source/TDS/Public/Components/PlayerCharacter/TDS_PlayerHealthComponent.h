﻿#pragma once

#include "CoreMinimal.h"
#include "TDS_HealthComponentBase.h"
#include "TDS_PlayerHealthComponent.generated.h"

class ATDS_CharacterBase;
struct FHealthInfo;
class UWTDS_PlayerInfoPanel;
class ATDS_PlayerCharacter;

DECLARE_MULTICAST_DELEGATE(FOnShieldWasDepleted);

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDS_PlayerHealthComponent : public UTDS_HealthComponentBase
{
	GENERATED_BODY()

public:
	UTDS_PlayerHealthComponent();
	
	void InitComponent(const TWeakObjectPtr<UWTDS_PlayerInfoPanel> PlayerInfoPanel);
	void InitPhysMaterials();
	virtual float DecreaseHealth(const float& Amount) override;
	virtual bool IncreaseHealthPoints(const float& Amount, const bool& bIsOverTime = false) override;
	bool IncreaseHealth(const FHealthInfo& HealthInfo);
	void SetShieldBarValue(const float& Percent);
	float SetShield(const float& Amount);
	bool IncreaseShield(const float& Amount);
	UFUNCTION(BlueprintCallable)
	float GetCurrentShield() const;
	UFUNCTION(BlueprintCallable)
	float GetMaxShield() const;
	void RefillHealth();
	void SetIsImmune(const bool& Value);
	UFUNCTION(BlueprintCallable)
	static UTDS_PlayerHealthComponent* GetPlayerHealthComponent_BP(AActor* Actor);
	static TWeakObjectPtr<UTDS_PlayerHealthComponent> GetPlayerHealthComponent(AActor* Actor);
protected:
	virtual void OnRegister() override;
	void SetIsShielded(const bool& Value);
	bool GetIsShielded() const;
	
private:
	TWeakObjectPtr<ATDS_CharacterBase> OwnerPtr;
	TWeakObjectPtr<UWTDS_PlayerInfoPanel> PlayerInfoPanelPtr;
	UPROPERTY(EditAnywhere)
	UPhysicalMaterial* GeneralPhysMaterial;
	UPROPERTY(EditAnywhere)
	UPhysicalMaterial* ShieldedPhysMaterial;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = Parameters)
	bool bIsShielded;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = Parameters)
	float CurrentShield;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = Parameters)
	float MaxShield;
	UPROPERTY(EditAnywhere)
	bool bIsImmune;
public:
	FOnShieldWasDepleted OnShieldWasDepleted_Delegate;
};
