﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_DelegateLibrary.h"
#include "TDS_StructLibrary.h"
#include "TDS_WeaponData.h"
#include "Engine/StreamableManager.h"
#include "TDS_WeaponHandlerComponent.generated.h"

class UWTDS_EffectsPanel;
class ATDS_PickupBase;
class UWTDS_WeaponSwapNotifyPanel;
struct FInputActionValue;
class UWTDS_WeaponPanel;
class UWTDS_InventoryPanel;
class ATDS_WeaponBase;
class ATDS_PlayerCharacter;
struct FWeaponInfo;
struct FWeaponParameters;
enum class EWeaponAmmoType : uint8;

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class TDS_API UTDS_WeaponHandlerComponent : public UActorComponent
{
	GENERATED_BODY()
	UTDS_WeaponHandlerComponent();
	
private:
	void InitDefaultWeapon(const FWeaponInfo& WeaponInfo, const bool& bIsRespawn);
	void SelectNextWeapon();
	void SelectPreviousWeapon();
	void SetupCurrentWeapon(const FWeaponInfo& WeaponInfo, UTDS_WeaponData* WeaponData);
	void DropOldWeapon(const FWeaponData& WeaponData);
	void SwapWeaponsInInventory(const FWeaponsInfo& WeaponsInfo);
	void CancelSwapWeaponsInInventory();
	void UpdateWeaponParameters(const FGuid& ID, const FWeaponParameters& Parameters);
	void StartCurrentWeaponFire(const TWeakObjectPtr<UAnimMontage>& Montage);
	void StopCurrentWeaponFire(const TWeakObjectPtr<UAnimMontage>& Montage);
	void StartCurrentWeaponReload(const TWeakObjectPtr<UAnimMontage>& Montage);
	void StopCurrentWeaponReload(const TWeakObjectPtr<UAnimMontage>& Montage);
	bool GetIsWeaponTypeOwned(const FWeaponInfo& Info);
	TTuple<FWeaponInfo, UTDS_WeaponData*> GetWeaponInfoFromArr(const EWeaponAmmoType& AmmoType);
	
protected:
	virtual void BeginPlay() override;
	virtual void OnRegister() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	void StartLoadingWeaponData(const FWeaponInfo& WeaponInfo, const FStreamableDelegate& Delegate);
	void OnDefaultWeaponDataLoaded(FWeaponInfo WeaponInfo, FPrimaryAssetId WeaponID, bool bIsRespawn);
	void OnSwapWeaponDataLoaded(FWeaponInfo WeaponInfo, FPrimaryAssetId WeaponID, TTuple<FWeaponInfo, UTDS_WeaponData*> OwnedWeapon);
	void OnNewWeaponDataLoaded(FWeaponInfo WeaponInfo, FPrimaryAssetId WeaponID);
	void OnWeaponEffectIconLoaded(FWeaponEffectInfo WeaponEffectInfo, const FEffectInfo* EffectInfo, TSubclassOf<UTDS_BaseEffect> EffectClass);
	
public:
	void InitComponent(const TWeakObjectPtr<UWTDS_InventoryPanel>& InventoryPanel, const TWeakObjectPtr<UWTDS_EffectsPanel>&
	                   WeaponEffectsPanel, const TWeakObjectPtr<UWTDS_WeaponSwapNotifyPanel>& WeaponSwapNotifyPanel);
	void HandleStartCurrentWeaponFire(const bool& InputValue);
	void HandleStopCurrentWeaponFire();
	void HandleCurrentWeaponReload();
	void SwitchWeapon(const float& Value);
	bool AddWeaponToInventory(ATDS_PickupBase* Pickup, const FWeaponInfo& WeaponInfo);
	bool AddAmmoToWeaponInInventory(const FAmmoInfo& AmmoInfo);
	bool AddWeaponEffect(const FWeaponEffectInfo& WeaponEffectInfo, const FEffectInfo* EffectInfo, TSubclassOf<UTDS_BaseEffect> EffectClass);
	void RemoveWeaponEffect();
	void InterruptWeaponEffect();
	TWeakObjectPtr<ATDS_WeaponBase> GetCurrentWeapon() const;
	TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>> GetOwnedWeapons();
	static TWeakObjectPtr<UTDS_WeaponHandlerComponent> GetWeaponHandlerComponent(AActor* Actor);
	TArray<EWeaponAmmoType> GetOwnedAmmoTypes();
private:
	TWeakObjectPtr<ATDS_PlayerCharacter> PlayerCharacterPtr;
	TWeakObjectPtr<UWTDS_InventoryPanel> InventoryPanelPtr;
	TWeakObjectPtr<UWTDS_EffectsPanel> WeaponEffectsPanelPtr;
	TWeakObjectPtr<UWTDS_WeaponSwapNotifyPanel> WeaponSwapNotifyPanelPtr;
	TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>> OwnedWeaponsArr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category= "Weapons")
	int32 CurrentIndex;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category= "Weapons")
	int32 CurrentID;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category = "Weapon")
	ATDS_WeaponBase* CurrentWeapon;
	UPROPERTY()
	ATDS_PickupBase* WeaponPickup;
};