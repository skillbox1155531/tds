﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "NiagaraSystem.h"
#include "Engine/DataAsset.h"
#include "TDS_EffectData.generated.h"

UCLASS()
class TDS_API UTDS_EffectData : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	UNiagaraSystem* EffectVFX;
	UPROPERTY(EditAnywhere)
	UTexture2D* EffectIcon;

	FPrimaryAssetId GetPrimaryAssetId() const override
	{
		return FPrimaryAssetId("Effects", GetFName());
	}
};
