﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Object.h"
#include "TDS_EnemyData.generated.h"

UCLASS()
class TDS_API UTDS_EnemyData : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Spawn Info")
	TSubclassOf<AActor> EnemyClass;


	FPrimaryAssetId GetPrimaryAssetId() const override
	{
		return FPrimaryAssetId("Enemies", GetFName());
	}
};
