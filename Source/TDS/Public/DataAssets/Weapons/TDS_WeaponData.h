﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TDS_StructLibrary.h"
#include "Sound/SoundCue.h"
#include "TDS_WeaponData.generated.h"

UCLASS()
class TDS_API UTDS_WeaponData : public UPrimaryDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | Mesh")
	UStaticMesh* WeaponStaticMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | Mesh")
	UStaticMesh* WeaponShellMeshFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | Mesh")
	UStaticMesh* WeaponClipMeshFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | VFX")
	UNiagaraSystem* WeaponMuzzleFlashFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | VFX")
	UNiagaraSystem* ProjectileTrailFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | SFX")
	USoundCue* WeaponFireSoundFX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | Animation")
	UAnimMontage* WeaponFireAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | Animation")
	UAnimMontage* WeaponReloadAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | UI")
	UTexture2D* WeaponThumbnail;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | UI")
	UTexture2D* AmmoThumbnail;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | Projectile ")
	UClass* ProjectileClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category="Weapon | Projectile | VFX")
	TMap<TEnumAsByte<EPhysicalSurface>, FProjectileHitFX> ProjectileHitFXDic;
	
	FPrimaryAssetId GetPrimaryAssetId() const override
	{
		return FPrimaryAssetId("Weapons", GetFName());
	}
};