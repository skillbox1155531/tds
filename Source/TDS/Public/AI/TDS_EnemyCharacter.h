﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_Actor.h"
#include "TDS_CharacterBase.h"
#include "TDS_EnemyCombat.h"
#include "TDS_EnemyCharacter.generated.h"

namespace EEnvQueryStatus
{
	enum Type : int;
}

class UEnvQuery;
class UTDS_EnemyCombatData;
class UTDS_PlayerHealthComponent;

DECLARE_MULTICAST_DELEGATE(FOnAttackEnded)

UCLASS()
class TDS_API ATDS_EnemyCharacter : public ATDS_CharacterBase, public ITDS_EnemyCombat, public ITDS_Actor
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDS_EnemyCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void SetupComponents();
	virtual void DeathLogic(AActor* DamageCauser);
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	void SpawnAmmoDrop(const TArray<EWeaponAmmoType>& AmmoTypes);
	UFUNCTION()
	void OnAmmoSpawnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus);
	
public:
	virtual void PostInitializeComponents() override;
	
	virtual bool GetIsAliveInt_Implementation() override;
	virtual void SetActorMovementStateInt_Implementation(const EMovementState& State) override;
	virtual void StartAttackTarget_Implementation(AActor* TargetActor) override;
	virtual void InterruptAttackTarget_Implementation() override;

	UFUNCTION()
	virtual void StopAttackTarget_Implementation(UAnimMontage* Montage, bool bInterrupted) override;
	UFUNCTION()
	virtual void NotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	UFUNCTION()
	void NotifyEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload);
	
protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UTDS_PlayerHealthComponent* HealthComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category = Combat)
	float HeavyAttackChance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category = Combat)
	float AttackOffset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category = Combat)
	UTDS_EnemyCombatData* LightAttack;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category = Combat)
	UTDS_EnemyCombatData* HeavyAttack;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = Combat)
	TWeakObjectPtr<UTDS_EnemyCombatData> CurrentAttack;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = Drops)
	TArray<FPickupInfo> AmmoDropsArr;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = Drops)
	UEnvQuery* AmmoSpawnQuery;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = Drops)
	int32 AmmoIndex;
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"), Category = Drops)
	float AmmoDropChance;
	
public:
	UPROPERTY()
	FGuid ID;
	UPROPERTY()
	int32 Weight;
	UPROPERTY()
	int32 KillPoints;
	UPROPERTY()
	bool bIsBoss;
	FOnAttackEnded OnAttackEnded_Delegate;
};