﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "EnvironmentQuery/EnvQueryTypes.h"
#include "TDS_BTTask_FindNextLocationPoint.generated.h"

class UEnvQueryInstanceBlueprintWrapper;

UCLASS()
class TDS_API UTDS_BTTask_FindNextLocationPoint : public UBTTaskNode
{
	GENERATED_BODY()

protected:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
	UFUNCTION()
	void OnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus);
	void FinishTask(const TWeakObjectPtr<UObject> Owner, EBTNodeResult::Type TaskResult) const;
	UBlackboardComponent* GetBlackboardComponent(TWeakObjectPtr<UObject> Owner) const;
private:
	TWeakObjectPtr<UBehaviorTreeComponent> BehaviorTreeComponentPtr;
	UPROPERTY(EditAnywhere, Category=AI)
	UEnvQuery* FindLocationQuery;
	UPROPERTY(EditAnywhere, Category = AI)
	FBlackboardKeySelector TargetLocationKey;
};
