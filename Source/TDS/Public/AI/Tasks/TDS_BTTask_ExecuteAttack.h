﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "TDS_BTTask_ExecuteAttack.generated.h"

class ATDS_AIController;

UCLASS()
class TDS_API UTDS_BTTask_ExecuteAttack : public UBTTaskNode
{
	GENERATED_BODY()

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	virtual void OnInstanceCreated(UBehaviorTreeComponent& OwnerComp) override;
};
