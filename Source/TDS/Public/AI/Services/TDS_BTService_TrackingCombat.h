﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "TDS_BTService_TrackingCombat.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API UTDS_BTService_TrackingCombat : public UBTService
{
	GENERATED_BODY()

	UTDS_BTService_TrackingCombat();
	
private:
	virtual void OnSearchStart(FBehaviorTreeSearchData& SearchData) override;
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
	
private:
	UPROPERTY(EditAnywhere, Category = AI)
	float AttackRange;
};
