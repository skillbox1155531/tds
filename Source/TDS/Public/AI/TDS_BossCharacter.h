﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_EnemyCharacter.h"
#include "TDS_BossCharacter.generated.h"

class UEnvQueryInstanceBlueprintWrapper;
class ATDS_Boss_RocketLauncher;

UCLASS()
class TDS_API ATDS_BossCharacter : public ATDS_EnemyCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDS_BossCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void AttackElapsed();
	UFUNCTION()
	void GetEnraged();
	virtual void NotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload) override;
	UFUNCTION()
	void OnBossEnrageAnimEnded(UAnimMontage* Montage, bool bInterrupted);
	void RunEQS();
	UFUNCTION()
	void OnMinionSpawnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus);
	virtual void DeathLogic(AActor* DamageCauser) override;
private:
	UPROPERTY(EditAnywhere, Category = FX)
	USoundCue* BossSpawnSound;
	UPROPERTY(EditAnywhere, Category = FX)
	USoundCue* BossEnrageSound;
	UPROPERTY(EditAnywhere, Category = Weapon)
	TSubclassOf<ATDS_Boss_RocketLauncher> RocketLauncherClass;
	UPROPERTY(EditAnywhere, Category = Minions)
	TSubclassOf<ATDS_EnemyCharacter> MinionClass;
	UPROPERTY(EditAnywhere, Category = Minions)
	int32 MinionsAmount;
	UPROPERTY(EditAnywhere, Category = Minions)
	UEnvQuery* MinionsSpawnQuery;
	UPROPERTY(VisibleAnywhere)
	TArray<AActor*> MinionsArr;
	UPROPERTY(EditAnywhere)
	UAnimMontage* BossEnrageMontage;
	UPROPERTY()
	ATDS_Boss_RocketLauncher* RocketLauncher;
	UPROPERTY(EditAnywhere)
	float AttackDelay;
	FTimerHandle Attack_TimerHandle;
};
