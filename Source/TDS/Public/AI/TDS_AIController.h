﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TDS_AIController.generated.h"

struct FAIStimulus;
class UAISenseConfig_Damage;
class UAISenseConfig_Hearing;
class UAISenseConfig_Sight;

UCLASS()
class TDS_API ATDS_AIController : public AAIController
{
	GENERATED_BODY()

public:
	ATDS_AIController();

	void AttackTarget();
	void DisableSenses();
protected:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void BeginPlay() override;
	virtual void PostInitializeComponents() override;
	
	void InitPerceptionComponent();
	void OnTargetBeingSeen(AActor* Actor);
	void OnTargetBeingHeard(const AActor* Actor, const FAIStimulus& Stimulus);
	void OnPawnBeingDamaged(AActor* Actor);
	void OnTargetBeingLost(FAIStimulus Stimulus);
	void OnTargetBeingSensed(AActor* Actor);
	UFUNCTION()
	void OnTargetDetected(AActor* Actor, const FAIStimulus Stimulus);
	void SetInvestigationTimer(const bool& bIsActive);
	void ResetAlarmedState();
	
private:
	UPROPERTY(VisibleAnywhere, Category = "AI")
	UAIPerceptionComponent* AIPerceptionComponent;
	UPROPERTY()
	UAISenseConfig_Sight* SightConfig;
	UPROPERTY()
	UAISenseConfig_Hearing* HearingConfig;
	UPROPERTY()
	UAISenseConfig_Damage* DamageConfig;
	UPROPERTY(EditAnywhere, Category = "AI")
	float SightRadius;
	UPROPERTY(EditAnywhere, Category = "AI")
	float SightLoseRadius;
	UPROPERTY(EditAnywhere, Category = "AI")
	float SightAngle;
	UPROPERTY(EditAnywhere, Category = "AI")
	float HearingRange;
	UPROPERTY(EditAnywhere, Category = "AI")
	float AlarmedHearingRange;
	UPROPERTY(EditAnywhere, Category = "AI")
	UBehaviorTree* BehaviorTree;

	UPROPERTY(EditAnywhere, Category = "AI")
	float InvestigationTime;
	
	FTimerHandle InvestigationTimeElapsed_TimerHandle;
};

