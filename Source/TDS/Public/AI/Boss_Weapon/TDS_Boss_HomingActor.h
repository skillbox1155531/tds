﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_Boss_HomingActor.generated.h"

class UNiagaraSystem;

UCLASS()
class TDS_API ATDS_Boss_HomingActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_Boss_HomingActor();

	void SetRadius(const float& Radius);
	void LoadAssets();
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

	void OnAssetsLoaded();
	void OnSpawnDecal();
	
private:
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UMaterialInstance> HitDecal;
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UNiagaraSystem> Explosion;
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UParticleSystem> Explosion_Old;
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<USoundCue> ExplosionSound;
	UPROPERTY(VisibleAnywhere)
	UDecalComponent* HitDecalComp;
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MeshComp;
	float OuterRadius;
};
