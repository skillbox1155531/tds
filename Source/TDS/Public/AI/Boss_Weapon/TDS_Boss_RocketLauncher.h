﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_Boss_HomingActor.h"
#include "GameFramework/Actor.h"
#include "TDS_Boss_RocketLauncher.generated.h"

namespace EEnvQueryStatus
{
	enum Type : int;
}

class UEnvQuery;
class UArrowComponent;
class ATDS_Boss_Rocket;

UCLASS()
class TDS_API ATDS_Boss_RocketLauncher : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_Boss_RocketLauncher();

	void SetSpawnRocketsTimer(AActor* Target);
	void SpawnRocket(ATDS_Boss_HomingActor* Target);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	void RunEQS();
	UFUNCTION()
	void OnRocketTargetQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus);

private:
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> HomingTarget;
	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MeshComp;
	UPROPERTY(VisibleAnywhere)
	UArrowComponent* SpawnLocation;
	UPROPERTY(EditAnywhere)
	TSubclassOf<ATDS_Boss_Rocket> RocketClass;
	UPROPERTY(EditAnywhere)
	int32 RocketsAmount;
	UPROPERTY(EditAnywhere)
	int32 SpawnedRocketsAmount;
	UPROPERTY(EditAnywhere)
	float SpawnRocketsDelay;
	UPROPERTY(EditDefaultsOnly, Category=EQS)
	UEnvQuery* RocketTargetQuery;
	FTimerHandle SpawnRockets_TimerHandle;
};