﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_Boss_Rocket.generated.h"

class ATDS_Boss_HomingActor;
class UArrowComponent;
class UBoxComponent;
class UNiagaraComponent;
class UNiagaraSystem;
class UProjectileMovementComponent;

USTRUCT()
struct FRocketInfo
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<UNiagaraSystem> Trail;
	UPROPERTY(EditAnywhere)
	TSoftObjectPtr<USoundCue> TrailSound;

	UPROPERTY(EditAnywhere)
	float Damage;
	UPROPERTY(EditAnywhere)
	float DamageFalloff;
	UPROPERTY(EditAnywhere)
	float InnerRadius;
	UPROPERTY(EditAnywhere)
	float OuterRadius;
};

UCLASS()
class TDS_API ATDS_Boss_Rocket : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDS_Boss_Rocket();

	void InitRocket(ATDS_Boss_HomingActor* Target);
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void PostInitializeComponents() override;
	
	void LoadStaticAssets();
	void OnStaticAssetsLoaded();
	void SetHomingActor();
	void HandleSpiralMovement();
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
	
private:
	UPROPERTY()
	ATDS_Boss_HomingActor* TargetActor;
	UPROPERTY(VisibleAnywhere)
	UBoxComponent* BoxComp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* MeshComp;
	UPROPERTY(VisibleAnywhere)
	UProjectileMovementComponent* RocketMovementComp;
	UPROPERTY(VisibleAnywhere)
	UNiagaraComponent* RocketTrailComp;
	UPROPERTY(VisibleAnywhere)
	UAudioComponent* AudioComp;
	UPROPERTY(EditAnywhere)
	FRocketInfo RocketInfo;
	FVector SpawnLocation;

	UPROPERTY(EditAnywhere)
	float HeightTrackRate;
	UPROPERTY(EditAnywhere)
	float HeightThreshold;
	FTimerHandle HeightTrack_TimerHandle;

	UPROPERTY(EditAnywhere)
	float SpiralUpdateRate;
	UPROPERTY(EditAnywhere, Category = "Spiral")
	float BaseSpiralSpeed;
	UPROPERTY(EditAnywhere, Category = "Spiral")
	float SpiralRadiusMin;
	UPROPERTY(EditAnywhere, Category = "Spiral")
	float SpiralRadiusMax;
	FTimerHandle Spiral_TimerHandle;
};
