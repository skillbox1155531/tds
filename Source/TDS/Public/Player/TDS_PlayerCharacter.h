﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "InputActionValue.h"
#include "TDS_Actor.h"
#include "TDS_CharacterBase.h"
#include "TDS_DelegateLibrary.h"
#include "TDS_IPickup.h"
#include "TDS_Player.h"
#include "TDS_PlayerCharacter.generated.h"

class UTDS_PlayerHealthComponent;
class UWTDS_PlayerHUD;
class UTDS_WeaponHandlerComponent;
class USpringArmComponent;
class UCameraComponent;
class ATDS_PlayerController;
class UTDS_PlayerAnimInstance;
class ATDS_WeaponBase;
class ATDS_PickupBase;

UCLASS()
class TDS_API ATDS_PlayerCharacter : public ATDS_CharacterBase, public ITDS_Actor , public ITDS_Player
{
	GENERATED_BODY()

public:
	ATDS_PlayerCharacter();

	void BindInputDelegates(const TWeakObjectPtr<ATDS_PlayerController>& PlayerController);
	virtual void InitComponents(const TWeakObjectPtr<UWTDS_PlayerHUD>& PlayerHUD) override;
	void PlayMontage(const TWeakObjectPtr<UAnimMontage>& Montage);
	void StopMontage(const TWeakObjectPtr<UAnimMontage>& Montage);
	void StopAllMontages();

	TWeakObjectPtr<UTDS_PlayerAnimInstance> GetPlayerAnimInstance() const;
	TWeakObjectPtr<USpringArmComponent> GetSpringArmComponent() const;
	TWeakObjectPtr<UDecalComponent> GetCursorDecalComponent() const;
	TWeakObjectPtr<UTDS_WeaponHandlerComponent> GetWeaponHandlerComponent() const;
	TWeakObjectPtr<UTDS_PlayerHealthComponent> GetHealthComponent() const;
	FDataTableRowHandle GetDefaultWeaponID() const;
	virtual FVector GetActorLocationInt_Implementation() override;
	virtual bool GetIsAliveInt_Implementation() override;
	virtual TArray<EWeaponAmmoType> GetOwnedAmmoTypes_Implementation() override;
private:
	void SetupCameraComponent();
	void SetupCursorDecalComponent();
	void SetupComponents();
	void HandleMoveCharacter(const FInputActionValue& InputActionValue);
	void HandleCharacterAim(const FInputActionValue& InputActionValue);
	void HandleFireWeapon(const FInputActionValue& InputActionValue);
	void HandleReloadWeapon(const FInputActionValue& InputActionValue);
	void HandleSwitchWeapon(const FInputActionValue& InputActionValue);
	void Test_HandleChangeHealth(const FInputActionValue& InputActionValue);
	void ShowDamageVisual(const float& Value);
	void DeathLogic(AActor* DamageCauser, APlayerController* PlayerController);

	UFUNCTION()
	void HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	USpringArmComponent* SpringArmComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UCameraComponent* CameraComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UDecalComponent* CursorDecalComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UTDS_WeaponHandlerComponent* WeaponHandlerComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UTDS_PlayerHealthComponent* HealthComponent;
	TWeakObjectPtr<UTDS_PlayerAnimInstance> AnimInstance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category = "Weapon")
	FDataTableRowHandle DefaultWeaponID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category = "Weapon")
	TWeakObjectPtr<ATDS_PickupBase> CurrentPickup;
	TArray<TWeakObjectPtr<UActorComponent>> EffectApplicableComponentsArr;
	UPROPERTY(VisibleAnywhere, Category=Status)
	FGameplayTag StunTag;
public:
	FOnChangeInputMode OnChangeInputMode;
};
