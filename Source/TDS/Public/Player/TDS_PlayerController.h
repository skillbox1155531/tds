﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "TDS_DelegateLibrary.h"
#include "GameFramework/PlayerController.h"
#include "TDS_PlayerController.generated.h"

class UWTDS_PlayerHUD;
class USpringArmComponent;
class UTDS_PlayerAnimInstance;
class ATDS_PlayerCharacter;
class UTDS_InputHandlerComponent;
struct FInputActionValue;

UCLASS()
class TDS_API ATDS_PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATDS_PlayerController();

private:
	void SetupComponents();
	void SetGameInputMode(const bool& bIsGameModeOnly);
	void UpdateAnimParameters() const;
	void UpdateCursorPosition();
	void UpdateCharRotationToCursor(const FVector& HitLocation);
	void TrackCursorDistance() const;

protected:
	virtual void OnPossess(APawn* InPawn) override;
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaSeconds) override;
	virtual void SetPawn(APawn* InPawn) override;
	void SetGameOnlyMode();
	UFUNCTION(BlueprintCallable)
	void SetGameUIOnlyMode();

	
private:
	TWeakObjectPtr<ATDS_PlayerCharacter> PlayerCharacterPtr;
	TWeakObjectPtr<UTDS_PlayerAnimInstance> PlayerAnimInstancePtr;
	TWeakObjectPtr<USpringArmComponent> SpringArmComponentPtr;
	TWeakObjectPtr<UDecalComponent> CursorDecalPtr;
	UPROPERTY(EditDefaultsOnly, Category=UI)
	TSubclassOf<UWTDS_PlayerHUD> PlayerHUDClass
;	UPROPERTY()
	UWTDS_PlayerHUD* PlayerHUD;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UTDS_InputHandlerComponent* InputHandlerComponent;
	UPROPERTY(VisibleAnywhere, Category=Status)
	FGameplayTag StunTag;
	bool bIsUIModeEnabled;
	FVector2d CachedMouseLoc;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category= "Camera | Tracking Settings")
	float DistanceThresholdMultiplayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category= "Camera | Tracking Settings")
	float CameraDirectionMultiplayer;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category= "Camera | Tracking Settings")
	float CameraDistanceTolerance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"), Category= "Camera | Tracking Settings")
	float CameraZeroDistance;
	FRotator LookAt;
public:
	FOnExecuteInputAction OnCharacterMove;
	FOnExecuteInputAction OnCharacterAim;
	FOnExecuteInputAction OnCharacterFireWeapon;
	FOnExecuteInputAction OnCharacterReloadWeapon;
	FOnExecuteInputAction OnCharacterSwitchWeapon;
	FOnExecuteInputAction OnCharacterChangeHealth;
};
