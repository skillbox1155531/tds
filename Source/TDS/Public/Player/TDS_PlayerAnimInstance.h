﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "TDS_PlayerAnimInstance.generated.h"

enum class EMovementState : uint8;

UCLASS()
class TDS_API UTDS_PlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	void SetVelocityParam(const FVector& Velocity);
	void SetMovementDirParam(const FVector& Velocity, const FRotator& Rotation);
	void SetMovementStateParam(const EMovementState& State);
	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta=(AllowPrivateAccess="true"))
	float MovementSpeed;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta=(AllowPrivateAccess="true"))
	float MovementDirection;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta=(AllowPrivateAccess="true"))
	EMovementState MovementState;
};
