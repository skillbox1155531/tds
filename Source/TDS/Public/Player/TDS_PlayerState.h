﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_WeaponData.h"
#include "GameFramework/PlayerState.h"
#include "TDS_PlayerState.generated.h"

struct FWeaponInfo;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPlayerStateValueChanged, const int32&, Value);

UCLASS()
class TDS_API ATDS_PlayerState : public APlayerState
{
	GENERATED_BODY()

	ATDS_PlayerState();
	
public:
	void PlayerDied(AActor* Killer);
	void IncreaseScore(const int32& Amount);
	void UpdateOwnedWeapons(const TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>>& OwnedWeapons);
	TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>> GetOwnedWeapons();
	int32 GetPlayerScore() const;
	int32 GetPlayerLives() const;

protected:
	virtual void PostInitializeComponents() override;
	
private:
	UPROPERTY(BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	int32 PlayerLives;
	UPROPERTY(BlueprintReadOnly, meta=(AllowPrivateAccess=true))
	int32 PlayerScore;
	UPROPERTY()
	TMap<FWeaponInfo, UTDS_WeaponData*> OwnedWeaponsDic;

public:
	UPROPERTY(BlueprintAssignable)
	FOnPlayerStateValueChanged OnLivesChanged;
	UPROPERTY(BlueprintAssignable)
	FOnPlayerStateValueChanged OnScoreChanged;
};
