﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_BaseEffect.h"
#include "TDS_Effect_Stun.generated.h"

UCLASS()
class TDS_API UTDS_Effect_Stun : public UTDS_BaseEffect
{
	GENERATED_BODY()

protected:
	virtual void OnEffectDataLoaded(const FEffectInfo EffectInfo, FPrimaryAssetId Data, AActor* Instigator) override;
};
