﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_BaseEffect.h"
#include "TDS_Effect_DamageOverTime.generated.h"


UCLASS()
class TDS_API UTDS_Effect_DamageOverTime : public UTDS_BaseEffect
{
	GENERATED_BODY()

protected:
	virtual void OnEffectDataLoaded(const FEffectInfo EffectInfo, FPrimaryAssetId Data, AActor* Instigator) override;
	virtual void ApplyPeriodicEffect(AActor* Instigator) override;

};