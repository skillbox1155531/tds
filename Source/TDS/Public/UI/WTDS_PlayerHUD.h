﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_PlayerHUD.generated.h"

class UWTDS_EffectsPanel;
class UWTDS_PlayerInfoPanel;
class UWTDS_InventoryPanel;
class UWTDS_WeaponSwapNotifyPanel;
/**
 * 
 */
UCLASS()
class TDS_API UWTDS_PlayerHUD : public UUserWidget
{
	GENERATED_BODY()

public:
	TWeakObjectPtr<UWTDS_InventoryPanel> GetInventoryPanel() const;
	TWeakObjectPtr<UWTDS_PlayerInfoPanel> GetPlayerInfoPanel() const;
	TWeakObjectPtr<UWTDS_WeaponSwapNotifyPanel> GetWeaponSwapNotifyPanel() const;
	TWeakObjectPtr<UWTDS_EffectsPanel> GetPlayerEffectsPanel() const;
	TWeakObjectPtr<UWTDS_EffectsPanel> GetWeaponEffectsPanel() const;
private:
	UPROPERTY(meta=(BindWidget))
	UWTDS_InventoryPanel* InventoryPanel;
	UPROPERTY(meta=(BindWidget))
	UWTDS_PlayerInfoPanel* PlayerInfoPanel;
	UPROPERTY(meta=(BindWidget))
	UWTDS_WeaponSwapNotifyPanel* WeaponSwapNotifyPanel;
	UPROPERTY(meta=(BindWidget))
	UWTDS_EffectsPanel* PlayerEffectsPanel;
	UPROPERTY(meta=(BindWidget))
	UWTDS_EffectsPanel* WeaponEffectsPanel;
};
