﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_DelegateLibrary.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_EffectIcon.generated.h"

struct FEffectInfo;
class UTextBlock;
class UProgressBar;
class UImage;
/**
 * 
 */
UCLASS()
class TDS_API UWTDS_EffectIcon : public UUserWidget
{
	GENERATED_BODY()

private:
	virtual void NativeConstruct() override;
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime) override;
	void UpdateBar();
	void UpdateText();
	
public:
	void EnableEffectIcon(const FEffectInfo& Info, UTexture2D* Texture);
	void EnableEffectIcon(const FWeaponEffectInfo& WeaponEffectInfo, UTexture2D* Texture);
	void RemoveEffectIcon();
	void IncreaseStacks();
	void Restart();
	FGuid GetID() const;
private:
	UPROPERTY(meta=(BindWidget))
	UImage* EffectImage;
	UPROPERTY(meta=(BindWidget))
	UProgressBar* EffectProgressBar;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* EffectTimerTextBlock;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* EffectStacksTextBlock;
	FGuid ID;
	float EffectDuration;
	float CurrentEffectTime;
	int8 Stacks;
	FTimerHandle EffectDurationTImerHandle;
	FTimerHandle EffectBar_TimerHandle;
	FTimerHandle EffectText_TimerHandle;
};
