﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_DelegateLibrary.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_CustomButton.generated.h"

class USizeBox;
class UButton;
class UTextBlock;

UCLASS()
class TDS_API UWTDS_CustomButton : public UUserWidget
{
	GENERATED_BODY()

	UFUNCTION()
	void OnButtonClicked();
	
protected:
	virtual void NativeConstruct() override;

public:
	TWeakObjectPtr<USizeBox> GetSizeBox() const;
	TWeakObjectPtr<UButton> GetButton() const;
	TWeakObjectPtr<UTextBlock> GetTextBlock() const;
	
private:
	UPROPERTY(meta=(BindWidget))
	USizeBox* SizeBox;
	UPROPERTY(meta=(BindWidget))
	UButton* MainButton;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* ButtonText;

public:
	FOnUIButtonClicked OnUIButtonClicked;
};
