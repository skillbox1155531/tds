﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_PickupIcon.generated.h"


class UImage;

UCLASS()
class TDS_API UWTDS_PickupIcon : public UUserWidget
{
	GENERATED_BODY()

protected:
	virtual void NativePreConstruct() override;
	
public:
	void SetIcon(const TWeakObjectPtr<UTexture2D>& Texture);
	bool GetIsIconSet() const;
private:
	UPROPERTY(meta=(BindWidget))
	UImage* Icon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess="true"))
	UTexture2D* IconTexture;
};
