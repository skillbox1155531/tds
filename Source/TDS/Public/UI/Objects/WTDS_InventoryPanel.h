﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_WeaponData.h"
#include "Blueprint/UserWidget.h"
#include "Libraries/TDS_StructLibrary.h"
#include "WTDS_InventoryPanel.generated.h"

class UWTDS_EffectsPanel;
class UVerticalBox;
class UWrapBox;
class UWTDS_WeaponPanel;

UCLASS()
class TDS_API UWTDS_InventoryPanel : public UUserWidget
{
	GENERATED_BODY()

	virtual void NativeConstruct() override;
	void UpdateUnselectedPanels(int32 Index, const FWeaponInfo& Info, UTDS_WeaponData* WeaponData, const bool& bIsIncrease);

public:
	void UpdateCurrentWeaponPanel(const FWeaponInfo& Info, UTDS_WeaponData* WeaponData);
	void UpdateWeaponPanels(const FWeaponInfo& Info, UTDS_WeaponData* WeaponData, const bool& bIsIncrease);
	TWeakObjectPtr<UWTDS_WeaponPanel> AddWeaponPanel();
	void SetInfoInUnselectedWeaponPanel(const TWeakObjectPtr<UWTDS_WeaponPanel>& WeaponPanel, const FWeaponInfo& Info, UTDS_WeaponData* WeaponData);
	void RebuildInventoryBox();
	void UpdateInfoInInventoryBox(const FWeaponInfo& Info, const UTDS_WeaponData* WeaponData);
	void StartCurrentWeaponReloadUI(const float& ReloadTime);
	void ExecuteCurrentWeaponReloadUI(const float& ReloadTime);
	TWeakObjectPtr<UWTDS_WeaponPanel> GetCurrentWeaponPanel() const;
private:
	UPROPERTY(meta=(BindWidget))
	UWrapBox* InventoryBox;
	UPROPERTY()
	UWTDS_WeaponPanel* CurrentWeaponPanel;
	UPROPERTY()
	TArray<UWTDS_WeaponPanel*> WeaponPanelsArr;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWTDS_WeaponPanel> SelectedWeaponPanelClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UWTDS_WeaponPanel> UnselectedWeaponPanelClass;
	
	FTimerHandle ReloadUITimerHandle;
};
