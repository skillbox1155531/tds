﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UObject/Object.h"
#include "WTDS_RespawnCountDown.generated.h"

class UTextBlock;
class UProgressBar;

UCLASS()
class TDS_API UWTDS_RespawnCountDown : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent)
	void SetTimer(const float& Value);
	void UpdateTimerBar(const float& Value);
	void UpdateTimerText(const float& Value);
	void ClearTimer();

private:
	UPROPERTY(meta=(BindWidget))
	UProgressBar* TimerProgressBar;
	UPROPERTY(meta=(BindWidget))
	UTextBlock* TimerText;
	float Timer;
	
	FTimerHandle TimerBar_TimerHandle;
	FTimerHandle TimerText_TimerHandle;
};
