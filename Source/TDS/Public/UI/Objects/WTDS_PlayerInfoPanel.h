﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_PlayerInfoPanel.generated.h"

class UProgressBar;
class UWTDS_ProgressBar;

UCLASS()
class TDS_API UWTDS_PlayerInfoPanel : public UUserWidget
{
	GENERATED_BODY()

private:
	void ModifyHealthBarColor(const float& Value);

protected:
	virtual void NativeConstruct() override;
	
public:
	void SetHealthBarValue(const float& Value);
	void SetShieldBarValue(const float& Value);
	void HandleModifyHealthBarValue(const float& Value);
	void HandleModifyShieldBarValue(const float& Value);
	
private:
	UPROPERTY(meta=(BindWidget))
	UWTDS_ProgressBar* HealthBar;
	UPROPERTY(meta=(BindWidget))
	UWTDS_ProgressBar* ShieldBar;
	UPROPERTY(Transient, meta=(BindWidgetAnim))
	UWidgetAnimation* LowHealthAlertAnim;
	TWeakObjectPtr<UUMGSequencePlayer> SequencePlayer;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = TEXT_TRUE), Category= "Parameters | Bar Textures")
	FProgressBarStyle BarStyleMax;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = TEXT_TRUE), Category= "Parameters | Bar Textures")
	FProgressBarStyle BarStyleMid;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = TEXT_TRUE), Category= "Parameters | Bar Textures")
	FProgressBarStyle BarStyleMin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = TEXT_TRUE), Category= "Parameters | Color Threshold")
	float MaxPercent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta=(AllowPrivateAccess = TEXT_TRUE), Category= "Parameters | Color Threshold")
	float MidPercent;
	FTimerHandle HealthBarTimerHandle;
	FTimerHandle ShieldBarTimerHandle;
	FWidgetAnimationDynamicEvent OnHealthAlertAnimationFinished;
};
