﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_DelegateLibrary.h"
#include "TDS_StructLibrary.h"
#include "TDS_WeaponData.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_WeaponSwapNotifyPanel.generated.h"

struct FWeaponInfo;
class UWTDS_CustomButton;
class UImage;

UCLASS()
class TDS_API UWTDS_WeaponSwapNotifyPanel : public UUserWidget
{
	GENERATED_BODY()
	
	void HandleButtonClicked(const TWeakObjectPtr<UWTDS_CustomButton>& Button);

	UFUNCTION()
	void ConfirmedSwapFinishedCallback();
	UFUNCTION()
	void CanceledSwapFinishedCallback();
protected:
	virtual void NativePreConstruct() override;
	virtual void NativeConstruct() override;

public:
	void HandleWeaponSwapNotification(TTuple<FWeaponInfo, UTDS_WeaponData*> Info1, TTuple<FWeaponInfo, UTDS_WeaponData*> Info2);
	
private:
	UPROPERTY(meta=(BindWidget))
	UImage* CurrentWeaponThumbnail;
	UPROPERTY(meta=(BindWidget))
	UImage* PickupWeaponThumbnail;
	UPROPERTY(meta=(BindWidget))
	UWTDS_CustomButton* SwapButton;
	UPROPERTY(meta=(BindWidget))
	UWTDS_CustomButton* CancelButton;
	UPROPERTY(Transient, meta=(BindWidgetAnim))
	UWidgetAnimation* ConfirmedSwapAnimation;
	UPROPERTY(Transient, meta=(BindWidgetAnim))
	UWidgetAnimation* CanceledSwapAnimation;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = "Customization")
	FVector2D ButtonSize;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = "Customization")
	UTexture2D* SwapButtonTextureNormal;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = "Customization")
	UTexture2D* SwapButtonTextureHovered;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = "Customization")
	UTexture2D* CancelButtonTextureNormal;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = "Customization")
	UTexture2D* CancelButtonTextureHovered;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = "Customization")
	FText SwapButtonName;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, meta=(AllowPrivateAccess = "true"), Category = "Customization")
	FText CancelButtonName;
	TTuple<FWeaponInfo, UTDS_WeaponData*> WeaponInfo1;
	TTuple<FWeaponInfo, UTDS_WeaponData*> WeaponInfo2;
	FWidgetAnimationDynamicEvent OnConfirmedSwapAnimationFinished;
	FWidgetAnimationDynamicEvent OnCanceledSwapAnimationFinished;

public:
	FOnWeaponsInfoSwapCompleted OnWeaponsInfoSwapCompleted;
	FOnWeaponsInfoSwapCanceled OnWeaponsInfoSwapCanceled;
};
