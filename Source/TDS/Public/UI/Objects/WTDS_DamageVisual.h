﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_DelegateLibrary.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_DamageVisual.generated.h"

class UTextBlock;

UCLASS()
class TDS_API UWTDS_DamageVisual : public UUserWidget
{
	GENERATED_BODY()

private:
	UFUNCTION()
	void OnDamageVisualCompletedCallback();
	
protected:
	virtual void NativeConstruct() override;

public:
	void PlayDamageVisual(const float& Value);
	void SetShieldDamageColor();
private:
	UPROPERTY(meta=(BindWidget))
	UTextBlock* TextBlock;
	UPROPERTY(Transient, meta=(BindWidgetAnim))
	UWidgetAnimation* DamageFadeOutAnim;
	FWidgetAnimationDynamicEvent OnDamageFadeOutAnimationFinished;

public:
	FOnDamageVisualCompleted OnDamageVisualCompleted;
};
