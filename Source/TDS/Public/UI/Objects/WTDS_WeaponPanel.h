﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_StructLibrary.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_WeaponPanel.generated.h"

class UTDS_WeaponData;
class UWTDS_EffectsPanel;
class UBorder;
class UImage;
class UProgressBar;
class UTextBlock;

UCLASS()
class TDS_API UWTDS_WeaponPanel : public UUserWidget
{
	GENERATED_BODY()

public:
	void SetBorder(const TWeakObjectPtr<UTexture2D>& Texture);
	void SetWeaponIcon(const TWeakObjectPtr<UTexture2D>& Texture);
	void SetBorderColor(const bool& bIsEmpty);
	void SetReloadProgressBarValue(const float& Value);
	void ResetReloadProgressBarValue();
	void SetIsReloadProgressBarVisible(const bool& Value);
	void SetAmmoText(const FText& Text);
	void SetAmmoText(const FText& Text1, const FText& Text2);
	void SetWeaponInfo(const FWeaponInfo& Info);
	void SetWeaponData(UTDS_WeaponData* Data);

	FWeaponInfo& GetWeaponInfo();
	UTDS_WeaponData* GetWeaponData();
	float GetReloadProgressBarValue() const;
private:
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UBorder* Border;
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UImage* WeaponIcon;
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UProgressBar* ReloadProgressBar;
	UPROPERTY(EditAnywhere, meta=(BindWidget))
	UTextBlock* AmmoText;
	UPROPERTY()
	FWeaponInfo WeaponInfo;
	UPROPERTY()
	UTDS_WeaponData* WeaponData;
};
