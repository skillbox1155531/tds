﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDS_DelegateLibrary.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_EffectsPanel.generated.h"

class UWTDS_EffectIcon;
struct FEffectInfo;
class UWrapBox;
/**
 * 
 */
UCLASS()
class TDS_API UWTDS_EffectsPanel : public UUserWidget
{
	GENERATED_BODY()

public:
	void AddEffect(const FEffectInfo& Info, UTexture2D* Texture);
	void AddEffect(const FWeaponEffectInfo& Info, UTexture2D* Texture);
	void ModifyEffect(const FGuid& ID);
	void RestartEffect(const FGuid& ID);
	void RemoveEffect(const FGuid& ID);
	void ResetPanel();
private:
	UPROPERTY(meta=(BindWidget))
	UWrapBox* Panel;
	TArray<UWTDS_EffectIcon*> EffectIconsArr;
	UPROPERTY(EditDefaultsOnly, Category="Effects")
	TSubclassOf<UWTDS_EffectIcon> EffectIconClass;
};
