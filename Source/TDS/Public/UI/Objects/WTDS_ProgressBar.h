﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "WTDS_ProgressBar.generated.h"

class UProgressBar;

UCLASS()
class TDS_API UWTDS_ProgressBar : public UUserWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
	void SetBarValue(const float Value);
	UFUNCTION(BlueprintCallable)
	void ModifyBarValue(const float Value);
	UFUNCTION()
	void UpdateBarValue(const float Value, const float Speed);
	UFUNCTION(BlueprintCallable)
	void SetWidgetStyle(FProgressBarStyle NewStyle);
private:
	UPROPERTY(meta = (BindWidget))
	UProgressBar* Bar;
	UPROPERTY(EditAnywhere, Category = Parameters)
	float InterpSpeed;
	FTimerHandle BarUpdate_TimerHandle;
};
