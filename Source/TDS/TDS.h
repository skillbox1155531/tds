// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(LogTDS, Log, All);

static void LogOnScreen(const UObject* WorldContext, const FString& Msg, FColor Color = FColor::White, float Duration = 5.f)
{
	if (!ensure(WorldContext))
	{
		return;
	}

	const UWorld* World = WorldContext -> GetWorld();

	if (!ensure(World))
	{
		return;
	}

	const FString NetPrefix = World -> IsNetMode(NM_Client) ? TEXT("[CLIENT] " : TEXT("[SERVER] "));

	if (GEngine)
	{
		GEngine -> AddOnScreenDebugMessage(-1, Duration, Color, NetPrefix + Msg);
	}
}