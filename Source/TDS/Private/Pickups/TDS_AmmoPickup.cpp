﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_AmmoPickup.h"

#include "TDS_StringLibrary.h"
#include "TDS_WeaponHandlerComponent.h"
#include "Components/DecalComponent.h"
#include "Engine/AssetManager.h"

ATDS_AmmoPickup::ATDS_AmmoPickup()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATDS_AmmoPickup::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	InitComponents();
}

void ATDS_AmmoPickup::InitComponents()
{
	if (FAmmoInfo* AmmoInfo = PickupInfo.GetInfo().GetRow<FAmmoInfo>(PickupInfo.GetInfo().RowName.ToString()))
	{
		FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
		const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnDataLoaded, PickupInfo.GetPickupMesh());
		Manager.RequestAsyncLoad(PickupInfo.GetPickupMesh().ToSoftObjectPath(), Delegate);
		AmmoPickupInfo = *AmmoInfo;
	}
}

void ATDS_AmmoPickup::InitComponents(FAmmoInfo* AmmoInfo, TSoftObjectPtr<UStaticMesh> PickupMesh,  TSoftObjectPtr<UMaterialInstance> PickupDecal)
{
	if (AmmoInfo)
	{
		UE_LOG(LogTemp, Display, TEXT("Attempting to load Ammo Info"));
		FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
		const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnDataLoaded, PickupMesh, PickupDecal);

		TArray<FSoftObjectPath> ObjectsToLoadArr;
		ObjectsToLoadArr.Emplace(PickupMesh.ToSoftObjectPath());
		ObjectsToLoadArr.Emplace( PickupDecal.ToSoftObjectPath());

		Manager.RequestAsyncLoad(ObjectsToLoadArr, Delegate);
		AmmoPickupInfo = *AmmoInfo;
	}
}

void ATDS_AmmoPickup::OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh)
{
	if (Mesh.IsValid())
	{
		if (UStaticMesh* MeshPtr = Mesh.Get())
		{
			UE_LOG(LogTemp, Display, TEXT("Finishing to load Ammo"));
			StaticMeshComponent -> SetStaticMesh(MeshPtr);
		}
	}

	if (!DecalComponent -> GetMaterial(0))
	{
		DecalComponent -> DestroyComponent();
	}

	if (!NiagaraComponent -> GetAsset())
	{
		NiagaraComponent -> DestroyComponent();
	}

	if (!IconWidgetComponent -> GetUserWidgetObject())
	{
		IconWidgetComponent -> DestroyComponent();
	}
}

void ATDS_AmmoPickup::OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh, TSoftObjectPtr<UMaterialInstance> Decal)
{
	if (Mesh.IsValid())
	{
		if (UStaticMesh* MeshPtr = Mesh.Get())
		{
			UE_LOG(LogTemp, Display, TEXT("Finishing to load Ammo"));
			StaticMeshComponent -> SetStaticMesh(MeshPtr);
		}
	}

	if (Decal.IsValid())
	{
		if (UMaterialInstance* DecalPtr = Decal.Get())
		{
			DecalComponent -> SetRelativeLocation(FVector(0.f, 0.f, -20.f));
			DecalComponent -> SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
			DecalComponent -> DecalSize = FVector(25.f, 50.f, 50.f);
			DecalComponent -> SetDecalMaterial(DecalPtr);
		}
	}

	if (!NiagaraComponent -> GetAsset())
	{
		NiagaraComponent -> DestroyComponent();
	}

	if (!IconWidgetComponent -> GetUserWidgetObject())
	{
		IconWidgetComponent -> DestroyComponent();
	}
}

void ATDS_AmmoPickup::HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor -> ActorHasTag(FTagLibrary::PlayerTag))
	{
		UE_LOG(LogTemp, Display, TEXT("Attempting to handle Ammo Overlap"));
		TWeakObjectPtr<UTDS_WeaponHandlerComponent> WeaponHandlerComponent = UTDS_WeaponHandlerComponent::GetWeaponHandlerComponent(OtherActor);
		
		if (WeaponHandlerComponent.IsValid())
		{
			if (WeaponHandlerComponent -> AddAmmoToWeaponInInventory(AmmoPickupInfo))
			{
				Destroy();
			}
		}
	}
}

