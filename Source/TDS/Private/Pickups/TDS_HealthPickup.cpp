﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HealthPickup.h"

#include "TDS_PlayerHealthComponent.h"
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"


ATDS_HealthPickup::ATDS_HealthPickup()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATDS_HealthPickup::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	InitComponents();
}

void ATDS_HealthPickup::InitComponents()
{
	if (FHealthInfo* HealthInfo = PickupInfo.GetInfo().GetRow<FHealthInfo>(PickupInfo.GetInfo().RowName.ToString()))
	{
		FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
		const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnDataLoaded, HealthInfo -> GetHealthPickupEmitter());
		Manager.RequestAsyncLoad(HealthInfo -> GetHealthPickupEmitter().ToSoftObjectPath(), Delegate);
		HealthPickupInfo = *HealthInfo;
	}
}

void ATDS_HealthPickup::OnDataLoaded(TSoftObjectPtr<UNiagaraSystem> NiagaraSystem)
{
	if (NiagaraSystem.IsValid())
	{
		if (UNiagaraSystem* NiagaraSystemPtr = NiagaraSystem.Get())
		{
			NiagaraComponent -> SetAsset(NiagaraSystemPtr);
			NiagaraComponent -> Activate();
		}
	}

	if (!StaticMeshComponent -> GetStaticMesh())
	{
		StaticMeshComponent -> DestroyComponent();
	}

	if (!IconWidgetComponent -> GetUserWidgetObject())
	{
		IconWidgetComponent -> DestroyComponent();
	}
}

void ATDS_HealthPickup::HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		TWeakObjectPtr<UTDS_PlayerHealthComponent> HealthComponent = UTDS_PlayerHealthComponent::GetPlayerHealthComponent(OtherActor);
		
		if (HealthComponent.IsValid())
		{
			if (HealthComponent.Get() -> IncreaseHealth(HealthPickupInfo))
			{
				Destroy();
			}
		}
	}
}




