﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_WeaponPickup.h"

#include "TDS_WeaponHandlerComponent.h"
#include "Engine/AssetManager.h"

// Sets default values
ATDS_WeaponPickup::ATDS_WeaponPickup()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ATDS_WeaponPickup::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	InitComponents();
}

void ATDS_WeaponPickup::InitComponents()
{
	if (FWeaponInfo* WeaponInfo = PickupInfo.GetInfo().GetRow<FWeaponInfo>(PickupInfo.GetInfo().RowName.ToString()))
	{
		FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
		const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnDataLoaded, PickupInfo.GetPickupMesh());
		Manager.RequestAsyncLoad(PickupInfo.GetPickupMesh().ToSoftObjectPath(), Delegate);
		WeaponPickupInfo = *WeaponInfo;
	}
}


void ATDS_WeaponPickup::OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh)
{
	if (Mesh.IsValid())
	{
		if (UStaticMesh* MeshPtr = Mesh.Get())
		{
			StaticMeshComponent -> SetStaticMesh(MeshPtr);
		}
	}

	if (!NiagaraComponent -> GetAsset())
	{
		NiagaraComponent -> DestroyComponent();
	}

	if (!IconWidgetComponent -> GetUserWidgetObject())
	{
		IconWidgetComponent -> DestroyComponent();
	}
}

void ATDS_WeaponPickup::HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		TWeakObjectPtr<UTDS_WeaponHandlerComponent> WeaponHandlerComponent = UTDS_WeaponHandlerComponent::GetWeaponHandlerComponent(OtherActor);
		
		if (WeaponHandlerComponent.IsValid())
		{
			if (WeaponHandlerComponent -> AddWeaponToInventory(this, WeaponPickupInfo))
			{
				Destroy();
			}
		}
	}
}
