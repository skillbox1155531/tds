﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_WeaponEffectPickup.h"

#include "TDS_WeaponHandlerComponent.h"
#include "Engine/AssetManager.h"

ATDS_WeaponEffectPickup::ATDS_WeaponEffectPickup()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATDS_WeaponEffectPickup::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	InitComponents();
}

void ATDS_WeaponEffectPickup::InitComponents()
{
	Super::InitComponents();

	if (FWeaponEffectInfo* WeaponEffectInfoPtr = PickupInfo.GetInfo().GetRow<FWeaponEffectInfo>(PickupInfo.GetInfo().RowName.ToString()))
	{
		if (FEffectInfo* EffectInfo = WeaponEffectInfoPtr -> GetInfo().GetRow<FEffectInfo>(WeaponEffectInfoPtr -> GetInfo().RowName.ToString()))
		{
			FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
			const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnDataLoaded, PickupInfo.GetPickupMesh(), EffectInfo -> GetEffectPickupEmitter());
			TArray<FSoftObjectPath> ObjectsToLoadArr;
			ObjectsToLoadArr.Emplace(PickupInfo.GetPickupMesh().ToSoftObjectPath());
			ObjectsToLoadArr.Emplace( EffectInfo -> GetEffectPickupEmitter().ToSoftObjectPath());
			
			Manager.RequestAsyncLoad(ObjectsToLoadArr, Delegate);
			WeaponEffectInfo = *WeaponEffectInfoPtr;
		}
	}
}

void ATDS_WeaponEffectPickup::HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		TWeakObjectPtr<UTDS_WeaponHandlerComponent> WeaponHandlerComponent = UTDS_WeaponHandlerComponent::GetWeaponHandlerComponent(OtherActor);

		if (WeaponHandlerComponent.IsValid())
		{
			if (FEffectInfo* EffectInfoPtr = WeaponEffectInfo.GetInfo().GetRow<FEffectInfo>(PickupInfo.GetInfo().RowName.ToString()))
			{
				if (WeaponHandlerComponent.Get() -> AddWeaponEffect(WeaponEffectInfo, EffectInfoPtr, WeaponEffectInfo.GetEffectClass()))
				{
					Destroy();
				}
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("%s::HandleStartOverlap: Couldn't load data"), *GetNameSafe(this));
			}
		}
	}
}

void ATDS_WeaponEffectPickup::OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh, TSoftObjectPtr<UNiagaraSystem> NiagaraSystem)
{
	if (Mesh.IsValid())
	{
		if (UStaticMesh* MeshPtr = Mesh.Get())
		{
			StaticMeshComponent -> SetStaticMesh(MeshPtr);
		}
	}
	
	if (NiagaraSystem.IsValid())
	{
		if (UNiagaraSystem* NiagaraSystemPtr = NiagaraSystem.Get())
		{
			NiagaraComponent -> SetAsset(NiagaraSystemPtr);
			NiagaraComponent -> Activate();
		}
	}

	if (!IconWidgetComponent -> GetUserWidgetObject())
	{
		IconWidgetComponent -> DestroyComponent();
	}
}

