﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_EffectPickup.h"

#include "TDS.h"
#include "TDS_CharacterBase.h"
#include "TDS_EffectsHandlerComponent.h"
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"

ATDS_EffectPickup::ATDS_EffectPickup()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATDS_EffectPickup::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	InitComponents();
}

void ATDS_EffectPickup::InitComponents()
{
	Super::InitComponents();

	if (FEffectInfo* EffectInfoPtr = PickupInfo.GetInfo().GetRow<FEffectInfo>(PickupInfo.GetInfo().RowName.ToString()))
	{
		FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
		const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnDataLoaded, EffectInfoPtr -> GetEffectPickupEmitter());
		Manager.RequestAsyncLoad(EffectInfoPtr -> GetEffectPickupEmitter().ToSoftObjectPath(), Delegate);
		EffectInfo = *EffectInfoPtr;
	}
}

void ATDS_EffectPickup::OnDataLoaded(TSoftObjectPtr<UNiagaraSystem> NiagaraSystem)
{
	if (NiagaraSystem.IsValid())
	{
		if (UNiagaraSystem* NiagaraSystemPtr = NiagaraSystem.Get())
		{
			NiagaraComponent -> SetAsset(NiagaraSystemPtr);
			NiagaraComponent -> Activate();
		}
	}

	if (!StaticMeshComponent -> GetStaticMesh())
	{
		StaticMeshComponent -> DestroyComponent();
	}

	if (!IconWidgetComponent -> GetUserWidgetObject())
	{
		IconWidgetComponent -> DestroyComponent();
	}
}

void ATDS_EffectPickup::HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// FString LogMsg = FString::Printf(TEXT("%s::HandleStartOverlap: Bump"), *GetNameSafe(this));
	// LogOnScreen(this, LogMsg, FColor::Green);
	
	if (OtherActor)
	{
		TWeakObjectPtr<UTDS_EffectsHandlerComponent> EffectsHandlerComponent = UTDS_EffectsHandlerComponent::GetEffectsHandlerComponent(OtherActor);

		if (EffectsHandlerComponent.IsValid())
		{
			if (FEffectInfo* EffectInfoPtr = PickupInfo.GetInfo().GetRow<FEffectInfo>(PickupInfo.GetInfo().RowName.ToString()))
			{
				EffectsHandlerComponent.Get() -> AddEffect(OtherActor, EffectInfoPtr, BaseEffectClass);
			}
		}
	}
}