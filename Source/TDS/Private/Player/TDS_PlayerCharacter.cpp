﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_PlayerCharacter.h"
#include "InputActionValue.h"
#include "TDS_EffectsHandlerComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "TDS_PlayerAnimInstance.h"
#include "TDS_StringLibrary.h"
#include "TDS_PlayerController.h"
#include "WTDS_PlayerHUD.h"
#include "TDS_WeaponHandlerComponent.h"
#include "TDS_PlayerHealthComponent.h"
#include "TDS_PlayerState.h"
#include "Components/BoxComponent.h"

ATDS_PlayerCharacter::ATDS_PlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;
	
	SetupCameraComponent();
	SetupCursorDecalComponent();
	SetupComponents();
	//SetupHitBoxComponent();

	StunTag = FGameplayTag::RequestGameplayTag(TEXT("Status.Stun"));
	Tags.Emplace(FTagLibrary::PlayerTag);
}

void ATDS_PlayerCharacter::SetupCameraComponent()
{
	if (!SpringArmComponent)
	{
		SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));

		if (SpringArmComponent)
		{
			SpringArmComponent -> SetUsingAbsoluteRotation(true);
			SpringArmComponent -> SetRelativeRotation(FRotator(-90.f, 0.f, 0.f));
			SpringArmComponent -> TargetArmLength = 1000.f;
			SpringArmComponent -> bDoCollisionTest = false;
			SpringArmComponent -> bEnableCameraLag = true;
			SpringArmComponent -> CameraLagSpeed = 2.f;
			SpringArmComponent -> SetupAttachment(RootComponent);
		}
	}

	if (SpringArmComponent && !CameraComponent)
	{
		CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

		if (CameraComponent)
		{
			CameraComponent -> SetupAttachment(SpringArmComponent, USpringArmComponent::SocketName);
			CameraComponent -> bUsePawnControlRotation = false;
		}
	}
}

void ATDS_PlayerCharacter::SetupCursorDecalComponent()
{
	if (!CursorDecalComponent)
	{
		CursorDecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("Mouse Cursor"));

		if (CursorDecalComponent)
		{
			CursorDecalComponent -> SetUsingAbsoluteLocation(true);
			CursorDecalComponent -> SetRelativeRotation(FRotator(90.f, 0.f, 0.f));
			CursorDecalComponent -> DecalSize = FVector(20.f, 32.f, 32.f);
			CursorDecalComponent -> SetupAttachment(RootComponent);
		}
	}
}

void ATDS_PlayerCharacter::SetupComponents()
{
	if (!WeaponHandlerComponent)
	{
		WeaponHandlerComponent = CreateDefaultSubobject<UTDS_WeaponHandlerComponent>(TEXT("Weapon Handler Component"));

		if (WeaponHandlerComponent)
		{
			AddOwnedComponent(WeaponHandlerComponent);
		}
	}

	if (!HealthComponent)
	{
		HealthComponent = CreateDefaultSubobject<UTDS_PlayerHealthComponent>(TEXT("Player Health Compomnent"));

		if (HealthComponent)
		{
			EffectApplicableComponentsArr.Emplace(HealthComponent);
			AddOwnedComponent(HealthComponent);
		}
	}

	if (GetCapsuleComponent())
	{
		GetCapsuleComponent() -> SetGenerateOverlapEvents(true);
		GetCapsuleComponent() -> OnComponentBeginOverlap.AddDynamic(this, &ThisClass::HandleStartOverlap);
	}
}

void ATDS_PlayerCharacter::InitComponents(const TWeakObjectPtr<UWTDS_PlayerHUD>& PlayerHUD)
{
	if (WeaponHandlerComponent)
	{
		if (PlayerHUD.Get() -> GetInventoryPanel().IsValid())
		{
			WeaponHandlerComponent -> InitComponent(PlayerHUD.Get() -> GetInventoryPanel(), PlayerHUD.Get() -> GetWeaponEffectsPanel(), PlayerHUD.Get() -> GetWeaponSwapNotifyPanel());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("ATDS_PlayerCharacter::InitComponents: InventoryPanel is NULL"));
		}
	}

	if (HealthComponent)
	{
		if (PlayerHUD.Get() -> GetPlayerInfoPanel().IsValid())
		{
			HealthComponent -> InitComponent(PlayerHUD.Get() -> GetPlayerInfoPanel());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("ATDS_PlayerCharacter::InitComponents: PlayerInfoPanel is NULL"));
		}
	}

	Super::InitComponents(PlayerHUD);
}

void ATDS_PlayerCharacter::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if (Cast<UTDS_PlayerAnimInstance>(GetMesh() -> GetAnimInstance()))
	{
		AnimInstance = Cast<UTDS_PlayerAnimInstance>(GetMesh() -> GetAnimInstance());
	}
}

void ATDS_PlayerCharacter::BindInputDelegates(const TWeakObjectPtr<ATDS_PlayerController>& PlayerController)
{
	if (PlayerController.IsValid())
	{
		PlayerController.Get() -> OnCharacterMove.BindUObject(this, &ThisClass::HandleMoveCharacter);
		PlayerController.Get() -> OnCharacterAim.BindUObject(this, &ThisClass::HandleCharacterAim);
		PlayerController.Get() -> OnCharacterFireWeapon.BindUObject(this, &ThisClass::HandleFireWeapon);
		PlayerController.Get() -> OnCharacterReloadWeapon.BindUObject(this, &ThisClass::HandleReloadWeapon);
		PlayerController.Get() -> OnCharacterSwitchWeapon.BindUObject(this, &ThisClass::HandleSwitchWeapon);
		PlayerController.Get() -> OnCharacterChangeHealth.BindUObject(this, &ThisClass::Test_HandleChangeHealth);
	}
}

void ATDS_PlayerCharacter::HandleMoveCharacter(const FInputActionValue& InputActionValue)
{
	if (EffectsHandlerComponent && !EffectsHandlerComponent -> HasTag(StunTag))
	{
		const FVector2D MovementVector = InputActionValue.Get<FVector2D>();

		if (Controller)
		{
			// const FRotator Rotation = Controller -> GetControlRotation();
			// const FRotator YawRotation(0, Rotation.Yaw, 0);
			// const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
			// const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

			AddMovementInput(FVector::ForwardVector, MovementVector.Y);
			AddMovementInput(FVector::RightVector, MovementVector.X);
		}
	}
}

void ATDS_PlayerCharacter::HandleCharacterAim(const FInputActionValue& InputActionValue)
{
	if (CurrentMovementState != EMovementState::MS_Stunned)
	{
		if (InputActionValue.Get<bool>())
		{
			SetMovementState(EMovementState::MS_Aim);
		}
		else
		{
			SetMovementState(EMovementState::MS_Walk);

			if (WeaponHandlerComponent)
			{
				WeaponHandlerComponent -> HandleStopCurrentWeaponFire();
			}
		}
	}
}

void ATDS_PlayerCharacter::HandleFireWeapon(const FInputActionValue& InputActionValue)
{
	if (InputEnabled())
	{
		const bool& ActionValue = InputActionValue.Get<bool>();
	
		if (WeaponHandlerComponent)
		{
			WeaponHandlerComponent -> HandleStartCurrentWeaponFire(ActionValue);
		}
	}
}

void ATDS_PlayerCharacter::HandleReloadWeapon(const FInputActionValue& InputActionValue)
{
	if (InputEnabled())
	{
		if (WeaponHandlerComponent)
		{
			WeaponHandlerComponent -> HandleCurrentWeaponReload();	
		}
	}
}

void ATDS_PlayerCharacter::HandleSwitchWeapon(const FInputActionValue& InputActionValue)
{
	if (InputEnabled())
	{
		if (WeaponHandlerComponent)
		{
			WeaponHandlerComponent -> SwitchWeapon(InputActionValue.Get<float>());
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("ATDS_PlayerCharacter::HandleSwitchWeapon: WeaponHandlerComponent is NULL"));
		}
	}
}

void ATDS_PlayerCharacter::Test_HandleChangeHealth(const FInputActionValue& InputActionValue)
{
	if (HealthComponent)
	{
		const float Value = InputActionValue.Get<float>();
		UE_LOG(LogTemp, Log, TEXT("ATDS_PlayerCharacter::Test_HandleChangeHealth: %f"), Value);
		Value > 0 ? HealthComponent -> IncreaseHealthPoints(15.f) : HealthComponent -> DecreaseHealth(15.f);
	}
}

void ATDS_PlayerCharacter::ShowDamageVisual(const float& Value)
{

}

void ATDS_PlayerCharacter::DeathLogic(AActor* DamageCauser, APlayerController* PlayerController)
{
	DisableInput(PlayerController);
				
	if (ensure(GetCapsuleComponent()))
	{
		GetMesh() -> SetAllBodiesSimulatePhysics(true);
		GetMesh() -> SetCollisionProfileName(TEXT("Ragdoll"));
		GetCapsuleComponent() -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	if (EffectsHandlerComponent)
	{
		EffectsHandlerComponent -> InterruptAllEffects();
	}

	WeaponHandlerComponent -> InterruptWeaponEffect();

	if (ATDS_PlayerState* PS = GetPlayerState<ATDS_PlayerState>())
	{
		PS -> UpdateOwnedWeapons(WeaponHandlerComponent -> GetOwnedWeapons());
		PS -> PlayerDied(DamageCauser);
	}
}

float ATDS_PlayerCharacter::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent,
                                       AController* EventInstigator, AActor* DamageCauser)
{
	if (HealthComponent && HealthComponent -> GetIsAlive())
	{
		const float Value = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
		HealthComponent -> DecreaseHealth(Value);

		if (!HealthComponent -> GetIsAlive())
		{
			if (APlayerController* PlayerController = Cast<APlayerController>(GetController()))
			{
				DeathLogic(DamageCauser, PlayerController);
			}
		}
	}

	return 0.f;
}

void ATDS_PlayerCharacter::HandleStartOverlap( UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex,
                                               bool bFromSweep, const FHitResult& SweepResult)
{
}

void ATDS_PlayerCharacter::PlayMontage(const TWeakObjectPtr<UAnimMontage>& Montage)
{
	if (AnimInstance.IsValid() && Montage.IsValid())
	{
		if (AnimInstance.Get() -> Montage_Play(Montage.Get()) == 0.f)
		{
			UE_LOG(LogTemp, Error, TEXT("ATDS_PlayerCharacter::HandleReloadWeapon: Failed to play Montage"));
		}
	}
}

void ATDS_PlayerCharacter::StopMontage(const TWeakObjectPtr<UAnimMontage>& Montage)
{
	if (AnimInstance.IsValid() && Montage.IsValid())
	{
		AnimInstance.Get() -> Montage_Stop(Montage.Get() -> BlendOut.GetBlendTime(), Montage.Get());
	}
}

void ATDS_PlayerCharacter::StopAllMontages()
{
	if (AnimInstance.IsValid())
	{
		AnimInstance.Get() -> Montage_Stop(.2f);
	}
}

TWeakObjectPtr<UTDS_PlayerAnimInstance> ATDS_PlayerCharacter::GetPlayerAnimInstance() const
{
	return AnimInstance;
}

TWeakObjectPtr<USpringArmComponent> ATDS_PlayerCharacter::GetSpringArmComponent() const
{
	return TWeakObjectPtr<USpringArmComponent>(SpringArmComponent);
}

TWeakObjectPtr<UDecalComponent> ATDS_PlayerCharacter::GetCursorDecalComponent() const
{
	return TWeakObjectPtr<UDecalComponent>(CursorDecalComponent);
}

TWeakObjectPtr<UTDS_WeaponHandlerComponent> ATDS_PlayerCharacter::GetWeaponHandlerComponent() const
{
	return WeaponHandlerComponent;
}

TWeakObjectPtr<UTDS_PlayerHealthComponent> ATDS_PlayerCharacter::GetHealthComponent() const
{
	return HealthComponent;
}

FDataTableRowHandle ATDS_PlayerCharacter::GetDefaultWeaponID() const
{
	return DefaultWeaponID;
}

FVector ATDS_PlayerCharacter::GetActorLocationInt_Implementation()
{
	return this -> GetActorLocation();
}

bool ATDS_PlayerCharacter::GetIsAliveInt_Implementation()
{
	return GetHealthComponent() -> GetIsAlive();
}

TArray<EWeaponAmmoType> ATDS_PlayerCharacter::GetOwnedAmmoTypes_Implementation()
{
	if (WeaponHandlerComponent)
	{
		return WeaponHandlerComponent -> GetOwnedAmmoTypes();
	}

	return TArray<EWeaponAmmoType>();
}

