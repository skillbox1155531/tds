﻿
#include "TDS_PlayerState.h"
#include "TDS_GameState.h"
#include "Kismet/GameplayStatics.h"

ATDS_PlayerState::ATDS_PlayerState()
{
	PlayerLives = 3;
	PlayerScore = 0;
}

void ATDS_PlayerState::PostInitializeComponents()
{
	Super::PostInitializeComponents();
}

void ATDS_PlayerState::PlayerDied(AActor* Killer)
{
	PlayerLives--;
	
	if (ATDS_GameState* GS = Cast<ATDS_GameState>(UGameplayStatics::GetGameState(this)))
	{
		GS -> OnActorKilled(GetPawn(), Killer);
	}

	OnLivesChanged.Broadcast(PlayerLives);
}

void ATDS_PlayerState::IncreaseScore(const int32& Amount)
{
	PlayerScore += Amount;

	OnScoreChanged.Broadcast(PlayerScore);
}

void ATDS_PlayerState::UpdateOwnedWeapons(const TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>>& OwnedWeapons)
{
	if (!OwnedWeapons.IsEmpty())
	{
		for (TTuple<FWeaponInfo, UTDS_WeaponData*> Weapon : OwnedWeapons)
		{
			OwnedWeaponsDic.Emplace(Weapon.Key, Weapon.Value);
		}
	}
}

TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>> ATDS_PlayerState::GetOwnedWeapons()
{
	TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>> OwnedWeapons;

	for (TTuple<FWeaponInfo, UTDS_WeaponData*> Weapon : OwnedWeaponsDic)
	{
		OwnedWeapons.Emplace(Weapon);
	}

	OwnedWeaponsDic.Empty();
	
	return OwnedWeapons;
}

int32 ATDS_PlayerState::GetPlayerScore() const
{
	return PlayerScore;
}

int32 ATDS_PlayerState::GetPlayerLives() const
{
	return PlayerLives;
}


