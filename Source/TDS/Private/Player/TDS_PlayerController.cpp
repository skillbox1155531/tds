﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_PlayerController.h"

#include "TDS_EffectsHandlerComponent.h"
#include "TDS_InputHandlerComponent.h"
#include "TDS_PlayerAnimInstance.h"
#include "TDS_PlayerCharacter.h"
#include "TDS_StringLibrary.h"
#include "WTDS_PlayerHUD.h"
#include "Blueprint/UserWidget.h"
#include "Components/DecalComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Kismet/KismetMathLibrary.h"

ATDS_PlayerController::ATDS_PlayerController()
{
	PrimaryActorTick.bCanEverTick = true;

	bIsUIModeEnabled = false;
	CachedMouseLoc = FVector2D::ZeroVector;
	
	DistanceThresholdMultiplayer = 0.5f;
	CameraDirectionMultiplayer = 10.f;
	CameraDistanceTolerance = 5.5f;
	CameraZeroDistance = 0.f;

	StunTag = FGameplayTag::RequestGameplayTag(TEXT("Status.Stun"));
	
	SetupComponents();
}

void ATDS_PlayerController::SetupComponents()
{
	if (!InputHandlerComponent)
	{
		InputHandlerComponent = CreateDefaultSubobject<UTDS_InputHandlerComponent>(TEXT("Input Handler"));
		AddOwnedComponent(InputHandlerComponent);
	}
}

void ATDS_PlayerController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	UE_LOG(LogTemp, Warning, TEXT("ATDS_PlayerController::OnPossess"));
	
	PlayerCharacterPtr = Cast<ATDS_PlayerCharacter>(InPawn);

	if (PlayerCharacterPtr.IsValid())
	{
		PlayerCharacterPtr.Get() -> BindInputDelegates(TWeakObjectPtr<ATDS_PlayerController>(this));
		PlayerAnimInstancePtr = PlayerCharacterPtr.Get() -> GetPlayerAnimInstance();
		SpringArmComponentPtr = PlayerCharacterPtr.Get() -> GetSpringArmComponent();
		CursorDecalPtr = PlayerCharacterPtr.Get() -> GetCursorDecalComponent();

		if (!PlayerHUD)
		{
			if (ensure(PlayerHUDClass))
			{
				PlayerHUD = CreateWidget<UWTDS_PlayerHUD>(this, PlayerHUDClass);
				PlayerCharacterPtr.Get() -> InitComponents(PlayerHUD);
				PlayerCharacterPtr.Get() -> OnChangeInputMode.BindUObject(this, &ATDS_PlayerController::SetGameInputMode);	
			}
		}
		else
		{
			PlayerCharacterPtr.Get() -> InitComponents(PlayerHUD);
			PlayerCharacterPtr.Get() -> OnChangeInputMode.BindUObject(this, &ATDS_PlayerController::SetGameInputMode);
		}
	}
}

void ATDS_PlayerController::BeginPlay()
{
	Super::BeginPlay();

	FInputModeGameOnly InputMode;
	InputMode.SetConsumeCaptureMouseDown(false);
	SetInputMode(InputMode);
	SetShowMouseCursor(false);

	PlayerHUD -> AddToViewport();
}

void ATDS_PlayerController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	UpdateAnimParameters();
	
	if (PlayerCharacterPtr.IsValid() && !PlayerCharacterPtr.Get() -> GetEffectsHandlerComponent() -> HasTag(StunTag))
	{
		if (!bIsUIModeEnabled)
		{
			UpdateCursorPosition();
			TrackCursorDistance();
		}
	}
}

void ATDS_PlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	UE_LOG(LogTemp, Display, TEXT("ATDS_PlayerController::SetPawn"));
}

void ATDS_PlayerController::SetGameInputMode(const bool& bIsGameModeOnly)
{
	bIsGameModeOnly ? SetGameOnlyMode() : SetGameUIOnlyMode();
}

void ATDS_PlayerController::SetGameOnlyMode()
{
	SetMouseLocation(CachedMouseLoc.X, CachedMouseLoc.Y);
	
	FInputModeGameOnly InputMode;
	InputMode.SetConsumeCaptureMouseDown(false);
	
	SetIgnoreMoveInput(false);
	SetInputMode(InputMode);
	SetShowMouseCursor(false);
	bIsUIModeEnabled = false;
}

void ATDS_PlayerController::SetGameUIOnlyMode()
{
	GetMousePosition(CachedMouseLoc.X, CachedMouseLoc.Y);
	
	FInputModeUIOnly InputMode;

	SetIgnoreMoveInput(true);
	SetInputMode(InputMode);
	SetShowMouseCursor(true);
	bIsUIModeEnabled = true;
}

void ATDS_PlayerController::UpdateAnimParameters() const
{
	if (PlayerCharacterPtr.IsValid() && PlayerAnimInstancePtr.IsValid())
	{
		const FVector Velocity = PlayerCharacterPtr.Get() -> GetVelocity();
		const FRotator Direction = PlayerCharacterPtr.Get() -> GetActorRotation();
		const EMovementState MovementState = PlayerCharacterPtr.Get() -> GetMovementState();
		
		PlayerAnimInstancePtr.Get() -> SetVelocityParam(Velocity);
		PlayerAnimInstancePtr.Get() -> SetMovementDirParam(Velocity, Direction);
		PlayerAnimInstancePtr.Get() -> SetMovementStateParam(MovementState);
	}
}

void ATDS_PlayerController::UpdateCursorPosition()
{
	 FHitResult HitResult;
	
	 if (GetHitResultUnderCursor(ECC_GameTraceChannel1, false, HitResult))
	 {
	 	if (CursorDecalPtr.IsValid())
	 	{
	 		CursorDecalPtr.Get() -> SetWorldLocation(HitResult.Location);
	 		CursorDecalPtr.Get() -> SetWorldRotation(HitResult.Normal.Rotation());
	 		UpdateCharRotationToCursor(HitResult.Location);
	 	}
	}
}

void ATDS_PlayerController::UpdateCharRotationToCursor(const FVector& HitLocation)
{
	if (PlayerCharacterPtr.IsValid())
	{
		if (PlayerCharacterPtr.Get() -> GetMovementState() != EMovementState::MS_Stunned)
		{
			const FVector Location = ITDS_Actor::Execute_GetActorLocationInt(GetPawn());
			const FVector CharLoc = FVector(Location.X, Location.Y, 0.f);
			LookAt = UKismetMathLibrary::FindLookAtRotation(CharLoc,  FVector(HitLocation.X, HitLocation.Y, 0.f));
		}
				
		PlayerCharacterPtr.Get() -> SetActorRotation(LookAt);
	}
}

void ATDS_PlayerController::TrackCursorDistance() const
{
	if (PlayerCharacterPtr.IsValid() && CursorDecalPtr.IsValid() && SpringArmComponentPtr.IsValid())
	{
		const FVector2d CharLoc = FVector2d(ITDS_Actor::Execute_GetActorLocationInt(GetPawn()));
		const FVector2d DecalLoc = FVector2d(CursorDecalPtr.Get() -> GetComponentLocation());
		const FVector CamLoc = SpringArmComponentPtr.Get() -> GetComponentLocation();

		const float CamToCharDistance = FVector2d::Distance(CharLoc, FVector2d(CamLoc));
		const float CharToDecalDistance = FVector2d::Distance(CharLoc, DecalLoc);
		
		const float CursorDistanceThreshold = SpringArmComponentPtr.Get() -> TargetArmLength * DistanceThresholdMultiplayer;
		const float CameraDistanceThreshold = CursorDistanceThreshold * DistanceThresholdMultiplayer;

		if (PlayerCharacterPtr.Get() -> GetMovementState() == EMovementState::MS_Aim)
		{
			if (CharToDecalDistance >= CursorDistanceThreshold)
			{
				const FVector Direction = FRotationMatrix(FRotator(0.f, PlayerCharacterPtr.Get() -> GetActorRotation().Yaw, 0.f)).GetUnitAxis(EAxis::X);
				
				CamToCharDistance < CameraDistanceThreshold
					? SpringArmComponentPtr.Get() -> SetWorldLocation(FVector(CamLoc + (Direction * CameraDirectionMultiplayer)))
					: SpringArmComponentPtr.Get() -> SetWorldLocation(FVector(CamLoc - (Direction * CameraDirectionMultiplayer)));
			}
			else
			{
				SpringArmComponentPtr.Get() -> SetWorldLocation(FVector(CharLoc.X, CharLoc.Y, CamLoc.Z));
			}
		}
		else
		{
			SpringArmComponentPtr.Get() -> SetWorldLocation(FVector(CharLoc.X, CharLoc.Y, CamLoc.Z));
		}
	}
}
