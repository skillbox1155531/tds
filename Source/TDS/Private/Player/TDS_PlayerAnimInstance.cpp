﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_PlayerAnimInstance.h"
#include "KismetAnimationLibrary.h"

void UTDS_PlayerAnimInstance::SetVelocityParam(const FVector& Velocity)
{
	MovementSpeed = Velocity.Size();
}

void UTDS_PlayerAnimInstance::SetMovementDirParam(const FVector& Velocity, const FRotator& Rotation)
{
	MovementDirection = UKismetAnimationLibrary::CalculateDirection(Velocity, Rotation);
}

void UTDS_PlayerAnimInstance::SetMovementStateParam(const EMovementState& State)
{
	MovementState = State;
}
