﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_GameMode.h"
#include "EngineUtils.h"
#include "TDS.h"
#include "TDS_EnemyData.h"
#include "TDS_EnemyCharacter.h"
#include "TDS_GameState.h"
#include "TDS_PlayerStart.h"
#include "WTDS_RespawnCountDown.h"
#include "Blueprint/UserWidget.h"
#include "Engine/AssetManager.h"
#include "EnvironmentQuery/EnvQueryInstanceBlueprintWrapper.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "Kismet/GameplayStatics.h"

ATDS_GameMode::ATDS_GameMode()
{
	CurrentCheckpointIndex = 0;
	RespawnDelay = 5.f;
	SpawnEnemyQuery = nullptr;
	EnemiesDataTable = nullptr;
	CurrentEnemiesWeight = 0;
	MaxEnemiesWeight = 50;
	CurrentPhaseIndex = 0;
	BossSpawnDelay = 10.f;
	bIsBossPhase = false;
}

void ATDS_GameMode::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	TWeakObjectPtr<ATDS_PlayerStart> Checkpoint = GetCheckpoint();
	
	if (Checkpoint.IsValid())
	{
		NewPlayer -> StartSpot = Cast<AActor>(Checkpoint .Get());	
	}
	
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);
}

void ATDS_GameMode::StartPlay()
{
	if (InitializeEnemiesData())
	{
		SetEnemySpawnTimer();
	}

	if (ATDS_GameState* GS = GetGameState<ATDS_GameState>())
	{
		UE_LOG(LogTemp, Warning, TEXT("ATDS_GameMode::StartPlay"));
		GS -> SetBossSpawnDelay(BossSpawnDelay);
		GS -> OnEnemyKilled_Delegate.BindUObject(this, &ThisClass::OnEnemyBeingKilled);
		GS -> OnPlayerKilled_Delegate.BindUObject(this, &ThisClass::RespawnPlayer);
		GS -> OnGameOver_Delegate.BindUObject(this, &ThisClass::GameOver);
		GS -> OnGamePhaseChanged_Delegate.AddDynamic(this, &ThisClass::SetCurrentPhase);
		GS -> OnBossPhaseStarted_Delegate.AddDynamic(this, &ThisClass::SetSpawnBossTimer);
	}

	Super::StartPlay();
}

bool ATDS_GameMode::InitializeEnemiesData()
{
	if (!EnemiesDataTable)
	{
		return false;
	}

	TArray<FPhaseInfo*> PhasesDataRows;

	EnemiesDataTable -> GetAllRows("", PhasesDataRows);

	if (!PhasesDataRows.IsEmpty())
	{
		for (FPhaseInfo* PhaseDataRow : PhasesDataRows)
		{
			if (PhaseDataRow -> PhaseIndex == CurrentPhaseIndex)
			{
				for (auto Enemy : PhaseDataRow -> Enemies)
				{
					EnemiesInfoArr.Emplace(*Enemy.GetRow<FEnemyInfo>(Enemy.RowName.ToString()));
				}
				
				break;
			}
		}
	}
	
	return !EnemiesInfoArr.IsEmpty();
}

AActor* ATDS_GameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	AActor* PlayerStart = nullptr;
	
	for (TActorIterator<ATDS_PlayerStart> It(GetWorld()); It; ++It)
	{
		if (*It)
		{
			CheckpointsArr.Emplace(It -> GetProgressIndex(), *It);

			if (It -> GetProgressIndex() == 0)
			{
				PlayerStart = *It;
			}
		}
	}
	
	return PlayerStart;
}

void ATDS_GameMode::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);
}

void ATDS_GameMode::SetEnemySpawnTimer()
{
	GetWorld() -> GetTimerManager().SetTimer(EnemySpawn_TimerHandle, this, &ThisClass::SpawnEnemies, 5.f, true, 0.f);
}

void ATDS_GameMode::SpawnEnemies()
{
	if (bIsBossPhase)
	{
		GetWorld() -> GetTimerManager().ClearTimer(EnemySpawn_TimerHandle);
		return;
	}
	
	if (CurrentEnemiesWeight == MaxEnemiesWeight)
	{
		//LogOnScreen(this, TEXT("ATDS_GameMode::SpawnEnemies: No more enemies can be spawned"), FColor::Yellow);
		return;
	}

	if (ensureMsgf(SpawnEnemyQuery, TEXT("ATDS_GameMode::SpawnEnemies: SpawnBotQuery is NULL")))
	{
		if (UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(this, SpawnEnemyQuery, this, EEnvQueryRunMode::AllMatching, nullptr))
		{
			QueryInstance -> GetOnQueryFinishedEvent().AddDynamic(this, &ThisClass::OnEnemyQueryCompleted);
		}
	}
}

void ATDS_GameMode::SetSpawnBossTimer()
{
	bIsBossPhase = true;
	GetWorld() -> GetTimerManager().SetTimer(BossSpawn_TimerHandle, this, &ThisClass::SpawnBossTimerElapsed, BossSpawnDelay, false);
}

void ATDS_GameMode::SpawnBossTimerElapsed()
{
	if (ensureMsgf(SpawnEnemyQuery, TEXT("ATDS_GameMode::SpawnEnemies: SpawnBotQuery is NULL")))
	{
		if (UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(this, SpawnEnemyQuery, this, EEnvQueryRunMode::RandomBest5Pct, nullptr))
		{
			QueryInstance -> GetOnQueryFinishedEvent().AddDynamic(this, &ThisClass::OnBossQueryCompleted);
		}
	}
}

void ATDS_GameMode::OnEnemyQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		//LogOnScreen(this, TEXT("ATDS_GameMode::OnEnemyQueryCompleted: EEnvQueryStatus::Failed"), FColor::Red);
		return;
	}

	TArray<FVector> LocationsArr;

	QueryInstance -> GetQueryResultsAsLocations(LocationsArr);
	
	if (!LocationsArr.IsEmpty())
	{
		TArray<FPrimaryAssetId> EnemyAssetIDs;

		int32 ExpectedEnemiesWeight = CurrentEnemiesWeight;
		
		while (ExpectedEnemiesWeight < MaxEnemiesWeight)
		{
			const int32 RandomIndex = FMath::RandRange(0, EnemiesInfoArr.Num() - 1);
			ExpectedEnemiesWeight += EnemiesInfoArr[RandomIndex].Weight;
			EnemyAssetIDs.Emplace(EnemiesInfoArr[RandomIndex].EnemyData);
		}

		if (!EnemyAssetIDs.IsEmpty())
		{
			if (UAssetManager* Manager = UAssetManager::GetIfInitialized())
			{
				TArray<FName> BundlesArr;
				FStreamableDelegate StreamableDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnEnemiesDataLoaded, EnemyAssetIDs, LocationsArr);
				Manager -> LoadPrimaryAssets(EnemyAssetIDs, BundlesArr, StreamableDelegate);
			}
		}
	}
}

void ATDS_GameMode::OnBossQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance,
	EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		//LogOnScreen(this, TEXT("ATDS_GameMode::OnEnemyQueryCompleted: EEnvQueryStatus::Failed"), FColor::Red);
		return;
	}

	TArray<FVector> LocationsArr;

	QueryInstance -> GetQueryResultsAsLocations(LocationsArr);

	if (!LocationsArr.IsEmpty())
	{
		if (UAssetManager* Manager = UAssetManager::GetIfInitialized())
		{
			TArray<FName> BundlesArr;
			FPrimaryAssetId AssetId = BossInfo.GetRow<FEnemyInfo>(BossInfo.RowName.ToString()) -> EnemyData;
			
			FStreamableDelegate StreamableDelegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnBossDataLoaded, AssetId, LocationsArr);
			Manager -> LoadPrimaryAsset(AssetId, BundlesArr, StreamableDelegate);
		}
	}
}

void ATDS_GameMode::OnEnemiesDataLoaded(TArray<FPrimaryAssetId> EnemyAssetIDs, TArray<FVector> LocationsArr)
{
	if (!EnemyAssetIDs.IsEmpty())
	{
		if (const UAssetManager* Manager = UAssetManager::GetIfInitialized())
		{
			for (int i = 0; i < EnemyAssetIDs.Num(); ++i)
			{
				if (const UTDS_EnemyData* EnemyData = Cast<UTDS_EnemyData>(Manager -> GetPrimaryAssetObject(EnemyAssetIDs[i])))
				{
					FEnemyInfo EnemyInfo = GetEnemyInfo(EnemyAssetIDs[i]);

					if (ATDS_EnemyCharacter* Enemy = GetWorld() -> SpawnActor<ATDS_EnemyCharacter>(EnemyData -> EnemyClass, LocationsArr[i], FRotator::ZeroRotator))
					{
						Enemy -> ID = FGuid::NewGuid();
						Enemy -> Weight = EnemyInfo.Weight;
						Enemy -> KillPoints = EnemyInfo.KillPoints;
						Enemy -> bIsBoss = false;
						
						CurrentEnemiesWeight = FMath::Min(CurrentEnemiesWeight + EnemyInfo.Weight, MaxEnemiesWeight);
						SpawnedEnemiesDic.Emplace(Enemy -> ID, Enemy);

						FString DebugMsg = FString::Printf(TEXT("ATDS_GameMode::OnEnemiesDataLoaded: %s was spawned"), *GetNameSafe(Enemy));
						LogOnScreen(this, DebugMsg, FColor::Yellow);
					}
				}
			}
		}
	}
}

void ATDS_GameMode::OnBossDataLoaded(FPrimaryAssetId Id, TArray<FVector> Locations)
{
	if (const UAssetManager* Manager = UAssetManager::GetIfInitialized())
	{
		if (UTDS_EnemyData* BossData = Cast<UTDS_EnemyData>(Manager -> GetPrimaryAssetObject(Id)))
		{
			if (ATDS_EnemyCharacter* Boss = GetWorld() -> SpawnActor<ATDS_EnemyCharacter>(BossData -> EnemyClass, Locations[0], FRotator::ZeroRotator))
			{
				if (DespawnNonBossEnemies())
				{
					Boss -> ID = FGuid::NewGuid();
					Boss -> Weight = 0;
					Boss -> KillPoints = 0;
					Boss -> bIsBoss = true;
				
					SpawnedEnemiesDic.Empty();
					SpawnedEnemiesDic.Emplace(Boss -> ID, Boss);
				}
			}
		}
	}
}

bool ATDS_GameMode::DespawnNonBossEnemies()
{
	if (!SpawnedEnemiesDic.IsEmpty())
	{
		for (auto Enemy : SpawnedEnemiesDic)
		{
			if (Enemy.Value && !Enemy.Value -> bIsBoss)
			{
				Enemy.Value -> Destroy();
			}
		}

		return true;
	}

	return true;
}

void ATDS_GameMode::CreateWinMenu()
{
	UGameplayStatics::SetGamePaused(this, true);

	if (ensure(WinMenuClass))
	{
		if (UUserWidget* WinMenu = CreateWidget<UUserWidget>(GetWorld(), WinMenuClass))
		{
			WinMenu -> AddToViewport(1);
		}
	}
}

FEnemyInfo ATDS_GameMode::GetEnemyInfo(FPrimaryAssetId AssetID)
{
	for (FEnemyInfo Enemy : EnemiesInfoArr)
	{
		if (Enemy.EnemyData == AssetID)
		{
			return Enemy;
		}
	}

	return FEnemyInfo();
}

void ATDS_GameMode::OnEnemyBeingKilled(const FGuid& ID, const int32& NewEnemiesSpawnMaxWeight)
{
	for (TTuple<FGuid, ATDS_EnemyCharacter*> Enemy : SpawnedEnemiesDic)
	{
		if (Enemy.Key == ID)
		{
			// UE_LOG(LogTemp, Warning, TEXT("ATDS_GameMode::OnEnemyBeingKilled: %s was killed"), *GetNameSafe(Enemy.Value));
			ATDS_EnemyCharacter* EnemyCharacter = Enemy.Value;

			CurrentEnemiesWeight -= EnemyCharacter -> Weight;
			SpawnedEnemiesDic.Remove(Enemy.Key);

			if (EnemyCharacter -> bIsBoss)
			{
				GetWorld() -> GetTimerManager().SetTimer(WinMenu_TimerHandle, this, &ThisClass::CreateWinMenu, 3.f, false);
			}
			
			if (EnemyCharacter)
			{
				FTimerHandle EnemyDestroy_TimerHandle;
				GetWorld() -> GetTimerManager().SetTimer(EnemyDestroy_TimerHandle, [&, EnemyCharacter] { EnemyCharacter -> SetActorHiddenInGame(true); }, 2.f, false);
			}

			break;
		}
	}

	MaxEnemiesWeight = NewEnemiesSpawnMaxWeight;
}

void ATDS_GameMode::OnEnemiesSpawnMaxWeightUpdated(const int32& NewWeight)
{
	MaxEnemiesWeight = NewWeight;
	UE_LOG(LogTemp, Warning, TEXT("ATDS_GameMode::OnEnemiesSpawnMaxWeightUpdated: New max weight: %i"), MaxEnemiesWeight)
}

void ATDS_GameMode::RespawnPlayer(AActor* Player, APlayerController* PlayerController)
{
	TWeakObjectPtr<ATDS_PlayerStart> Checkpoint = GetCheckpoint();
		
	if (Checkpoint.IsValid())
	{
		PlayerController -> StartSpot = Cast<AActor>(Checkpoint.Get());

		if (UWTDS_RespawnCountDown* CDTimer = SetupRespawnCountDownTimer())
		{
			FTimerHandle Destroy_TimerHandle;
			GetWorld() -> GetTimerManager().SetTimer(Destroy_TimerHandle, [&, Player, PlayerController, CDTimer]
			{
				PlayerController -> UnPossess();
				RestartPlayer(PlayerController);
				CDTimer -> ClearTimer();
				CDTimer -> RemoveFromParent();
				Player -> Destroy();
					
			}, RespawnDelay, false);
		}
	}
}

UWTDS_RespawnCountDown* ATDS_GameMode::SetupRespawnCountDownTimer()
{
	if (ensure(RespawnCountDownClass))
	{
		if (UWTDS_RespawnCountDown* CDTimer = CreateWidget<UWTDS_RespawnCountDown>(GetWorld(), RespawnCountDownClass))
		{
			CDTimer -> SetTimer(RespawnDelay);
			CDTimer -> AddToViewport(1);

			return CDTimer;
		}
	}

	return nullptr;
}

void ATDS_GameMode::GameOver()
{
	if (ensure(GameOverMenuClass))
	{
		if (UUserWidget* GameOverMenu = CreateWidget(GetWorld(), GameOverMenuClass))
		{
			GameOverMenu -> AddToViewport(1);
		}
	}
}

void ATDS_GameMode::CreateGameOverMenu()
{
	
}

void ATDS_GameMode::SetCurrentProgressIndex(const int32& Index)
{
	CurrentCheckpointIndex = Index;
}

int32 ATDS_GameMode::GetCurrentProgressIndex() const
{
	return CurrentCheckpointIndex;
}

TWeakObjectPtr<ATDS_PlayerStart> ATDS_GameMode::GetCheckpoint() const
{
	if (!CheckpointsArr.IsEmpty())
	{
		for (TTuple<int, TWeakObjectPtr<ATDS_PlayerStart>> Checkpoint : CheckpointsArr)
		{
			if (Checkpoint.Key == CurrentCheckpointIndex)
			{
				return Checkpoint.Value;
			}
		}
	}

	return nullptr;
}

void ATDS_GameMode::SetCurrentPhase(int32 NewPhase)
{
	CurrentPhaseIndex = NewPhase;

	InitializeEnemiesData();
}
