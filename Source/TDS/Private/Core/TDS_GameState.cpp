﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_GameState.h"
#include "TDS_EnemyCharacter.h"
#include "TDS_PlayerState.h"
#include "TDS_StringLibrary.h"


void ATDS_GameState::BeginPlay()
{
	Super::BeginPlay();

	
}

void ATDS_GameState::OnActorKilled(AActor* Victim, AActor* Killer)
{
	if (Killer && Killer -> ActorHasTag(FTagLibrary::PlayerTag))
	{
		//UE_LOG(LogTemp, Display, TEXT("ATDS_GameMode::OnActorKilled: %s killed by %s"), *GetNameSafe(Victim), *GetNameSafe(Killer));
		IncreasePlayerScore(Victim, Killer);
	}

	if (Victim && Victim -> ActorHasTag(FTagLibrary::PlayerTag))
	{
		HandleRespawnPlayer(Victim);
	}
}

void ATDS_GameState::IncreasePlayerScore(const AActor* Enemy, AActor* Player)
{
	if (const ATDS_EnemyCharacter* EnemyCharacter = Cast<ATDS_EnemyCharacter>(Enemy))
	{
		if (APawn* PlayerPawn = Cast<APawn>(Player))
		{
			if (ATDS_PlayerState* PlayerState = PlayerPawn -> GetPlayerState<ATDS_PlayerState>())
			{
				PlayerState -> IncreaseScore(EnemyCharacter -> KillPoints);

				if (ensureMsgf(EnemiesSpawnWeightCurve, TEXT("ATDS_GameState::IncreasePlayerScore: EnemiesSpawnWeightCurve is NULL")))
				{
					const int32 NewEnemiesMaxSpawnWeight = FMath::RoundToInt32(EnemiesSpawnWeightCurve -> GetFloatValue(PlayerState -> GetPlayerScore()));

					if (!OnEnemyKilled_Delegate.ExecuteIfBound(EnemyCharacter -> ID, NewEnemiesMaxSpawnWeight))
					{
						UE_LOG(LogTemp, Error, TEXT("ATDS_GameState::OnActorKilled: OnEnemyKilled isn't bound"));
					}

					if (!bIsBossPhaseStarted)
					{
						HandleGamePhase(0, PlayerState -> GetPlayerScore());
					}
				}
			}
		}
	}
}

void ATDS_GameState::HandleGamePhase(const int32& PhaseIndex, const int32& CurrentScore)
{
	bIsBossPhaseStarted = LevelPhasesArr.IsEmpty();
	
	if (!bIsBossPhaseStarted)
	{
		if (LevelPhasesArr.IsValidIndex(PhaseIndex))
		{
			if (LevelPhasesArr[PhaseIndex].Score <= CurrentScore)
			{
				LevelPhasesArr.RemoveAt(PhaseIndex);
				HandleGamePhase(PhaseIndex, CurrentScore);
			}
			else
			{
				if (CurrentPhaseIndex != LevelPhasesArr[PhaseIndex].PhaseIndex)
				{
					CurrentPhaseIndex = LevelPhasesArr[PhaseIndex].PhaseIndex;
					OnGamePhaseChanged_Delegate.Broadcast(CurrentPhaseIndex);
				}
			}
		}
	}
	else
	{
		OnBossPhaseStarted_Delegate.Broadcast();
	}
}

void ATDS_GameState::HandleRespawnPlayer(AActor* Player)
{
	if (APawn* PlayerPawn = Cast<APawn>(Player))
	{
		if (ATDS_PlayerState* PlayerState = PlayerPawn -> GetPlayerState<ATDS_PlayerState>())
		{
			if (PlayerState -> GetPlayerLives() > 0)
			{
				if (!OnPlayerKilled_Delegate.ExecuteIfBound(Player, Cast<APlayerController>(PlayerPawn -> GetController())))
				{
					UE_LOG(LogTemp, Error, TEXT("ATDS_GameState::OnActorKilled: OnPlayerKilledDelegate isn't bound"));
				}
			}
			else
			{
				if (!OnGameOver_Delegate.ExecuteIfBound())
				{
					UE_LOG(LogTemp, Error, TEXT("ATDS_GameState::OnActorKilled: OnGameOverDelegate isn't bound"));
				}
			}
		}
	}
}

TArray<APawn*> ATDS_GameState::GetPlayerCharacters()
{
	return PlayerPawnsArr;
}

void ATDS_GameState::SetBossSpawnDelay(const int32& NewBossSpawnDelay)
{
	BossSpawnDelay = NewBossSpawnDelay;
}

void ATDS_GameState::AddPlayerState(APlayerState* PlayerState)
{
	Super::AddPlayerState(PlayerState);

	if (PlayerState)
	{
		UE_LOG(LogTemp, Warning, TEXT("ATDS_GameState::AddPlayerState: Adding %s"), *GetNameSafe(PlayerState))
		PlayerState -> OnPawnSet.AddDynamic(this, &ThisClass::OnNewPawnSetToPlayerState);
	}
}

void ATDS_GameState::RemovePlayerState(APlayerState* PlayerState)
{
	Super::RemovePlayerState(PlayerState);

	if (APawn* PlayerPawn = PlayerState -> GetPawn())
	{
		PlayerPawnsArr.Remove(PlayerPawn);
	}
}

void ATDS_GameState::OnNewPawnSetToPlayerState(APlayerState* Player, APawn* NewPawn, APawn* OldPawn)
{
	if (OldPawn)
	{
		if (PlayerPawnsArr.Contains(OldPawn))
		{
			PlayerPawnsArr.Remove(OldPawn);
		}
	}

	if (NewPawn)
	{
		PlayerPawnsArr.AddUnique(NewPawn);
	}
}
