﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_PlayerStart.h"
#include "TDS_GameMode.h"
#include "TDS_GameState.h"
#include "TDS_StringLibrary.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/DecalComponent.h"
#include "Engine/AssetManager.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

ATDS_PlayerStart::ATDS_PlayerStart(const FObjectInitializer& ObjectInitializer) : APlayerStart(ObjectInitializer)
{
	PrimaryActorTick.bCanEverTick = false;
	
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComponent"));

	if (BoxComponent)
	{
		BoxComponent -> SetupAttachment(RootComponent);
	}

	DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComponent"));

	if (DecalComponent)
	{
		DecalComponent -> SetupAttachment(BoxComponent);
	}

	GetCapsuleComponent() -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetCapsuleComponent() -> Mobility = EComponentMobility::Movable;
}

void ATDS_PlayerStart::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	BoxComponent -> OnComponentBeginOverlap.AddDynamic(this, &ThisClass::ATDS_PlayerStart::HandleStartOverlap);
}

void ATDS_PlayerStart::HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor -> ActorHasTag(FTagLibrary::PlayerTag))
	{
		if (ATDS_GameMode* GM = Cast<ATDS_GameMode>(UGameplayStatics::GetGameMode(this)))
		{
			if (ProgressIndex > GM -> GetCurrentProgressIndex())
			{
				// DecalComponent -> SetHiddenInGame(true);

				FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
				const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::PlayCheckpointSound, GM, OtherActor);
				Manager.RequestAsyncLoad(CheckpointSound.ToSoftObjectPath(), Delegate);
			}
		}
	}
}

void ATDS_PlayerStart::PlayCheckpointSound(ATDS_GameMode* GM, AActor* OtherActor)
{
	if (CheckpointSound.IsValid())
	{
		if (USoundCue* CheckpointSoundPtr = CheckpointSound.Get())
		{
			UGameplayStatics::PlaySoundAtLocation(this, CheckpointSoundPtr, GetActorLocation());
		}
	}

	SetActorRotation(OtherActor -> GetActorRotation());
	GM -> SetCurrentProgressIndex(ProgressIndex);
}
