﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_WeaponHandlerComponent.h"

#include "TDS.h"
#include "TDS_PickupBase.h"
#include "TDS_PlayerCharacter.h"
#include "TDS_PlayerState.h"
#include "TDS_ProjectilePoolComponent.h"
#include "TDS_StringLibrary.h"
#include "WTDS_InventoryPanel.h"
#include "WTDS_WeaponPanel.h"
#include "TDS_WeaponBase.h"
#include "WTDS_EffectsPanel.h"
#include "WTDS_WeaponSwapNotifyPanel.h"
#include "Engine/AssetManager.h"
#include "Engine/StaticMeshActor.h"
#include "Engine/StreamableManager.h"


// Sets default values for this component's properties
UTDS_WeaponHandlerComponent::UTDS_WeaponHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.TickInterval = 2.f;
	
	CurrentIndex = 0;
	CurrentID = 0;
	CurrentWeapon = nullptr;
	WeaponPickup = nullptr;
}

void UTDS_WeaponHandlerComponent::InitComponent(const TWeakObjectPtr<UWTDS_InventoryPanel>& InventoryPanel, const TWeakObjectPtr<UWTDS_EffectsPanel>& WeaponEffectsPanel, const TWeakObjectPtr<UWTDS_WeaponSwapNotifyPanel>& WeaponSwapNotifyPanel)
{
	if (InventoryPanel.IsValid())
	{
		InventoryPanelPtr = InventoryPanel.Get();
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UTDS_WeaponHandlerComponent::InitComponent: InventoryPanel is NULL"));
	}

	if (WeaponSwapNotifyPanel.IsValid())
	{
		WeaponSwapNotifyPanelPtr = WeaponSwapNotifyPanel.Get();
		WeaponSwapNotifyPanelPtr.Get() -> OnWeaponsInfoSwapCompleted.BindUObject(this, &ThisClass::SwapWeaponsInInventory);
		WeaponSwapNotifyPanelPtr.Get() -> OnWeaponsInfoSwapCanceled.BindUObject(this, &ThisClass::CancelSwapWeaponsInInventory);
	}

	if (WeaponEffectsPanel.IsValid())
	{
		WeaponEffectsPanelPtr = WeaponEffectsPanel.Get();
	}

	if (PlayerCharacterPtr.IsValid())
	{
		if (ATDS_PlayerState* PS = PlayerCharacterPtr.Get() -> GetPlayerState<ATDS_PlayerState>())
		{
			TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>> OwnedWeapons;
			OwnedWeapons.Append(PS -> GetOwnedWeapons());

			if (!OwnedWeapons.IsEmpty())
			{
				OwnedWeaponsArr.Append(OwnedWeapons);
				InitDefaultWeapon(OwnedWeaponsArr[0].Key, true);
			}
			else
			{
				InitDefaultWeapon(*PlayerCharacterPtr.Get() -> GetDefaultWeaponID().GetRow<FWeaponInfo>(PlayerCharacterPtr.Get() -> GetDefaultWeaponID().RowName.ToString()), false);
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_WeaponHandlerComponent::BeginPlay: PlayerState is NULL"));
		}
	}
}

// Called when the game starts
void UTDS_WeaponHandlerComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTDS_WeaponHandlerComponent::OnRegister()
{
	Super::OnRegister();
	
	PlayerCharacterPtr = Cast<ATDS_PlayerCharacter>(GetOwner());
}

void UTDS_WeaponHandlerComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	if (CurrentWeapon)
	{
		CurrentWeapon -> Destroy();
	}
	
	Super::EndPlay(EndPlayReason);
}

void UTDS_WeaponHandlerComponent::TickComponent(float DeltaTime, ELevelTick TickType,
                                                FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//UE_LOG(LogTemp, Log, TEXT("Current Index: %i"), CurrentIndex);

	// if (ATDS_PlayerState* PS = PlayerCharacterPtr.Get() -> GetPlayerState<ATDS_PlayerState>())
	// {
	// 	UE_LOG(LogTemp, Log, TEXT("UTDS_WeaponHandlerComponent::BeginPlay: PS is Valid"), OwnedWeapons.Num());
	// }
}

void UTDS_WeaponHandlerComponent::InitDefaultWeapon(const FWeaponInfo& WeaponInfo, const bool& bIsRespawn)
{
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = PlayerCharacterPtr.Get();
	SpawnParameters.Instigator = PlayerCharacterPtr.Get();

	CurrentWeapon = GetWorld() -> SpawnActor<ATDS_WeaponBase>(ATDS_WeaponBase::StaticClass(), FTransform(), SpawnParameters);

	if (CurrentWeapon)
	{
		const FAttachmentTransformRules AttachmentTransformRules(EAttachmentRule::SnapToTarget, false);
			
		if (IsValid(PlayerCharacterPtr.Get() -> GetMesh()) && InventoryPanelPtr.IsValid())
		{
			CurrentWeapon -> AttachToComponent(PlayerCharacterPtr.Get() -> GetMesh(), AttachmentTransformRules, FSocketLibrary::CharacterWeaponSocket);
			CurrentWeapon -> OnStartFireWeapon.AddUObject(this, &ThisClass::StartCurrentWeaponFire);
			CurrentWeapon -> OnStopFireWeapon.AddUObject(this, &ThisClass::StopCurrentWeaponFire);
			CurrentWeapon -> OnStartReloadWeapon.AddUObject(this, &ThisClass::StartCurrentWeaponReload);
			CurrentWeapon -> OnStopReloadWeapon.AddUObject(this, &ThisClass::StopCurrentWeaponReload);
			CurrentWeapon -> OnUpdateWeaponInfo.AddUObject(this, &ThisClass::UpdateWeaponParameters);
					
			const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnDefaultWeaponDataLoaded, WeaponInfo, WeaponInfo.GetWeaponData(), bIsRespawn);
			StartLoadingWeaponData(WeaponInfo, Delegate);
		}
	}
}

void UTDS_WeaponHandlerComponent::StartLoadingWeaponData(const FWeaponInfo& WeaponInfo, const FStreamableDelegate& Delegate)
{
	if (UAssetManager* Manager = UAssetManager::GetIfInitialized())
	{
		const TArray<FName> BundlesArr;
		Manager -> LoadPrimaryAsset(WeaponInfo.GetWeaponData(), BundlesArr, Delegate);
	}
}

void UTDS_WeaponHandlerComponent::OnDefaultWeaponDataLoaded(FWeaponInfo WeaponInfo, FPrimaryAssetId WeaponID, const bool bIsRespawn)
{
	if (const UAssetManager* Manager = UAssetManager::GetIfInitialized())
	{
		if (UTDS_WeaponData* WeaponData = Cast<UTDS_WeaponData>(Manager -> GetPrimaryAssetObject(WeaponID)))
		{
			LogOnScreen(this, TEXT("UTDS_WeaponHandlerComponent::OnDefaultWeaponDataLoaded: Current Weapon Data was Loaded"), FColor::Green);

			if (!bIsRespawn)
			{
				const int32 Index = OwnedWeaponsArr.Emplace(WeaponInfo, WeaponData);
			
				CurrentWeapon -> InitWeapon(OwnedWeaponsArr[Index].Key, OwnedWeaponsArr[Index].Value);

				if (InventoryPanelPtr.IsValid())
				{
					InventoryPanelPtr.Get() -> UpdateCurrentWeaponPanel(OwnedWeaponsArr[Index].Key, OwnedWeaponsArr[Index].Value);
				}
			}
			else
			{
				CurrentWeapon -> InitWeapon(WeaponInfo, WeaponData);

				if (InventoryPanelPtr.IsValid())
				{
					for (int32 i = 1; i < OwnedWeaponsArr.Num(); i++)
					{
						if (OwnedWeaponsArr.IsValidIndex(i))
						{
							InventoryPanelPtr.Get() -> UpdateWeaponPanels(OwnedWeaponsArr[i].Key, OwnedWeaponsArr[i].Value, true);
						}
					}

					InventoryPanelPtr.Get() -> UpdateCurrentWeaponPanel(WeaponInfo, WeaponData);
				}
			}
		}
		else
		{
			LogOnScreen(this, TEXT("UTDS_WeaponHandlerComponent::OnDefaultWeaponDataLoaded: Current Weapon Data wasn't Loaded"), FColor::Red);
		}
	}
}

void UTDS_WeaponHandlerComponent::OnSwapWeaponDataLoaded(FWeaponInfo WeaponInfo, FPrimaryAssetId WeaponID, TTuple<FWeaponInfo, UTDS_WeaponData*> OwnedWeapon)
{
	if (const UAssetManager* Manager = UAssetManager::GetIfInitialized())
	{
		if (UTDS_WeaponData* WeaponData = Cast<UTDS_WeaponData>(Manager -> GetPrimaryAssetObject(WeaponID)))
		{
			if (PlayerCharacterPtr.IsValid())
			{
				if (PlayerCharacterPtr.Get() -> OnChangeInputMode.ExecuteIfBound(false))
				{
					WeaponSwapNotifyPanelPtr.Get() -> HandleWeaponSwapNotification(OwnedWeapon, MakeTuple(WeaponInfo, WeaponData));
				}
			}
		}
	}
}

void UTDS_WeaponHandlerComponent::OnNewWeaponDataLoaded(FWeaponInfo WeaponInfo, FPrimaryAssetId WeaponID)
{
	if (const UAssetManager* Manager = UAssetManager::GetIfInitialized())
	{
		if (UTDS_WeaponData* WeaponData = Cast<UTDS_WeaponData>(Manager -> GetPrimaryAssetObject(WeaponID)))
		{
			LogOnScreen(this, TEXT("UTDS_WeaponHandlerComponent::OnNewWeaponDataLoaded: New Weapon Data was Loaded"), FColor::Green);

			TWeakObjectPtr<UWTDS_WeaponPanel> WeaponPanel = InventoryPanelPtr.Get() -> AddWeaponPanel();
			
			if (!WeaponPanel.IsValid())
			{
				UE_LOG(LogTemp, Error, TEXT("UTDS_WeaponHandlerComponent::AddWeaponToInventory: Couldn't create a Weapon Panel"));
				return;
			}

			const int32 Index = OwnedWeaponsArr.Insert(MakeTuple(WeaponInfo, WeaponData), CurrentIndex + 1);
			InventoryPanelPtr.Get() -> SetInfoInUnselectedWeaponPanel(WeaponPanel, OwnedWeaponsArr[Index].Key, OwnedWeaponsArr[Index].Value);
		}
		else
		{
			LogOnScreen(this, TEXT("UTDS_WeaponHandlerComponent::OnNewWeaponDataLoaded: New Weapon Data wasn't Loaded"), FColor::Red);
		}
	}
}

bool UTDS_WeaponHandlerComponent::AddWeaponToInventory(ATDS_PickupBase* Pickup, const FWeaponInfo& WeaponInfo)
{
	if (!GetIsWeaponTypeOwned(WeaponInfo))
	{
		const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnNewWeaponDataLoaded, WeaponInfo, WeaponInfo.GetWeaponData());
		StartLoadingWeaponData(WeaponInfo, Delegate);
		
		return true;
	}

	WeaponPickup = Pickup;
	const TTuple<FWeaponInfo, UTDS_WeaponData*> Info = GetWeaponInfoFromArr(WeaponInfo.GetWeaponParameters().GetWeaponAmmoType());
	const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnSwapWeaponDataLoaded, WeaponInfo, WeaponInfo.GetWeaponData(), Info);
	StartLoadingWeaponData(WeaponInfo, Delegate);
	
	return false;
}

bool UTDS_WeaponHandlerComponent::AddAmmoToWeaponInInventory(const FAmmoInfo& AmmoInfo)
{
	TArray<FWeaponInfo> WeaponsArr;
	
	for (auto& Weapon : OwnedWeaponsArr)
	{
		const bool bIsAmmoNeeded = Weapon.Key.GetWeaponParameters().GetCurrentRoundsInReserve() != Weapon.Key.GetWeaponParameters().GetMaxRoundsInReserve() ||
				Weapon.Key.GetWeaponParameters().GetCurrentRoundsInClip() != Weapon.Key.GetWeaponParameters().GetMaxRoundsInClip();
		
		if (Weapon.Key.GetWeaponParameters().GetWeaponAmmoType() == AmmoInfo.GetAmmoType() && bIsAmmoNeeded)
		{
			WeaponsArr.Emplace(Weapon.Key);
		}
	}

	if (!WeaponsArr.IsEmpty())
	{
		const int32 AmmoAmount = AmmoInfo.GetAmmoAmount() / WeaponsArr.Num();

		for (FWeaponInfo& Weapon : WeaponsArr)
		{
			FWeaponParameters WeaponParameters = Weapon.GetWeaponParameters();
			
			if (WeaponParameters.GetCurrentRoundsInReserve() + AmmoAmount > WeaponParameters.GetMaxRoundsInReserve())
			{
				const int32 Amount = FMath::Abs(WeaponParameters.GetMaxRoundsInReserve() - WeaponParameters.GetCurrentRoundsInReserve() + AmmoAmount);
				WeaponParameters.SetCurrentRoundsInReserve(WeaponParameters.GetMaxRoundsInReserve());
				WeaponParameters.SetCurrentRoundsInClip(FMath::Min(WeaponParameters.GetCurrentRoundsInClip() + Amount, WeaponParameters.GetMaxRoundsInClip()));
			}
			else
			{
				WeaponParameters.SetCurrentRoundsInReserve(WeaponParameters.GetCurrentRoundsInReserve() + AmmoAmount);
			}
			
			UpdateWeaponParameters(Weapon.GetID(), WeaponParameters);
			
			if (CurrentWeapon && CurrentWeapon -> GetID() == Weapon.GetID())
			{
				CurrentWeapon -> AddAmmoToClip(WeaponParameters.GetCurrentRoundsInClip());
				CurrentWeapon -> AddAmmoToReserve(WeaponParameters.GetCurrentRoundsInReserve());
			}
		}
	}

	return !WeaponsArr.IsEmpty();
}

bool UTDS_WeaponHandlerComponent::AddWeaponEffect(const FWeaponEffectInfo& WeaponEffectInfo, const FEffectInfo* EffectInfo, TSubclassOf<UTDS_BaseEffect> EffectClass)
{
	if (CurrentWeapon && CurrentWeapon -> GetProjectilePoolComponent().IsValid())
	{
		if (!CurrentWeapon -> GetProjectilePoolComponent().Get() -> IsEffectEnabled())
		{
			FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
			const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnWeaponEffectIconLoaded, WeaponEffectInfo, EffectInfo, EffectClass);
			Manager.RequestAsyncLoad(WeaponEffectInfo.GetEffectIcon().ToSoftObjectPath(), Delegate);
			return true;
		}
		
		return false;
	}

	return false;
}

void UTDS_WeaponHandlerComponent::RemoveWeaponEffect()
{
	if (WeaponEffectsPanelPtr.IsValid())
	{
		WeaponEffectsPanelPtr.Get() -> ResetPanel();
	}
}

void UTDS_WeaponHandlerComponent::InterruptWeaponEffect()
{
	if (WeaponEffectsPanelPtr.IsValid())
	{
		WeaponEffectsPanelPtr.Get() -> ResetPanel();
	}
	if (CurrentWeapon && CurrentWeapon -> GetProjectilePoolComponent().IsValid())
	{
		CurrentWeapon -> GetProjectilePoolComponent().Get() -> DisableEffect();
	}
}

void UTDS_WeaponHandlerComponent::OnWeaponEffectIconLoaded(const FWeaponEffectInfo WeaponEffectInfo, const FEffectInfo* EffectInfo, TSubclassOf<UTDS_BaseEffect> EffectClass)
{
	if (WeaponEffectInfo.GetEffectIcon().IsValid())
	{
		if (UTexture2D* WeaponEffectIcon = WeaponEffectInfo.GetEffectIcon().Get())
		{
			if (WeaponEffectsPanelPtr.IsValid())
			{
				WeaponEffectsPanelPtr.Get() -> AddEffect(WeaponEffectInfo, WeaponEffectIcon);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("UTDS_WeaponHandlerComponent::OnWeaponEffectIconLoaded: WeaponEffectsPanelPtr is NULL"));
			}

			if (CurrentWeapon && CurrentWeapon -> GetProjectilePoolComponent().IsValid())
			{
				CurrentWeapon -> GetProjectilePoolComponent().Get() -> EnableEffect(WeaponEffectInfo, EffectInfo, EffectClass);
				CurrentWeapon -> GetProjectilePoolComponent().Get() -> OnDisableEffect.AddUObject(this, &ThisClass::RemoveWeaponEffect);
			}
		}
	}
}


void UTDS_WeaponHandlerComponent::SwitchWeapon(const float& Value)
{
	if (CurrentWeapon)
	{
		if (InventoryPanelPtr.IsValid() && OwnedWeaponsArr.Num() > 1)
		{
			UpdateWeaponParameters(CurrentWeapon -> GetID(), CurrentWeapon -> GetWeaponParameters());
			InventoryPanelPtr.Get() -> UpdateWeaponPanels(OwnedWeaponsArr[CurrentIndex].Key, OwnedWeaponsArr[CurrentIndex].Value, Value > 0);
	
			Value > 0 ? SelectNextWeapon() : SelectPreviousWeapon();
		}
	}
}

void UTDS_WeaponHandlerComponent::SelectNextWeapon()
{
	if (!OwnedWeaponsArr.IsEmpty())
	{
		CurrentIndex + 1 > OwnedWeaponsArr.Num() - 1 ? CurrentIndex = 0 : CurrentIndex++;
		SetupCurrentWeapon(OwnedWeaponsArr[CurrentIndex].Key, OwnedWeaponsArr[CurrentIndex].Value);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UTDS_WeaponHandlerComponent::SelectNextWeapon: OwnedWeaponsArr Num: %i"), OwnedWeaponsArr.Num());
	}
}

void UTDS_WeaponHandlerComponent::SelectPreviousWeapon()
{
	if (!OwnedWeaponsArr.IsEmpty())
	{
		CurrentIndex - 1 < 0 ? 	CurrentIndex = OwnedWeaponsArr.Num() - 1 : CurrentIndex--;
		SetupCurrentWeapon(OwnedWeaponsArr[CurrentIndex].Key, OwnedWeaponsArr[CurrentIndex].Value);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("UTDS_WeaponHandlerComponent::SelectPreviousWeapon: OwnedWeaponsArr Num: %i"), OwnedWeaponsArr.Num());
	}
}

void UTDS_WeaponHandlerComponent::SetupCurrentWeapon(const FWeaponInfo& WeaponInfo, UTDS_WeaponData* WeaponData)
{
	if (CurrentWeapon)
	{
		PlayerCharacterPtr.Get() -> StopAllMontages();
		CurrentWeapon -> ResetWeapon();

		CurrentWeapon -> InitWeapon(WeaponInfo, WeaponData);

		if (InventoryPanelPtr.IsValid())
		{
			InventoryPanelPtr.Get() -> UpdateCurrentWeaponPanel(WeaponInfo, WeaponData);
		}
	}
}

void UTDS_WeaponHandlerComponent::SwapWeaponsInInventory(const FWeaponsInfo& WeaponsInfo)
{
	const FWeaponData NewWeapon = WeaponsInfo.Key;
	const FWeaponData OwnedWeapon = WeaponsInfo.Value;
	
	for (auto& Weapon : OwnedWeaponsArr)
	{
		if (Weapon.Key.GetID() == OwnedWeapon.Key.GetID())
		{
			Weapon = NewWeapon;
	
			if (InventoryPanelPtr.IsValid())
			{
				InventoryPanelPtr.Get() -> UpdateInfoInInventoryBox(Weapon.Key, Weapon.Value);
			}

			if (CurrentWeapon -> GetID() == OwnedWeapon.Key.GetID())
			{
				SetupCurrentWeapon(Weapon.Key, Weapon.Value);
				break;
			}
		}
	}
	
	if (PlayerCharacterPtr.IsValid())
	{
		if (!PlayerCharacterPtr.Get() -> OnChangeInputMode.ExecuteIfBound(true))
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_WeaponHandlerComponent::SwapWeaponsInInventory: OnChangeInputMode isn't Bound"));	
		}
		else
		{
			if (WeaponPickup)
			{
				WeaponPickup -> Destroy();
			}
			
			DropOldWeapon(OwnedWeapon);
		}
	}
}

void UTDS_WeaponHandlerComponent::DropOldWeapon(const FWeaponData& WeaponData)
{
	const FVector Location = CurrentWeapon -> GetActorLocation();
	const FRotator Rotation = FRotator(0.f, 90.f, -90.f);
	FActorSpawnParameters ActorSpawnParameters;
	ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	ActorSpawnParameters.Owner = PlayerCharacterPtr.Get();
	
	TWeakObjectPtr<AStaticMeshActor> OldWeapon = GetWorld() -> SpawnActor<AStaticMeshActor>(Location, Rotation, ActorSpawnParameters);
		
	if (OldWeapon.IsValid() && WeaponData.Value -> WeaponStaticMesh)
	{
		OldWeapon.Get() -> GetStaticMeshComponent() -> SetMobility(EComponentMobility::Movable);
		OldWeapon.Get() -> GetStaticMeshComponent() -> SetCollisionProfileName(FCollisionProfileLibrary::EjectableMeshProfile);
		OldWeapon.Get() -> GetStaticMeshComponent() -> SetStaticMesh(WeaponData.Value -> WeaponStaticMesh);
		OldWeapon.Get() -> GetStaticMeshComponent() -> SetSimulatePhysics(true);
		OldWeapon.Get() -> GetStaticMeshComponent() -> SetEnableGravity(true);
		OldWeapon.Get() -> SetLifeSpan(3.f);
				
		FVector Direction = PlayerCharacterPtr.Get() -> GetActorForwardVector() * 100.f;
				
		OldWeapon.Get() -> GetStaticMeshComponent() -> AddImpulse(Direction, NAME_None, true);
	}
}

void UTDS_WeaponHandlerComponent::CancelSwapWeaponsInInventory()
{
	if (PlayerCharacterPtr.IsValid())
	{
		if (!PlayerCharacterPtr.Get() -> OnChangeInputMode.ExecuteIfBound(true))
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_WeaponHandlerComponent::CancelSwapWeaponsInInventory: OnChangeInputMode isn't Bound"));	
		}
	}
}

void UTDS_WeaponHandlerComponent::UpdateWeaponParameters(const FGuid& ID, const FWeaponParameters& Parameters)
{
	if (!OwnedWeaponsArr.IsEmpty())
	{
		for (auto& Weapon : OwnedWeaponsArr)
		{
			if (Weapon.Key.GetID() == ID)
			{
				FWeaponInfo& Info = Weapon.Get<0>();
				Info.SetWeaponParameters(Parameters);
				
				if (InventoryPanelPtr.IsValid())
				{
					InventoryPanelPtr.Get() -> UpdateInfoInInventoryBox(Info, Weapon.Value);
					return;
				}
			}
		}
	}
}

void UTDS_WeaponHandlerComponent::HandleStartCurrentWeaponFire(const bool& InputValue)
{
	if (CurrentWeapon)
	{
		CurrentWeapon -> HandleStartFire(InputValue);
	}
}

void UTDS_WeaponHandlerComponent::HandleStopCurrentWeaponFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon -> HandleStopFire();
	}
}


void UTDS_WeaponHandlerComponent::StartCurrentWeaponFire(const TWeakObjectPtr<UAnimMontage>& Montage)
{
	if (PlayerCharacterPtr.IsValid())
	{
		PlayerCharacterPtr -> PlayMontage(Montage);
	}
}

void UTDS_WeaponHandlerComponent::StopCurrentWeaponFire(const TWeakObjectPtr<UAnimMontage>& Montage)
{
	if (PlayerCharacterPtr.IsValid())
	{
		PlayerCharacterPtr -> StopMontage(Montage);
	}
}

void UTDS_WeaponHandlerComponent::HandleCurrentWeaponReload()
{
	if (CurrentWeapon && !CurrentWeapon -> GetIsReloading())
	{
		CurrentWeapon -> HandleReload();
	}
}

void UTDS_WeaponHandlerComponent::StartCurrentWeaponReload(const TWeakObjectPtr<UAnimMontage>& Montage)
{
	if (Montage.IsValid() && PlayerCharacterPtr.IsValid())
	{
		PlayerCharacterPtr.Get() -> PlayMontage(Montage);

		if(InventoryPanelPtr.IsValid() && CurrentWeapon)
		{
			InventoryPanelPtr.Get() -> StartCurrentWeaponReloadUI(CurrentWeapon -> GetReloadTime());
		}
	}
}


void UTDS_WeaponHandlerComponent::StopCurrentWeaponReload(const TWeakObjectPtr<UAnimMontage>& Montage)
{
	if (Montage.IsValid() && PlayerCharacterPtr.IsValid())
	{
		PlayerCharacterPtr.Get() -> StopMontage(Montage);
	}
}

bool UTDS_WeaponHandlerComponent::GetIsWeaponTypeOwned(const FWeaponInfo& Info)
{
	for (auto Weapon : OwnedWeaponsArr)
	{
		if (Weapon.Key.GetWeaponParameters().GetWeaponAmmoType() == Info.GetWeaponParameters().GetWeaponAmmoType())
		{
			return true;
		}
	}

	return false;
}

TTuple<FWeaponInfo, UTDS_WeaponData*> UTDS_WeaponHandlerComponent::GetWeaponInfoFromArr(const EWeaponAmmoType& AmmoType)
{
	for (auto Weapon : OwnedWeaponsArr)
	{
		if (Weapon.Key.GetWeaponParameters().GetWeaponAmmoType() == AmmoType)
		{
			return Weapon;
		}
	}

	return MakeTuple(FWeaponInfo(), nullptr);
}

TWeakObjectPtr<UTDS_WeaponHandlerComponent> UTDS_WeaponHandlerComponent::GetWeaponHandlerComponent(AActor* Actor)
{
	if (Actor)
	{
		if (ATDS_PlayerCharacter* PlayerCharacter = Cast<ATDS_PlayerCharacter>(Actor))
		{
			return PlayerCharacter -> GetWeaponHandlerComponent();
		}
	}

	return nullptr;
}

TArray<EWeaponAmmoType> UTDS_WeaponHandlerComponent::GetOwnedAmmoTypes()
{
	if (OwnedWeaponsArr.IsEmpty())
	{
		return TArray<EWeaponAmmoType>();
	}
	
	TArray<EWeaponAmmoType> OwnedAmmoTypes;
	
	for (TTuple<FWeaponInfo, UTDS_WeaponData*> Weapon : OwnedWeaponsArr)
	{
		OwnedAmmoTypes.Emplace(Weapon.Key.GetWeaponParameters().GetWeaponAmmoType());
	}

	return OwnedAmmoTypes;
}

TWeakObjectPtr<ATDS_WeaponBase> UTDS_WeaponHandlerComponent::GetCurrentWeapon() const
{
	return CurrentWeapon;
}

TArray<TTuple<FWeaponInfo, UTDS_WeaponData*>> UTDS_WeaponHandlerComponent::GetOwnedWeapons()
{
	UpdateWeaponParameters(CurrentWeapon -> GetID(), CurrentWeapon -> GetWeaponParameters());
	return OwnedWeaponsArr;
}
