﻿#include "TDS_PlayerHealthComponent.h"
#include "TDS_PlayerCharacter.h"
#include "TDS_StringLibrary.h"
#include "WTDS_PlayerInfoPanel.h"

UTDS_PlayerHealthComponent::UTDS_PlayerHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	bIsShielded = true;
	MaxShield = 100.f;
	CurrentShield = MaxShield;

	bIsImmune = false;
}

void UTDS_PlayerHealthComponent::InitComponent(const TWeakObjectPtr<UWTDS_PlayerInfoPanel> PlayerInfoPanel)
{
	if (PlayerInfoPanel.IsValid())
	{
		PlayerInfoPanelPtr = PlayerInfoPanel.Get();
		
		if (PlayerInfoPanelPtr.IsValid())
		{
			PlayerInfoPanelPtr.Get() -> SetHealthBarValue(CurrentHealth / MaxHealth);

			bIsShielded = MaxShield > 0.f;
			CurrentShield = MaxShield;
			bIsShielded ? PlayerInfoPanelPtr.Get() -> SetShieldBarValue(CurrentShield / MaxShield) : PlayerInfoPanelPtr.Get() -> SetShieldBarValue(0.f);
			
			if (OwnerPtr.IsValid() && GeneralPhysMaterial && ShieldedPhysMaterial)
			{
				bIsShielded ? OwnerPtr.Get() -> SetHitBoxPhysMaterial(ShieldedPhysMaterial) : OwnerPtr.Get() -> SetHitBoxPhysMaterial(GeneralPhysMaterial);
			}
		}
	}
}

void UTDS_PlayerHealthComponent::OnRegister()
{
	Super::OnRegister();

	OwnerPtr = Cast<ATDS_CharacterBase>(GetOwner());
}

float UTDS_PlayerHealthComponent::DecreaseHealth(const float& Amount)
{
	if (!bIsImmune)
	{
		const float Damage = FMath::RoundToFloat(Amount);
		SpawnDamageVisual(Damage, bIsShielded);
	
		if (bIsShielded)
		{
			CurrentShield -= Damage;
			OnShieldChanged_Delegate.Broadcast(CurrentShield, CurrentShield / MaxShield);
		
			if (CurrentShield <= 0.f)
			{
				const auto Remainder = FMath::Abs(CurrentShield);

				CurrentShield = 0;
				bIsShielded = false;
		
				float Value = Super::DecreaseHealth(Remainder);
			
				if (PlayerInfoPanelPtr.IsValid())
				{
					PlayerInfoPanelPtr -> HandleModifyShieldBarValue(CurrentShield / MaxShield);
					PlayerInfoPanelPtr -> HandleModifyHealthBarValue(Value / MaxHealth);
				}

				if (OwnerPtr.IsValid())
				{
					OwnerPtr.Get() -> SetHitBoxPhysMaterial(GeneralPhysMaterial);
				}

				OnShieldWasDepleted_Delegate.Broadcast();
				
				return Value;
			}

			if (PlayerInfoPanelPtr.IsValid())
			{
				PlayerInfoPanelPtr -> HandleModifyShieldBarValue(CurrentShield / MaxShield);
			}
		
			return CurrentShield;
		}

		float Value = Super::DecreaseHealth(FMath::RoundToFloat(Damage));

		if (PlayerInfoPanelPtr.IsValid())
		{
			PlayerInfoPanelPtr.Get() -> HandleModifyHealthBarValue(Value / MaxHealth);
		}
	}
	
	
	return CurrentHealth;
}

bool UTDS_PlayerHealthComponent::IncreaseHealth(const FHealthInfo& HealthInfo)
{
	return HealthInfo.GetHealthType() == EHealthType::HT_Health ? IncreaseHealthPoints(HealthInfo.GetHealthAmount()) : IncreaseShield(HealthInfo.GetHealthAmount());
}

bool UTDS_PlayerHealthComponent::IncreaseHealthPoints(const float& Amount, const bool& bIsOverTime)
{
	if (Super::IncreaseHealthPoints(Amount, bIsOverTime))
	{
		if (PlayerInfoPanelPtr.IsValid())
		{
			PlayerInfoPanelPtr -> HandleModifyHealthBarValue(CurrentHealth / MaxHealth);

			return true;
		}
	}

	return false;
}

bool UTDS_PlayerHealthComponent::IncreaseShield(const float& Amount)
{
	if (!bIsShielded || MaxShield < Amount)
	{
		SetShieldBarValue(SetShield(Amount));
		return true;
	}
	if (CurrentShield != MaxShield)
	{
		CurrentShield = FMath::Min(CurrentShield + Amount, MaxShield);
		SetShieldBarValue(CurrentShield / MaxShield);
		return true;
	}

	return false;
}

float UTDS_PlayerHealthComponent::GetCurrentShield() const
{
	return CurrentShield;
}

float UTDS_PlayerHealthComponent::GetMaxShield() const
{
	return MaxShield;
}

void UTDS_PlayerHealthComponent::RefillHealth()
{
	CurrentHealth = MaxHealth;
		
	OnHealthChanged_Delegate.Broadcast(CurrentHealth, CurrentHealth / MaxHealth);
}

void UTDS_PlayerHealthComponent::SetIsImmune(const bool& Value)
{
	bIsImmune = Value;
}

void UTDS_PlayerHealthComponent::SetShieldBarValue(const float& Percent)
{
	if (PlayerInfoPanelPtr.IsValid())
	{
		PlayerInfoPanelPtr -> HandleModifyShieldBarValue(Percent);
	}
}

float UTDS_PlayerHealthComponent::SetShield(const float& Amount)
{
	bIsShielded = true;
	MaxShield = Amount;
	CurrentShield = MaxShield;

	if (OwnerPtr.IsValid())
	{
		OwnerPtr.Get() -> SetHitBoxPhysMaterial(ShieldedPhysMaterial);
	}

	return CurrentShield / MaxShield;
}

void UTDS_PlayerHealthComponent::SetIsShielded(const bool& Value)
{
	bIsShielded = Value;
}

bool UTDS_PlayerHealthComponent::GetIsShielded() const
{
	return bIsShielded;
}

UTDS_PlayerHealthComponent* UTDS_PlayerHealthComponent::GetPlayerHealthComponent_BP(AActor* Actor)
{
	if (Actor)
	{
		return Actor -> FindComponentByClass<UTDS_PlayerHealthComponent>();
	}

	return nullptr;
}

TWeakObjectPtr<UTDS_PlayerHealthComponent> UTDS_PlayerHealthComponent::GetPlayerHealthComponent(AActor* Actor)
{
	if (Actor)
	{
		return Actor -> FindComponentByClass<UTDS_PlayerHealthComponent>();
	}

	return nullptr;
}
