﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter/TDS_PlayerInventoryComponent.h"


// Sets default values for this component's properties
UTDS_PlayerInventoryComponent::UTDS_PlayerInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


// Called when the game starts
void UTDS_PlayerInventoryComponent::BeginPlay()
{
	Super::BeginPlay();
}