﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_ProjectilePoolComponent.h"

#include "TDS_BaseEffect.h"
#include "TDS_ProjectileBase.h"
#include "TDS_StringLibrary.h"
#include "TDS_WeaponData.h"

UTDS_ProjectilePoolComponent::UTDS_ProjectilePoolComponent()
{
	WeaponDataPtr = nullptr;
}

void UTDS_ProjectilePoolComponent::SpawnProjectilePool(const FWeaponInfo& WeaponInfo, UTDS_WeaponData* WeaponData, const FVector& ProjectileSpawnPosition)
{
	Info = WeaponInfo;
	WeaponDataPtr = WeaponData;
	UClass* Class = WeaponData -> ProjectileClass;
		
	FTransform Transform = FTransform();
	Transform.SetLocation(ProjectileSpawnPosition);
			
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParameters.Owner = GetOwner();

	if (Class)
	{
		for (int i = 0; i < WeaponInfo.GetWeaponParameters().GetMaxRoundsInClip(); ++i)
		{
			TWeakObjectPtr<ATDS_ProjectileBase> Projectile = GetWorld() -> SpawnActor<ATDS_ProjectileBase>(Class, ProjectileSpawnPosition, FRotator::ZeroRotator,SpawnParameters);
		
			if (Projectile.IsValid() && SpawnParameters.Owner)
			{
				Projectile.Get() -> AttachToActor(SpawnParameters.Owner, FAttachmentTransformRules::KeepWorldTransform);
				Projectile.Get() -> SetupProjectileParameters(Info.GetWeaponParameters(), WeaponDataPtr);
				Projectile.Get() -> OnDeactivateProjectile.AddUObject(this, &ThisClass::AddProjectileToPool);
				Projectile.Get() -> Tags.Emplace(FTagLibrary::ProjectileTag);
				OnEnableEffect.AddUObject(Projectile.Get(), &ATDS_ProjectileBase::OnEnableEffect);
				OnDisableEffect.AddUObject(Projectile.Get(), &ATDS_ProjectileBase::OnDisableEffect);
				InactiveProjectileArr.Emplace(Projectile);
			}
		}
	}

	if (EffectData.EffectClass)
	{
		OnEnableEffect.Broadcast(EffectData.EffectData, EffectData.EffectClass);
	}
}

void UTDS_ProjectilePoolComponent::ResetProjectilePool()
{
	if (!InactiveProjectileArr.IsEmpty())
	{
		for (TWeakObjectPtr<ATDS_ProjectileBase>& Projectile : InactiveProjectileArr)
		{
			if (Projectile.IsValid())
			{
				Projectile.Get() -> Destroy();
				Projectile.Reset();
			}
		}

		InactiveProjectileArr.Empty();
	}

	if (!ActiveProjectileArr.IsEmpty())
	{
		for (TWeakObjectPtr<ATDS_ProjectileBase>& Projectile : ActiveProjectileArr)
		{
			if (Projectile.IsValid())
			{
				Projectile.Get() -> Destroy();
				Projectile.Reset();
			}

			InactiveProjectileArr.Empty();
		}
	}
}

TWeakObjectPtr<ATDS_ProjectileBase> UTDS_ProjectilePoolComponent::GetProjectileFromPool()
{
	if (!InactiveProjectileArr.IsEmpty())
	{
		if (InactiveProjectileArr.Last().IsValid())
		{
			ActiveProjectileArr.Emplace(InactiveProjectileArr.Pop());
			
			return ActiveProjectileArr.Last();
		}
	}
	else
	{
		if (!ActiveProjectileArr.IsEmpty())
		{
			UClass* Class = ActiveProjectileArr.Last().Get() -> GetClass();
			
			FTransform Transform = FTransform();
			Transform.SetLocation(GetOwner() -> GetActorLocation());
			
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParameters.Owner = GetOwner();

			TWeakObjectPtr<ATDS_ProjectileBase> Projectile = Cast<ATDS_ProjectileBase>(GetWorld() -> SpawnActor(Class, &Transform, SpawnParameters));
		
			if (Projectile.IsValid() && SpawnParameters.Owner)
			{
				Projectile.Get() -> AttachToActor(SpawnParameters.Owner, FAttachmentTransformRules::KeepWorldTransform);
				Projectile.Get() -> SetupProjectileParameters(Info.GetWeaponParameters(), WeaponDataPtr);
				Projectile.Get() -> OnDeactivateProjectile.AddUObject(this, &ThisClass::AddProjectileToPool);
				Projectile.Get() -> Tags.Emplace(FTagLibrary::ProjectileTag);
				ActiveProjectileArr.Emplace(Projectile);

				return Projectile;
			}
		}
	}

	return TWeakObjectPtr<ATDS_ProjectileBase>(nullptr);
}

void UTDS_ProjectilePoolComponent::AddProjectileToPool(const TWeakObjectPtr<ATDS_ProjectileBase> Projectile)
{
	if (ActiveProjectileArr.Contains(Projectile))
	{
		int32 Index = ActiveProjectileArr.Find(Projectile);

		if (ActiveProjectileArr.IsValidIndex(Index))
		{
			InactiveProjectileArr.Emplace(ActiveProjectileArr[Index].Get());
			ActiveProjectileArr.Remove(ActiveProjectileArr[Index].Get());
		}
	}
}

void UTDS_ProjectilePoolComponent::EnableEffect(const FWeaponEffectInfo& WeaponEffectInfo, const FEffectInfo* EffectInfo, TSubclassOf<UTDS_BaseEffect> Effect)
{
	EffectData.EffectData = *EffectInfo;
	EffectData.EffectClass = Effect;
	
	OnEnableEffect.Broadcast(EffectData.EffectData, EffectData.EffectClass);

	GetWorld() -> GetTimerManager().SetTimer(EffectTimerElapsed_TimerHandle, this, &UTDS_ProjectilePoolComponent::DisableEffect, WeaponEffectInfo.GetEffectDuration(), false);
}

void UTDS_ProjectilePoolComponent::DisableEffect()
{
	EffectData.EffectData = FEffectInfo();
	EffectData.EffectClass = nullptr;
	
	OnDisableEffect.Broadcast();
}

bool UTDS_ProjectilePoolComponent::IsEffectEnabled() const
{
	return EffectData.EffectClass != nullptr;
}
