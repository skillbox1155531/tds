﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_InputHandlerComponent.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "TDS_StringLibrary.h"
#include "TDS_InputConfig.h"
#include "TDS_PlayerController.h"

UTDS_InputHandlerComponent::UTDS_InputHandlerComponent(): InputConfig(nullptr), bIsAiming(false)
{
}

void UTDS_InputHandlerComponent::BeginPlay()
{
	Super::BeginPlay();
	
	SetupInput();
}

void UTDS_InputHandlerComponent::OnRegister()
{
	Super::OnRegister();
	
	PlayerControllerPtr = Cast<ATDS_PlayerController>(GetOwner());
	InputConfig = LoadObject<UTDS_InputConfig>(this, *FPathLibrary::InputConfigPath);
}

void UTDS_InputHandlerComponent::SetupInput()
{
	if (PlayerControllerPtr.IsValid() && InputConfig)
	{
		AddMappingContext();
		BindInputDelegates();
	}
}

void UTDS_InputHandlerComponent::AddMappingContext()
{
	if (PlayerControllerPtr.IsValid())
	{
		TWeakObjectPtr<UEnhancedInputLocalPlayerSubsystem> Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerControllerPtr.Get() -> GetLocalPlayer());

		if (Subsystem.IsValid())
		{
			if (InputConfig -> IMC_BaseContext)
			{
				Subsystem.Get() -> AddMappingContext(InputConfig -> IMC_BaseContext, InputConfig -> ContextPriority);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("No Input Mapping Context found"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("No Subsystem found"));
		}	
	}
}

void UTDS_InputHandlerComponent::BindInputDelegates()
{
	if (PlayerControllerPtr.IsValid() && InputConfig)
	{
		if (IsValid(Cast<UEnhancedInputComponent>(PlayerControllerPtr.Get() -> InputComponent)))
		{
			TWeakObjectPtr<UEnhancedInputComponent> EnhancedInputComponent = Cast<UEnhancedInputComponent>(PlayerControllerPtr.Get() -> InputComponent);

			if (EnhancedInputComponent.IsValid())
			{
				BindMovement(EnhancedInputComponent);
			}
		}
	}
}

void UTDS_InputHandlerComponent::BindMovement(const TWeakObjectPtr<UEnhancedInputComponent>& EnhancedInputComponent)
{
	if (EnhancedInputComponent.IsValid())
	{
		if (InputConfig -> IA_Move && InputConfig -> IA_Aim && InputConfig -> IA_Fire && InputConfig -> IA_Reload && InputConfig -> IA_SwitchWeapon)
		{
			EnhancedInputComponent.Get() -> BindAction(InputConfig -> IA_Move, ETriggerEvent::Triggered, this, &ThisClass::HandleMove);
			EnhancedInputComponent.Get()  -> BindAction(InputConfig -> IA_Aim, ETriggerEvent::Triggered, this, &ThisClass::HandleAim);
			EnhancedInputComponent.Get()  -> BindAction(InputConfig -> IA_Aim, ETriggerEvent::Completed, this, &ThisClass::HandleAim);
			EnhancedInputComponent.Get()  -> BindAction(InputConfig -> IA_Fire, ETriggerEvent::Triggered, this, &ThisClass::HandleFire);
			EnhancedInputComponent.Get()  -> BindAction(InputConfig -> IA_Reload, ETriggerEvent::Triggered, this, &ThisClass::HandleReload);
			EnhancedInputComponent.Get()  -> BindAction(InputConfig -> IA_SwitchWeapon, ETriggerEvent::Started, this, &ThisClass::HandleSwitchWeapon);
			EnhancedInputComponent.Get()  -> BindAction(InputConfig -> IA_Health, ETriggerEvent::Started, this, &ThisClass::HandleHealth);
		}
	}
}

void UTDS_InputHandlerComponent::HandleMove(const FInputActionValue& InputActionValue)
{
	if (PlayerControllerPtr.IsValid())
	{
		if (!PlayerControllerPtr.Get() -> OnCharacterMove.ExecuteIfBound(InputActionValue))
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_InputHandlerComponent::HandleMove: OnCharacterMove isn't bound"));
		}
	}
}

void UTDS_InputHandlerComponent::HandleAim(const FInputActionValue& InputActionValue)
{
	if (PlayerControllerPtr.IsValid())
	{
		InputActionValue.Get<bool>() ? bIsAiming = true : bIsAiming = false;
		
		if (!PlayerControllerPtr.Get() -> OnCharacterAim.ExecuteIfBound(InputActionValue))
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_InputHandlerComponent::HandleAim: OnCharacterAim isn't bound"));
		}
	}
}

void UTDS_InputHandlerComponent::HandleFire(const FInputActionValue& InputActionValue)
{
	if (bIsAiming && PlayerControllerPtr.IsValid())
	{
		//UE_LOG(LogTemp, Log, TEXT("UTDS_InputHandlerComponent::HandleFire: Click"));
		if (!PlayerControllerPtr.Get() -> OnCharacterFireWeapon.ExecuteIfBound(InputActionValue))
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_InputHandlerComponent::HandleFire: OnCharacterFireWeapon isn't bound"));
		}
	}
}

void UTDS_InputHandlerComponent::HandleReload(const FInputActionValue& InputActionValue)
{
	if (PlayerControllerPtr.IsValid())
	{
		//UE_LOG(LogTemp, Log, TEXT("UTDS_InputHandlerComponent::HandleReload: Click"));
		if (!PlayerControllerPtr.Get() -> OnCharacterReloadWeapon.ExecuteIfBound(InputActionValue))
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_InputHandlerComponent::HandleReload: OnCharacterReloadWeapon isn't bound"));
		}
	}
}

void UTDS_InputHandlerComponent::HandleSwitchWeapon(const FInputActionValue& InputActionValue)
{
	if (PlayerControllerPtr.IsValid())
	{
		if (!PlayerControllerPtr.Get() -> OnCharacterSwitchWeapon.ExecuteIfBound(InputActionValue))
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_InputHandlerComponent::HandleSwitchWeapon: OnCharacterSwitchWeapon isn't bound"));
		}
	}
}

void UTDS_InputHandlerComponent::HandleHealth(const FInputActionValue& InputActionValue)
{
	if (PlayerControllerPtr.IsValid())
	{
		if (!PlayerControllerPtr.Get() -> OnCharacterChangeHealth.ExecuteIfBound(InputActionValue))
		{
			UE_LOG(LogTemp, Error, TEXT("UTDS_InputHandlerComponent::HandleHealth: OnCharacterSwitchWeapon isn't bound"));
		}
	}
}
