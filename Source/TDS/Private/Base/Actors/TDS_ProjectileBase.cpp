﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_ProjectileBase.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "TDS_ProjectilePoolComponent.h"
#include "TDS_StringLibrary.h"
#include "TDS_StructLibrary.h"
#include "TDS_WeaponData.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"

ATDS_ProjectileBase::ATDS_ProjectileBase()
{
	ATDS_ProjectileBase::SetupComponents();
	SetProjectileVariables();
	Tags.Emplace(FTagLibrary::ProjectileTag);
}

void ATDS_ProjectileBase::SetupComponents()
{
	if (!BoxComponent)
	{
		BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));

		if (BoxComponent)
		{
			BoxComponent -> SetBoxExtent(FVector(3.f, 3.f, 3.f));
			BoxComponent -> SetSimulatePhysics(false);
			BoxComponent -> SetNotifyRigidBodyCollision(true);
			BoxComponent -> SetGenerateOverlapEvents(false);
			BoxComponent -> SetCollisionProfileName(FCollisionProfileLibrary::ProjectileProfile);
			BoxComponent -> GetBodyInstance() -> bLockXRotation = true;
			BoxComponent -> GetBodyInstance() -> bLockYRotation = true;
			RootComponent = BoxComponent;
		}
	}

	if (!TrailComponent)
	{
		TrailComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Trail Component"));

		if (TrailComponent)
		{
			TrailComponent -> SetAutoActivate(false);
			TrailComponent -> SetAutoDestroy(false);
			TrailComponent -> SetupAttachment(RootComponent);
		}
	}
}

void ATDS_ProjectileBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	// TWeakObjectPtr<UTDS_ProjectilePoolComponent> ProjectilePoolComponent = Cast<UTDS_ProjectilePoolComponent>(GetOwner());
	//
	// if (ProjectilePoolComponent.IsValid())
	// {
	// 	ProjectilePoolComponent.Get() -> OnEnableEffect.AddUObject(this, &ThisClass::OnEnableEffect);
	// }
}

void ATDS_ProjectileBase::SetProjectileVariables()
{
	bIsHit = false;
	InitialXVelocity = 0.f;
	Damage = 0.f;
	MaxRange = 0.f;
	CachedLaunchPosition = FVector::ZeroVector;
}

float ATDS_ProjectileBase::TrackProjectile()
{
	return FVector::Distance(CachedLaunchPosition, GetActorLocation());
}

void ATDS_ProjectileBase::SetupProjectileParameters(const FWeaponParameters& WeaponParameters, const UTDS_WeaponData* WeaponData)
{
	InitialXVelocity = WeaponParameters.GetInitialProjectileXVelocity();
	InitialZVelocity = WeaponParameters.GetInitialProjectileZVelocity();
	Damage = WeaponParameters.GetDamage();
	MaxRange = WeaponParameters.GetMaxFireDistance();
	
	if (TrailComponent && WeaponData -> ProjectileTrailFX)
	{
		TrailComponent -> SetAsset(WeaponData -> ProjectileTrailFX);
	}

	SetActive(false);
}

void ATDS_ProjectileBase::Launch(const FVector& ShotDirection)
{
	Activate(ShotDirection);

	FVector Direction = (ShotDirection * InitialXVelocity) + (GetActorUpVector() * InitialZVelocity);
	
	BoxComponent -> AddImpulse(Direction, NAME_None, true);
}

void ATDS_ProjectileBase::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
								FVector NormalImpulse, const FHitResult& Hit)
{
	// if (OtherComp && OtherComp -> Mobility == EComponentMobility::Movable)
	// {
	// 	OtherComp -> AddImpulse(GetActorForwardVector() * 50.f, NAME_None, true);
	// }
}

void ATDS_ProjectileBase::PlayProjectileFX(const TEnumAsByte<EPhysicalSurface>& SurfaceType, const FHitResult& HitResult)
{
	FProjectileHitFX HitFX;
	OnGetProjectileHitFX.Broadcast(SurfaceType ,HitFX);
	
	if (HitFX.GetHitSoundFX().IsValid())
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitFX.GetHitSoundFX().Get(), HitResult.Location);
	}
		
	if (HitFX.GetHitVisualFX().IsValid())
	{
		UNiagaraFunctionLibrary::SpawnSystemAttached(HitFX.GetHitVisualFX().Get(), HitResult.GetComponent(), NAME_None, HitResult.Location, HitResult.Location.Rotation(), EAttachLocation::KeepWorldPosition, true);
	}

	if (HitFX.GetHitDecalFX().IsValid())
	{
		UGameplayStatics::SpawnDecalAttached(HitFX.GetHitDecalFX().Get(), FVector(10.f), HitResult.GetComponent(), NAME_None, HitResult.Location, HitResult.Location.Rotation(), EAttachLocation::KeepWorldPosition, 3.f);
	}

	OnGetProjectileHitFX.Clear();
}

void ATDS_ProjectileBase::OnEnableEffect(const FEffectInfo& Info, TSubclassOf<UTDS_BaseEffect> Effect)
{
	EffectInfo = Info;
	EffectClass = Effect;
}

void ATDS_ProjectileBase::OnDisableEffect()
{
	EffectInfo = FEffectInfo();
	EffectClass = nullptr;
}

void ATDS_ProjectileBase::SetActive(const bool& bIsActive)
{
	if(BoxComponent)
	{
		BoxComponent -> SetActive(bIsActive);
		BoxComponent -> SetSimulatePhysics(bIsActive);
	}

	if (TrailComponent)
	{
		TrailComponent -> SetActive(bIsActive);
		bIsActive ? TrailComponent -> Activate() : TrailComponent -> Deactivate();
	}

	bIsHit = !bIsActive;
	SetActorTickEnabled(bIsActive);
	SetActorHiddenInGame(!bIsActive);
	SetActorEnableCollision(bIsActive);
}

void ATDS_ProjectileBase::Activate(const FVector& ShotDirection)
{
	SetActive(true);
	SetActorRotation(FRotator(0.f, ShotDirection.Rotation().Yaw, 0.f));
	CachedLaunchPosition = GetActorLocation();
	
	if (BoxComponent -> GetBodyInstance())
	{
		BoxComponent -> GetBodyInstance() -> bLockZRotation = true;
		BoxComponent -> GetBodyInstance() -> CreateDOFLock();
	}
}

void ATDS_ProjectileBase::Deactivate()
{
	if (BoxComponent -> GetBodyInstance())
	{
		BoxComponent -> GetBodyInstance() -> bLockZRotation = false;
		BoxComponent -> GetBodyInstance() -> CreateDOFLock();
	}

	if (GetOwner())
	{
		OnDeactivateProjectile.Broadcast(TWeakObjectPtr<ATDS_ProjectileBase>(this));
		SetActorLocation(GetOwner() -> GetActorLocation());
		SetActorRotation(FRotator::ZeroRotator);
		SetActive(false);
		AttachToActor(GetOwner(), FAttachmentTransformRules::KeepWorldTransform);
		
		if (GetWorld())
		{
			GetWorld() -> GetTimerManager().ClearTimer(ProjectileTrackerTimerHandle);
		}
	}
}

