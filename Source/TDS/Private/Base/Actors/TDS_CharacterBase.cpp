﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_CharacterBase.h"

#include "TDS_StringLibrary.h"
#include "WTDS_PlayerHUD.h"
#include "Base/Components/TDS_EffectsHandlerComponent.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"

ATDS_CharacterBase::ATDS_CharacterBase()
{
	PrimaryActorTick.bCanEverTick = false;

	SetupCollisionComponents();
	SetupMovementComponent();
	SetupHitBoxComponent();
	SetupEffectsHandlerComponent();
}

void ATDS_CharacterBase::SetupCollisionComponents() const
{
	if (GetCapsuleComponent())
	{
		GetCapsuleComponent() -> SetCapsuleSize(34.f, 88.f);
		GetCapsuleComponent() -> SetNotifyRigidBodyCollision(false);
		GetCapsuleComponent() -> SetGenerateOverlapEvents(false);
		GetCapsuleComponent() -> SetCollisionProfileName(FCollisionProfileLibrary::CharacterMovementCapsule);
	}
}

void ATDS_CharacterBase::SetupMovementComponent() const
{
	if (GetCharacterMovement())
	{
		GetCharacterMovement() -> bOrientRotationToMovement = false;
		GetCharacterMovement() -> RotationRate = FRotator(0.f, 640.f, 0.f);
		GetCharacterMovement() -> bConstrainToPlane = true;
		GetCharacterMovement() -> bSnapToPlaneAtStart = true;
	}
}

void ATDS_CharacterBase::SetupHitBoxComponent()
{
	if (!HitBoxComponent)
	{
		HitBoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Hit Box Component"));

		if (HitBoxComponent)
		{
			HitBoxComponent -> SetBoxExtent(FVector(40.f, 40.f, 85.f));
			HitBoxComponent -> SetNotifyRigidBodyCollision(true);
			HitBoxComponent -> SetGenerateOverlapEvents(false);
			HitBoxComponent -> SetCollisionProfileName(FCollisionProfileLibrary::HitBox);
			HitBoxComponent -> SetupAttachment(RootComponent);
			Tags.Emplace(FTagLibrary::DamageableTag);
		}
	}
}

void ATDS_CharacterBase::SetupEffectsHandlerComponent()
{
	if (!EffectsHandlerComponent)
	{
		EffectsHandlerComponent = CreateDefaultSubobject<UTDS_EffectsHandlerComponent>(TEXT("Effects Handler Component"));

		if (EffectsHandlerComponent)
		{
			AddOwnedComponent(EffectsHandlerComponent);
		}
	}
}

void ATDS_CharacterBase::SetMovementState(const EMovementState& NewMovementState)
{
	if (IsValid(GetCharacterMovement()))
	{
		if (NewMovementState == EMovementState::MS_Idle)
		{
			GetCharacterMovement() -> MaxWalkSpeed = MovementParameters.GetIdleSpeed();
			CurrentMovementState = EMovementState::MS_Idle;
		}
		else if (NewMovementState == EMovementState::MS_Walk)
		{
			FGameplayTag Slowed = FGameplayTag::RequestGameplayTag(TEXT("Status.Slowed"));
			
			if (!EffectsHandlerComponent -> HasTag(Slowed))
			{
				GetCharacterMovement() -> MaxWalkSpeed = MovementParameters.GetWalkSpeed();
			}

			CurrentMovementState = EMovementState::MS_Walk;
		}
		else if (NewMovementState == EMovementState::MS_Sprint)
		{
			GetCharacterMovement() -> MaxWalkSpeed = MovementParameters.GetSprintSpeed();
			CurrentMovementState = EMovementState::MS_Sprint;
		}
		else if (NewMovementState == EMovementState::MS_Aim)
		{
			FGameplayTag Slowed = FGameplayTag::RequestGameplayTag(TEXT("Status.Slowed"));
			
			if (!EffectsHandlerComponent -> HasTag(Slowed))
			{
				GetCharacterMovement() -> MaxWalkSpeed = MovementParameters.GetAimSpeed();
			}

			CurrentMovementState = EMovementState::MS_Aim;
		}
	}
}

float ATDS_CharacterBase::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator,
                                     AActor* DamageCauser)
{
	return Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
}

void ATDS_CharacterBase::InitComponents(const TWeakObjectPtr<UWTDS_PlayerHUD>& PlayerHUD)
{
	if (EffectsHandlerComponent)
	{
		if (PlayerHUD.IsValid() && PlayerHUD.Get() -> GetPlayerEffectsPanel().IsValid())
		{
			EffectsHandlerComponent -> InitComponent(PlayerHUD.Get() -> GetPlayerEffectsPanel());
		}
	}
}

void ATDS_CharacterBase::SetHitBoxPhysMaterial(const TWeakObjectPtr<UPhysicalMaterial>& Material)
{
	if (HitBoxComponent && Material.IsValid())
	{
		HitBoxComponent -> SetPhysMaterialOverride(Material.Get());
	}
}

TWeakObjectPtr<UTDS_EffectsHandlerComponent> ATDS_CharacterBase::GetEffectsHandlerComponent() const
{
	return EffectsHandlerComponent;
}

TWeakObjectPtr<UCharacterMovementComponent> ATDS_CharacterBase::GetCharacterMovementComponent() const
{
	return GetCharacterMovement();
}

EMovementState ATDS_CharacterBase::GetMovementState() const
{
	return CurrentMovementState;
}