﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_DamageVisualBase.h"
#include "TDS_StringLibrary.h"
#include "WTDS_DamageVisual.h"
#include "Components/WidgetComponent.h"

// Sets default values
ATDS_DamageVisualBase::ATDS_DamageVisualBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	SetupWidgetComponent();
}

void ATDS_DamageVisualBase::SetupWidgetComponent()
{
	if (!DamageWidgetComponent)
	{
		DamageWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("Damage Widget");

		if (DamageWidgetComponent)
		{
			DamageWidgetComponent -> SetWidgetSpace(EWidgetSpace::Screen);
			DamageWidgetComponent -> SetWidgetClass(LoadClass<UWTDS_DamageVisual>(this, *FPathLibrary::DamageVisualWidgetPath));
			DamageWidgetComponent -> SetDrawSize(FVector2D(25.f, 25.f));
			
			RootComponent = DamageWidgetComponent;
		}
	}
}

void ATDS_DamageVisualBase::Activate(const float& Value, const bool& bIsShielded)
{
	if (DamageVisualWidget.IsValid())
	{
		if (bIsShielded)
		{
			DamageVisualWidget.Get() -> SetShieldDamageColor();
		}
			
		DamageVisualWidget.Get() -> SetVisibility(ESlateVisibility::Visible);
		DamageVisualWidget.Get() -> PlayDamageVisual(Value);
	}
}

void ATDS_DamageVisualBase::Deactivate()
{
	Destroy();
}

void ATDS_DamageVisualBase::BeginPlay()
{
	Super::BeginPlay();

	DamageVisualWidget = Cast<UWTDS_DamageVisual>(DamageWidgetComponent -> GetUserWidgetObject());

	if (DamageVisualWidget.IsValid())
	{
		DamageVisualWidget.Get() -> SetVisibility(ESlateVisibility::Hidden);
		DamageVisualWidget.Get() -> OnDamageVisualCompleted.BindUObject(this, &ThisClass::Deactivate);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("ATDS_DamageVisualBase::OnConstruction: DamageVisualWidget is NULL"));
	}
}
