﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_PickupBase.h"
#include "NiagaraComponent.h"
#include "TDS_StringLibrary.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include "Components/WidgetComponent.h"

ATDS_PickupBase::ATDS_PickupBase()
{
	PrimaryActorTick.bCanEverTick = false;

	SetupComponents();
}

void ATDS_PickupBase::SetupComponents()
{
	if (!Root)
	{
		Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));

		if (Root)
		{
			RootComponent = Root;
		}
	}

	if (!CollisionComponent)
	{
		CollisionComponent = CreateDefaultSubobject<UBoxComponent>("Collision");

		if (CollisionComponent)
		{
			CollisionComponent -> SetSimulatePhysics(false);
			CollisionComponent -> SetNotifyRigidBodyCollision(false);
			CollisionComponent -> SetGenerateOverlapEvents(true);
			CollisionComponent -> SetCollisionProfileName(FCollisionProfileLibrary::PickupProfile);
			CollisionComponent -> SetupAttachment(RootComponent);
		}
	}

	if (!StaticMeshComponent)
	{
		StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));

		if (StaticMeshComponent)
		{
			StaticMeshComponent -> SetCollisionProfileName(FCollisionProfileLibrary::NoCollisionProfile);
			StaticMeshComponent -> SetupAttachment(RootComponent);
		}
	}

	if (!NiagaraComponent)
	{
		NiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("Niagara"));

		if (NiagaraComponent)
		{
			NiagaraComponent -> SetupAttachment(RootComponent);
		}
	}

	if (!DecalComponent)
	{
		DecalComponent = CreateDefaultSubobject<UDecalComponent>(TEXT("Decal"));

		if (DecalComponent)
		{
			DecalComponent -> SetupAttachment(RootComponent);
		}
	}

	if (!IconWidgetComponent)
	{
		IconWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("Icon Widget");

		if (IconWidgetComponent)
		{
			IconWidgetComponent -> SetWidgetSpace(EWidgetSpace::Screen);
			IconWidgetComponent -> SetDrawSize(FVector2D(25.f, 25.f));
			IconWidgetComponent -> SetupAttachment(RootComponent);
		}
	}
}

// Called when the game starts or when spawned
void ATDS_PickupBase::BeginPlay()
{
	Super::BeginPlay();
}

void ATDS_PickupBase::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	CollisionComponent -> OnComponentBeginOverlap.AddDynamic(this, &ThisClass::ATDS_PickupBase::HandleStartOverlap);
}

void ATDS_PickupBase::InitComponents()
{
}

void ATDS_PickupBase::OnDataLoaded(TSoftObjectPtr<UStaticMesh> Mesh, TSoftObjectPtr<UNiagaraSystem> NiagaraSystem, TSoftObjectPtr<UTexture2D> Texture2D)
{
}

void ATDS_PickupBase::HandleStartOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                         UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}