﻿
#include "TDS_EffectsHandlerComponent.h"
#include "TDS_BaseEffect.h"
#include "TDS_CharacterBase.h"
#include "TDS_StructLibrary.h"
#include "WTDS_EffectsPanel.h"

UTDS_EffectsHandlerComponent::UTDS_EffectsHandlerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UTDS_EffectsHandlerComponent::InitComponent(const TWeakObjectPtr<UWTDS_EffectsPanel>& Panel)
{
	if (Panel.IsValid())
	{
		EffectsPanel = Panel.Get();
	}
}

void UTDS_EffectsHandlerComponent::OnRegister()
{
	Super::OnRegister();
}

void UTDS_EffectsHandlerComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
}

void UTDS_EffectsHandlerComponent::AddEffect(AActor* Instigator, const FEffectInfo* EffectInfo, TSubclassOf<UTDS_BaseEffect> EffectClass)
{
	if (!EffectClass)
	{
		UE_LOG(LogTemp, Error, TEXT("%s::AddAction: Action Class is NULL"), *GetNameSafe(GetOwner()));
		return;
	}

	if (UTDS_BaseEffect* NewEffect = NewObject<UTDS_BaseEffect>(this, EffectClass))
	{
		if (EffectInfo)
		{
			if (UTDS_BaseEffect* Effect = FindEffect(EffectInfo -> GetID()))
			{
				Effect -> GetEffectInfo().GetIsStackable() ? ModifyEffect(Instigator, Effect) : RestartEffect(Instigator, Effect);
				NewEffect = nullptr;
			}
			else
			{
				if (NewEffect -> GetIsAutoStart() && NewEffect -> CanStart(Instigator))
				{
					NewEffect -> StartEffect(Instigator, *EffectInfo);
					EffectsArr.Emplace(NewEffect);
				}
			}
		}
	}
}

void UTDS_EffectsHandlerComponent::RestartEffect(AActor* Instigator, UTDS_BaseEffect* Effect)
{
	Effect -> RestartEffect(Instigator);

	if (EffectsPanel.IsValid())
	{
		EffectsPanel.Get() -> RestartEffect(Effect -> GetEffectInfo().GetID());
	}
}

void UTDS_EffectsHandlerComponent::ModifyEffect(AActor* Instigator, UTDS_BaseEffect* Effect)
{
	if (Effect -> GetEffectInfo().GetCurrentStacks() == Effect -> GetEffectInfo().GetMaxStacks())
	{
		return;
	}

	Effect -> ModifyEffect(Instigator);

	if (EffectsPanel.IsValid())
	{
		EffectsPanel.Get() -> ModifyEffect(Effect -> GetEffectInfo().GetID());
	}
}

void UTDS_EffectsHandlerComponent::RemoveEffect(UTDS_BaseEffect* Effect)
{
	if (Effect && !Effect -> GetIsActive())
	{
		if (EffectsPanel.IsValid())
		{
			EffectsPanel.Get() -> RemoveEffect(Effect -> GetEffectInfo().GetID());
		}
		
		EffectsArr.Remove(Effect);
	}
}

void UTDS_EffectsHandlerComponent::InterruptAllEffects()
{
	if (!EffectsArr.IsEmpty())
	{
		TArray<UTDS_BaseEffect*> Effects = EffectsArr;

		for (UTDS_BaseEffect* Effect : Effects)
		{
			if (Effect)
			{
				RemoveTags(Effect -> GetGrantTags());
				RemoveTags(Effect -> GetBlockTags());
				
				Effect -> InterruptEffect(GetOwner());
				
				if (EffectsPanel.IsValid())
				{
					EffectsPanel.Get() -> RemoveEffect(Effect -> GetEffectInfo().GetID());
				}
			}
		}
	}
}

void UTDS_EffectsHandlerComponent::AddTags(const FGameplayTagContainer& Tags)
{
	ActiveGameplayTags.AppendTags(Tags);
}

void UTDS_EffectsHandlerComponent::RemoveTags(const FGameplayTagContainer& Tags)
{
	ActiveGameplayTags.RemoveTags(Tags);
}

bool UTDS_EffectsHandlerComponent::HasTags(const FGameplayTagContainer& Tags)
{
	return ActiveGameplayTags.HasAny(Tags);
}

bool UTDS_EffectsHandlerComponent::HasTag(FGameplayTag Tag)
{
	return ActiveGameplayTags.HasTag(Tag);
}

void UTDS_EffectsHandlerComponent::EnableIcon( const FEffectInfo& Info, UTexture2D* Texture)
{
	if (EffectsPanel.IsValid())
	{
		EffectsPanel.Get() -> AddEffect(Info, Texture);
	}
}

UTDS_BaseEffect* UTDS_EffectsHandlerComponent::FindEffect(const FGuid& ID)
{
	for (UTDS_BaseEffect* Effect : EffectsArr)
	{
		if (Effect -> GetEffectInfo().GetID() == ID)
		{
			return Effect;
		}
	}

	return nullptr;
}

TWeakObjectPtr<UTDS_EffectsHandlerComponent> UTDS_EffectsHandlerComponent::GetEffectsHandlerComponent(AActor* Actor)
{
	if (Actor)
	{
		return Actor -> FindComponentByClass<UTDS_EffectsHandlerComponent>();
	}

	return nullptr;
}