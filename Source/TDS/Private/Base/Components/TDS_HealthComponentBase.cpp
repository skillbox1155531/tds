﻿#include "TDS_HealthComponentBase.h"

#include "TDS_DamageVisualBase.h"

UTDS_HealthComponentBase::UTDS_HealthComponentBase()
{
	PrimaryComponentTick.bCanEverTick = false;

	MaxHealth = 100.f;
	CurrentHealth = 0.f;
}

float UTDS_HealthComponentBase::DecreaseHealth(const float& Amount)
{
	CurrentHealth = FMath::Max(CurrentHealth - Amount, 0.f);
	
	OnHealthChanged_Delegate.Broadcast(CurrentHealth, CurrentHealth / MaxHealth);
	
	return CurrentHealth;
}

void UTDS_HealthComponentBase::SpawnDamageVisual(const float& Amount, const bool& bIsShielded)
{
	const float X = FMath::RandRange(-.1f, .1f) * 100;
	const FVector Loc = GetOwner() -> GetActorLocation() + FVector(X, 0.f, 0.f);
	const FRotator Rot = FRotator::ZeroRotator;
	FActorSpawnParameters SpawnParameters;
	SpawnParameters.Owner = GetOwner();
	
	if (ATDS_DamageVisualBase* Visual = GetWorld() -> SpawnActor<ATDS_DamageVisualBase>(ATDS_DamageVisualBase::StaticClass(), Loc, Rot, SpawnParameters))
	{
		Visual -> Activate(Amount, bIsShielded);
	}
}

bool UTDS_HealthComponentBase::IncreaseHealthPoints(const float& Amount, const bool& bIsOverTime)
{
	if (CurrentHealth != MaxHealth)
	{
		CurrentHealth = FMath::Min(MaxHealth, CurrentHealth + Amount);
		return true;
	}
	
	return false;
}

float UTDS_HealthComponentBase::GetCurrentHealth() const
{
	return CurrentHealth;
}

float UTDS_HealthComponentBase::GetMaxHealth() const
{
	return MaxHealth;
}

bool UTDS_HealthComponentBase::GetIsAlive() const
{
	return CurrentHealth > 0;
}

void UTDS_HealthComponentBase::OnRegister()
{
	Super::OnRegister();

	CurrentHealth = MaxHealth;
}