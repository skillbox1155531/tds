﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_BaseEffect.h"
#include "NiagaraComponent.h"
#include "NiagaraFunctionLibrary.h"
#include "TDS.h"
#include "TDS_CharacterBase.h"
#include "TDS_EffectData.h"
#include "TDS_EffectsHandlerComponent.h"
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"
#include "GameFramework/Character.h"

UTDS_BaseEffect::UTDS_BaseEffect()
{
	InstigatorPtr = nullptr;
	EffectVFXSystem = nullptr;
	EffectVFX = nullptr;
	EffectIcon = nullptr;
	bIsActive = false;
	bIsAutoStart = false;
}

bool UTDS_BaseEffect::CanStart(AActor* Instigator)
{
	if (bIsActive)
	{
		return false;
	}

	TWeakObjectPtr<UTDS_EffectsHandlerComponent> EffectsHandlerComponent = GetOwningComponent();
	
	return !EffectsHandlerComponent -> HasTags(BlockTags);
}

void UTDS_BaseEffect::StartEffect(AActor* Instigator, FEffectInfo EffectInfo)
{
	const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnEffectDataLoaded, EffectInfo, EffectInfo.GetEffectData(), Instigator);

	if (UAssetManager* Manager = UAssetManager::GetIfInitialized())
	{
		const TArray<FName> BundlesArr;
		Manager -> LoadPrimaryAsset(EffectInfo.GetEffectData(), BundlesArr, Delegate);
	}
}

void UTDS_BaseEffect::RestartEffect(AActor* Instigator)
{
	ClearEffectTimers(Instigator);
	SetEffectTimers();
}

void UTDS_BaseEffect::ModifyEffect(AActor* Instigator)
{
	Info.SetEffectValue(Info.GetEffectValue() * Info.GetEffectStackMultiplier());
	Info.SetCurrentStacks(Info.GetCurrentStacks() + 1);
	RestartEffect(Instigator);
}

void UTDS_BaseEffect::OnEffectDataLoaded(FEffectInfo EffectInfo, FPrimaryAssetId Data, AActor* Instigator)
{
	if (const UAssetManager* Manager = UAssetManager::GetIfInitialized())
	{
		if (UTDS_EffectData* EffectData = Cast<UTDS_EffectData>(Manager -> GetPrimaryAssetObject(Data)))
		{
			Info = EffectInfo;
			EffectVFXSystem = EffectData -> EffectVFX;
			EffectIcon = EffectData -> EffectIcon;

			TWeakObjectPtr<UTDS_EffectsHandlerComponent> EffectsHandlerComponent = GetOwningComponent();
	
			EffectsHandlerComponent.Get() -> AddTags(GrantTags);
			EffectsHandlerComponent.Get() -> AddTags(BlockTags);
			EffectsHandlerComponent.Get() -> EnableIcon(EffectInfo, EffectIcon);
			
			bIsActive = true;
			InstigatorPtr = Instigator;
			
			SetEffectTimers();
		}
		else
		{
			FString LogMsg = FString::Printf(TEXT("%s::OnActionDataLoaded: Couldn't Load Data"), *GetNameSafe(this));
			LogOnScreen(this, LogMsg, FColor::Red);
		}
	}
}

void UTDS_BaseEffect::StopEffect(AActor* Instigator)
{
	ClearEffectTimers(Instigator);
	
	TWeakObjectPtr<UTDS_EffectsHandlerComponent> EffectsHandlerComponent = GetOwningComponent();

	EffectsHandlerComponent.Get() -> RemoveTags(GrantTags);
	EffectsHandlerComponent.Get() -> RemoveTags(BlockTags);
	
	bIsActive = false;
	InstigatorPtr = Instigator;

	EffectsHandlerComponent.Get() -> RemoveEffect(this);

	if (EffectVFX)
	{
		EffectVFX -> DestroyComponent();
	}
}

void UTDS_BaseEffect::InterruptEffect(AActor* Instigator)
{
	GetWorld() -> GetTimerManager().ClearTimer(EffectDuration_TimerHandle);
	GetWorld() -> GetTimerManager().ClearTimer(EffectRate_TimerHandle);
	
	bIsActive = false;
	InstigatorPtr = Instigator;

	if (EffectVFX)
	{
		EffectVFX -> DestroyComponent();
	}
}

bool UTDS_BaseEffect::GetIsActive() const
{
	return bIsActive;
}

bool UTDS_BaseEffect::GetIsAutoStart() const
{
	return bIsAutoStart;
}

TWeakObjectPtr<UTDS_EffectsHandlerComponent> UTDS_BaseEffect::GetOwningComponent() const
{
	return Cast<UTDS_EffectsHandlerComponent>(GetOuter());
}

UWorld* UTDS_BaseEffect::GetWorld() const
{
	if (const UActorComponent* Component = Cast<UActorComponent>(GetOuter()))
	{
		return Component -> GetWorld();
	}
	
	return nullptr;
}

void UTDS_BaseEffect::ApplyInstantEffect(AActor* Instigator)
{
}

void UTDS_BaseEffect::ApplyPeriodicEffect(AActor* Instigator)
{
	//LogOnScreen(GetOwningComponent().Get(), TEXT("%s::ApplyPeriodicEffect: Burn"), FColor::Orange);
}

void UTDS_BaseEffect::SetEffectTimers()
{
	if (Info.GetEffectDuration() > 0.f)
	{
		GetWorld() -> GetTimerManager().SetTimer(EffectDuration_TimerHandle, [&] { StopEffect(InstigatorPtr); }, Info.GetEffectDuration(), false);
		
		if (Info.GetEffectRate() > 0.f)
		{
			GetWorld() -> GetTimerManager().SetTimer(EffectRate_TimerHandle, [&] { ApplyPeriodicEffect(InstigatorPtr); }, Info.GetEffectRate(), true);
		}
		else
		{
			ApplyInstantEffect(InstigatorPtr);
		}
	}
}

void UTDS_BaseEffect::ClearEffectTimers(AActor* Instigator)
{
	if (GetWorld() -> GetTimerManager().GetTimerRemaining(EffectRate_TimerHandle) < KINDA_SMALL_NUMBER)
	{
		ApplyPeriodicEffect(Instigator);
	}

	GetWorld() -> GetTimerManager().ClearTimer(EffectDuration_TimerHandle);
	GetWorld() -> GetTimerManager().ClearTimer(EffectRate_TimerHandle);
}

void UTDS_BaseEffect::SpawnEffectVFX(AActor* Instigator, FName Socket)
{
	if (ensure(EffectVFXSystem))
	{
		EffectVFX = UNiagaraFunctionLibrary::SpawnSystemAttached(EffectVFXSystem, Instigator -> GetRootComponent(), NAME_None, FVector::ZeroVector, FQuat::Identity.Rotator(), EAttachLocation::SnapToTarget, false);
		
		if (!EffectVFX)
		{
			UE_LOG(LogTemp, Error, TEXT("%s::SpawnEffectVFX: Couldn't spawn VFX"), *GetNameSafe(this));
		}
	}
}

FEffectInfo UTDS_BaseEffect::GetEffectInfo() const
{
	return Info;
}

UTexture2D* UTDS_BaseEffect::GetIcon() const
{
	return EffectIcon;
}

FGameplayTagContainer UTDS_BaseEffect::GetGrantTags() const
{
	return GrantTags;
}

FGameplayTagContainer UTDS_BaseEffect::GetBlockTags() const
{
	return BlockTags;
}
