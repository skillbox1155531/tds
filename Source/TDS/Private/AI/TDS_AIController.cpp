﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_AIController.h"
#include "TDS.h"
#include "TDS_EnemyCharacter.h"
#include "TDS_EnemyCombat.h"
#include "TDS_PlayerHealthComponent.h"
#include "TDS_StringLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Damage.h"
#include "Perception/AISenseConfig_Hearing.h"
#include "Perception/AISenseConfig_Sight.h"

ATDS_AIController::ATDS_AIController()
{
	PrimaryActorTick.bCanEverTick = true;
	BehaviorTree = nullptr;

	SightRadius = 500.f;
	SightLoseRadius = 800.f;
	SightAngle = 180.f;

	HearingRange = 550.f;
	AlarmedHearingRange = 850.f;

	InvestigationTime = 10.f;
	
	InitPerceptionComponent();
}

void ATDS_AIController::AttackTarget()
{
	if (AActor* TargetActor = Cast<AActor>(GetBlackboardComponent() -> GetValueAsObject(UBlackboardKeyLibrary::TargetActorKey)))
	{
		UE_LOG(LogTemp, Warning, TEXT("ATDS_AIController::AttackTarget: %s starts Attack"), *GetNameSafe(GetPawn()))
		ITDS_EnemyCombat::Execute_StartAttackTarget(GetPawn(), TargetActor);
	}
}

void ATDS_AIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);

	if (ensureMsgf(BehaviorTree, TEXT("AAR_AIController::OnPossess: BT is NULL! Provide a valid path")))
	{
		RunBehaviorTree(BehaviorTree);

		GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::CanAttackKey, true);
		ITDS_Actor::Execute_SetActorMovementStateInt(GetPawn(), EMovementState::MS_Walk);
	}
}

void ATDS_AIController::BeginPlay()
{
	Super::BeginPlay();

	if (UAISenseConfig_Hearing* HearingConfigPtr = Cast<UAISenseConfig_Hearing>(AIPerceptionComponent -> GetSenseConfig(UAISense::GetSenseID<UAISense_Hearing>())))
	{
		if (HearingRange != HearingConfigPtr -> HearingRange)
		{
			HearingRange = HearingConfigPtr -> HearingRange;
		}
	}
}

void ATDS_AIController::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	AIPerceptionComponent -> OnTargetPerceptionUpdated.AddDynamic(this, &ThisClass::ATDS_AIController::OnTargetDetected);
}

void ATDS_AIController::InitPerceptionComponent()
{
	if (!AIPerceptionComponent)
	{
		AIPerceptionComponent = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("Perception Component"));

		if (AIPerceptionComponent)
		{
			SightConfig = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("Sight Config"));

			if (SightConfig)
			{
				SightConfig -> SightRadius = SightRadius;
				SightConfig -> LoseSightRadius = SightRadius;
				SightConfig -> PeripheralVisionAngleDegrees = SightAngle;
				SightConfig -> SetMaxAge(5.f);
				SightConfig -> AutoSuccessRangeFromLastSeenLocation = SightRadius / 2.f;
				SightConfig -> DetectionByAffiliation.bDetectEnemies = true;
				SightConfig -> DetectionByAffiliation.bDetectFriendlies = true;
				SightConfig -> DetectionByAffiliation.bDetectNeutrals = true;
				
				AIPerceptionComponent -> ConfigureSense(*SightConfig);
				AIPerceptionComponent -> SetDominantSense(*SightConfig -> GetSenseImplementation());
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("%s::InitPerceptionComponent: Failed to create Sight Config"), *GetNameSafe(this));
			}
			
			HearingConfig = CreateDefaultSubobject<UAISenseConfig_Hearing>(TEXT("Hearing Config"));

			if (HearingConfig)
			{
				HearingConfig -> HearingRange = HearingRange;
				HearingConfig -> SetMaxAge(5.f);
				HearingConfig -> DetectionByAffiliation.bDetectEnemies = true;
				HearingConfig -> DetectionByAffiliation.bDetectFriendlies = true;
				HearingConfig -> DetectionByAffiliation.bDetectNeutrals = true;
			
				AIPerceptionComponent -> ConfigureSense(*HearingConfig);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("%s::InitPerceptionComponent: Failed to create Hearing Config"), *GetNameSafe(this));
			}
			
			DamageConfig = CreateDefaultSubobject<UAISenseConfig_Damage>(TEXT("Damage Config"));

			if (DamageConfig)
			{
				DamageConfig -> SetMaxAge(5.f);
				
				AIPerceptionComponent -> ConfigureSense(*DamageConfig);
			}
			else
			{
				UE_LOG(LogTemp, Error, TEXT("%s::InitPerceptionComponent: Failed to create Damage Config"), *GetNameSafe(this));
			}
		}
	}
}

void ATDS_AIController::DisableSenses()
{
	AIPerceptionComponent -> OnTargetPerceptionUpdated.Clear();
}

void ATDS_AIController::OnTargetDetected(AActor* Actor, const FAIStimulus Stimulus)
{
	if (Actor -> ActorHasTag(FTagLibrary::PlayerTag) && ITDS_Actor::Execute_GetIsAliveInt(Actor))
	{
		if (Stimulus.WasSuccessfullySensed())
		{
			OnTargetBeingSensed(Actor);

			if (Stimulus.Type == UAISense::GetSenseID<UAISense_Sight>())
			{
				UE_LOG(LogTemp, Warning, TEXT("OnTargetDetected: Target's in Line of Sight"));
				OnTargetBeingSeen(Actor);
			}
			else if (Stimulus.Type== UAISense::GetSenseID<UAISense_Hearing>())
			{
				OnTargetBeingHeard(Actor, Stimulus);
			}
			else if (Stimulus.Type == UAISense::GetSenseID<UAISense_Damage>())
			{
				OnPawnBeingDamaged(Actor);
			}
		}
		else
		{
			OnTargetBeingLost(Stimulus);
		}
	}
}

void ATDS_AIController::OnTargetBeingSensed(AActor* Actor)
{
	//UE_LOG(LogTemp, Warning, TEXT("ATDS_AIController::OnTargetDetected: %s detected %s"), *GetNameSafe(GetPawn()), *GetNameSafe(Actor));
	
	GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::IsAlarmedKey, true);
	SetInvestigationTimer(false);
}

void ATDS_AIController::OnTargetBeingSeen(AActor* Actor)
{
	//UE_LOG(LogTemp, Warning, TEXT("ATDS_AIController::OnTargetDetected: %s saw %s"), *GetNameSafe(GetPawn()), *GetNameSafe(Actor));
	GetBlackboardComponent() -> SetValueAsObject(UBlackboardKeyLibrary::TargetActorKey, Actor);

	if (ATDS_EnemyCharacter* EnemyCharacter = Cast<ATDS_EnemyCharacter>(GetPawn()))
	{
		EnemyCharacter -> GetCharacterMovement() -> bOrientRotationToMovement = false;
		EnemyCharacter -> SetMovementState(EMovementState::MS_Sprint);
	}
}

void ATDS_AIController::OnTargetBeingHeard(const AActor* Actor, const FAIStimulus& Stimulus)
{
	//UE_LOG(LogTemp, Warning, TEXT("ATDS_AIController::OnTargetDetected: %s heard %s"), *GetNameSafe(GetPawn()), *GetNameSafe(Actor));

	if (UAISenseConfig_Hearing* HearingConfigPtr = Cast<UAISenseConfig_Hearing>(AIPerceptionComponent -> GetSenseConfig(UAISense::GetSenseID<UAISense_Hearing>())))
	{
		HearingConfigPtr -> HearingRange = AlarmedHearingRange;
	}
	
	if (!GetBlackboardComponent() -> GetValueAsObject(UBlackboardKeyLibrary::TargetActorKey))
	{
		GetBlackboardComponent() -> SetValueAsVector(UBlackboardKeyLibrary::InvestigationLocationKey, Stimulus.StimulusLocation);
		SetInvestigationTimer(true);
	}
}

void ATDS_AIController::OnPawnBeingDamaged(AActor* Actor)
{
	//UE_LOG(LogTemp, Warning, TEXT("ATDS_AIController::OnTargetDetected: %s damaged %s"), *GetNameSafe(Actor), *GetNameSafe(GetPawn()));
	
	if (!GetBlackboardComponent() -> GetValueAsObject(UBlackboardKeyLibrary::TargetActorKey))
	{
		GetBlackboardComponent() -> SetValueAsObject(UBlackboardKeyLibrary::TargetActorKey, Actor);

		if (ATDS_EnemyCharacter* EnemyCharacter = Cast<ATDS_EnemyCharacter>(GetPawn()))
		{
			EnemyCharacter -> GetCharacterMovement() -> bOrientRotationToMovement = false;
			EnemyCharacter -> SetMovementState(EMovementState::MS_Sprint);
		}
	}
}

void ATDS_AIController::OnTargetBeingLost(const FAIStimulus Stimulus)
{
	if (Stimulus.Type== UAISense::GetSenseID<UAISense_Hearing>())
	{
		return;
	}

	DrawDebugSphere(GetWorld(), Stimulus.StimulusLocation, 20, 30, FColor::Red, false, 10.f);
	DrawDebugString(GetWorld(), Stimulus.StimulusLocation, TEXT("Lost Target Here"), nullptr, FColor::Black, 10.f);

	GetBlackboardComponent() -> SetValueAsObject(UBlackboardKeyLibrary::TargetActorKey, nullptr);
	GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::IsAlarmedKey, true);
	GetBlackboardComponent() -> SetValueAsVector(UBlackboardKeyLibrary::InvestigationLocationKey, Stimulus.StimulusLocation);

	if (ATDS_EnemyCharacter* EnemyCharacter = Cast<ATDS_EnemyCharacter>(GetPawn()))
	{
		EnemyCharacter -> GetCharacterMovement() -> bOrientRotationToMovement = true;
		EnemyCharacter -> SetMovementState(EMovementState::MS_Walk);
	}
	 
	SetInvestigationTimer(true);
}

void ATDS_AIController::SetInvestigationTimer(const bool& bIsActive)
{
	bIsActive ? GetWorld() -> GetTimerManager().SetTimer(InvestigationTimeElapsed_TimerHandle, this, &ThisClass::ResetAlarmedState, InvestigationTime, false)
	: GetWorld() -> GetTimerManager().ClearTimer(InvestigationTimeElapsed_TimerHandle);
}

void ATDS_AIController::ResetAlarmedState()
{
	FString DebugMsg = FString::Printf(TEXT("ATDS_AIController::SetInvestigationTimer: %s finished investigating"), *GetNameSafe(GetPawn()));
	LogOnScreen(this, DebugMsg, FColor::Green);

	if (UAISenseConfig_Hearing* HearingConfigPtr = Cast<UAISenseConfig_Hearing>(AIPerceptionComponent -> GetSenseConfig(UAISense::GetSenseID<UAISense_Hearing>())))
	{
		HearingConfigPtr -> HearingRange = HearingRange;
	}
	
	GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::IsAlarmedKey, false);
}

