﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_BossCharacter.h"

#include "AIController.h"
#include "TDS_AIController.h"
#include "TDS_PlayerHealthComponent.h"
#include "TDS_StringLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Boss_Weapon/TDS_Boss_RocketLauncher.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ATDS_BossCharacter::ATDS_BossCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BossSpawnSound = nullptr;
	RocketLauncher = nullptr;
	AttackDelay = 10.f;
	
	MinionClass = nullptr;
	BossEnrageMontage = nullptr;
	BossEnrageSound = nullptr;
	MinionsSpawnQuery = nullptr;
	MinionsAmount = 5;
}

// Called when the game starts or when spawned
void ATDS_BossCharacter::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), BossSpawnSound, GetActorLocation());

	if (ensure(RocketLauncherClass))
	{
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;
		
		RocketLauncher = GetWorld() -> SpawnActor<ATDS_Boss_RocketLauncher>(RocketLauncherClass, SpawnParameters);

		if (RocketLauncher)
		{
			const FAttachmentTransformRules AttachmentTransformRules(EAttachmentRule::SnapToTarget, false);
			RocketLauncher -> AttachToComponent(GetMesh(), AttachmentTransformRules, FSocketLibrary::BossWeaponSocket);

			GetWorld() -> GetTimerManager().SetTimer(Attack_TimerHandle, this, &ThisClass::AttackElapsed, AttackDelay, true, 5.f);
		}
	}

	if (HealthComponent)
	{
		HealthComponent -> OnShieldWasDepleted_Delegate.AddUObject(this, &ThisClass::GetEnraged);
	}

	if (ATDS_AIController* MyAIController = Cast<ATDS_AIController>(GetController()))
	{
		MyAIController -> DisableSenses();
		MyAIController -> GetBlackboardComponent() -> SetValueAsObject(UBlackboardKeyLibrary::TargetActorKey, UGameplayStatics::GetPlayerPawn(GetWorld(), 0));
	}
}

void ATDS_BossCharacter::AttackElapsed()
{
	if (AAIController* AIController = Cast<AAIController>(GetController()))
	{
		if (AActor* Target =  Cast<AActor>(AIController -> GetBlackboardComponent() -> GetValueAsObject(UBlackboardKeyLibrary::TargetActorKey)))
		{
			RocketLauncher -> SetSpawnRocketsTimer(Target);
		}
	}
}

void ATDS_BossCharacter::GetEnraged()
{
	if (ensure(BossEnrageMontage))
	{
		UE_LOG(LogTemp, Display, TEXT("Enraging the Energing Montage"));

		GetMesh() -> GetAnimInstance() -> StopAllMontages(1.f);
		GetCharacterMovement() -> StopActiveMovement();
		GetCharacterMovement() -> MovementMode = MOVE_None;
		SetActorMovementStateInt_Implementation(EMovementState::MS_Idle);
		HealthComponent -> SetIsImmune(true);
		HealthComponent -> RefillHealth();
		GetWorld() -> GetTimerManager().ClearTimer(Attack_TimerHandle);
		
		AAIController* MyAIController = Cast<AAIController>(GetController());
		MyAIController -> GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::CanAttackKey, false);
		
		GetMesh() -> GetAnimInstance() -> OnMontageEnded.AddDynamic(this, &ThisClass::OnBossEnrageAnimEnded);
		PlayAnimMontage(BossEnrageMontage);
	}
}

void ATDS_BossCharacter::NotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	Super::NotifyBegin(NotifyName, BranchingPointPayload);

	if (NotifyName == FAnimNotifyLibrary::EnrageNotify)
	{
		UE_LOG(LogTemp, Display, TEXT("Enraging Sound"));
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), BossEnrageSound, GetActorLocation());
	}
}

void ATDS_BossCharacter::OnBossEnrageAnimEnded(UAnimMontage* Montage, bool bInterrupted)
{
	if (Montage == BossEnrageMontage)
	{
		GetCharacterMovement() -> MovementMode = MOVE_Walking;
		HealthComponent -> SetIsImmune(false);
		AAIController* MyAIController = Cast<AAIController>(GetController());
		MyAIController -> GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::CanAttackKey, true);
		SetActorMovementStateInt_Implementation(EMovementState::MS_Walk);
		
		if (RocketLauncher)
		{
			GetWorld() -> GetTimerManager().SetTimer(Attack_TimerHandle, this, &ThisClass::AttackElapsed, AttackDelay, true, 5.f);
		}
		
		UE_LOG(LogTemp, Display, TEXT("Montage Ended"));
		RunEQS();
	}
}

void ATDS_BossCharacter::RunEQS()
{
	if (ensureMsgf(MinionsSpawnQuery, TEXT("ATDS_GameMode::SpawnEnemies: SpawnBotQuery is NULL")))
	{
		if (UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(this, MinionsSpawnQuery, this, EEnvQueryRunMode::AllMatching, nullptr))
		{
			QueryInstance -> GetOnQueryFinishedEvent().AddDynamic(this, &ThisClass::OnMinionSpawnQueryCompleted);
		}
	}
}

void ATDS_BossCharacter::OnMinionSpawnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance,
	EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		return;
	}

	TArray<FVector> LocationsArr;
	QueryInstance -> GetQueryResultsAsLocations(LocationsArr);

	if (LocationsArr.IsEmpty())
	{
		return;
	}

	AAIController* MyAIController = Cast<AAIController>(GetController());
	UObject* TargetActor = MyAIController -> GetBlackboardComponent() -> GetValueAsObject(UBlackboardKeyLibrary::TargetActorKey);
	
	for (int i = 0; i < MinionsAmount; ++i)
	{
		if (LocationsArr.IsValidIndex(i))
		{
			FActorSpawnParameters SpawnParameters;
			SpawnParameters.Owner = this;
			SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
			
			if (ATDS_EnemyCharacter* Minion = GetWorld() -> SpawnActor<ATDS_EnemyCharacter>(MinionClass, LocationsArr[i], FQuat::Identity.Rotator(), SpawnParameters))
			{
				if (ATDS_AIController* AIController = Cast<ATDS_AIController>(Minion -> GetController()))
				{
					if (TargetActor)
					{
						AIController -> DisableSenses();
						AIController -> GetBlackboardComponent() -> SetValueAsObject(UBlackboardKeyLibrary::TargetActorKey, TargetActor);
						Minion -> SetActorMovementStateInt_Implementation(EMovementState::MS_Sprint);
						MinionsArr.Emplace(Minion);
					}
				}
			}
		}
	}
}

void ATDS_BossCharacter::DeathLogic(AActor* DamageCauser)
{
	Super::DeathLogic(DamageCauser);

	if (RocketLauncher)
	{
		RocketLauncher -> SetActorHiddenInGame(true);
		RocketLauncher -> SetLifeSpan(1.f);
	}

	for (AActor* Minion : MinionsArr)
	{
		if (Minion)
		{
			Minion -> SetActorHiddenInGame(true);
			Minion -> SetLifeSpan(1.f);
		}
	}
}
