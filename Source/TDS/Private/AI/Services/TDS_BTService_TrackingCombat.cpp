﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_BTService_TrackingCombat.h"
#include "AIController.h"
#include "TDS_Actor.h"
#include "TDS_CharacterBase.h"
#include "TDS_EnemyCombat.h"
#include "TDS_PlayerHealthComponent.h"
#include "TDS_StringLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"


UTDS_BTService_TrackingCombat::UTDS_BTService_TrackingCombat()
{
	AttackRange = 250.f;
}

void UTDS_BTService_TrackingCombat::OnSearchStart(FBehaviorTreeSearchData& SearchData){
	
	Super::OnSearchStart(SearchData);
}

void UTDS_BTService_TrackingCombat::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
	
	if (OwnerComp.GetAIOwner() -> GetBlackboardComponent() -> GetValueAsObject(UBlackboardKeyLibrary::TargetActorKey))
	{
		APawn* Pawn = OwnerComp.GetAIOwner() -> GetPawn();
		UObject* Target = OwnerComp.GetBlackboardComponent() -> GetValueAsObject(UBlackboardKeyLibrary::TargetActorKey);

		const FVector TargetLocation = ITDS_Actor::Execute_GetActorLocationInt(Target);
		const float Distance = FVector::Distance(TargetLocation, Pawn -> GetActorLocation());
		const bool bIsInRange = Distance < AttackRange && OwnerComp.GetAIOwner() -> LineOfSightTo(Cast<AActor>(Target));
		
		OwnerComp.GetAIOwner() -> GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::IsInAttackRangeKey, bIsInRange);

		if (!bIsInRange && !OwnerComp.GetAIOwner() -> GetBlackboardComponent() -> GetValueAsBool(UBlackboardKeyLibrary::CanAttackKey))
		{
			ITDS_EnemyCombat::Execute_InterruptAttackTarget(Pawn);
		}

		if (!ITDS_Actor::Execute_GetIsAliveInt(Target))
		{
			ITDS_EnemyCombat::Execute_InterruptAttackTarget(Pawn);
			ITDS_Actor::Execute_SetActorMovementStateInt(Pawn, EMovementState::MS_Walk);
			
			OwnerComp.GetBlackboardComponent() -> SetValueAsObject(UBlackboardKeyLibrary::TargetActorKey, nullptr);
			OwnerComp.GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::IsAlarmedKey, false);
		}
	}
}
