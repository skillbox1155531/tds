﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_BTTask_FindNextLocationPoint.h"

#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Services/BTService_RunEQS.h"
#include "EnvironmentQuery/EnvQuery.h"
#include "EnvironmentQuery/EnvQueryInstanceBlueprintWrapper.h"
#include "EnvironmentQuery/EnvQueryManager.h"

EBTNodeResult::Type UTDS_BTTask_FindNextLocationPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	if (ensureMsgf(FindLocationQuery, TEXT("UTDS_BTTask_FindNextLocationPoint::ExecuteTask: FindLocationQuery is NULL")))
	{
		if (UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(this, FindLocationQuery, OwnerComp.GetAIOwner(), EEnvQueryRunMode::AllMatching, nullptr))
		{
			//UE_LOG(LogTemp, Warning, TEXT("UTDS_BTTask_FindNextLocationPoint::ExecuteTask:: Starting Query for %s"), *GetNameSafe(OwnerComp.GetAIOwner() -> GetPawn()));
			QueryInstance -> GetOnQueryFinishedEvent().AddDynamic(this, &ThisClass::OnQueryCompleted);

			return EBTNodeResult::InProgress;
		}
	}
	

	return EBTNodeResult::Failed;
}

void UTDS_BTTask_FindNextLocationPoint::OnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance, EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		FinishTask(QueryInstance -> GetQueryResult() -> Owner, EBTNodeResult::Failed);
	}

	TArray<FVector> LocationsArr;

	QueryInstance -> GetQueryResultsAsLocations(LocationsArr);

	if (!LocationsArr.IsEmpty())
	{
		UBlackboardComponent* BlackboardComponent = GetBlackboardComponent(QueryInstance -> GetQueryResult() -> Owner);
		const int32 Index = FMath::RandRange(0, LocationsArr.Num() - 1);

		if (LocationsArr.IsValidIndex(Index))
		{
			if (BlackboardComponent)
			{
				BlackboardComponent -> SetValueAsVector(TargetLocationKey.SelectedKeyName, LocationsArr[Index]);

				FinishTask(QueryInstance -> GetQueryResult() -> Owner, EBTNodeResult::Succeeded);
				return;
			}
		}
	}

	FinishTask(QueryInstance -> GetQueryResult() -> Owner, EBTNodeResult::Failed);
}

void UTDS_BTTask_FindNextLocationPoint::FinishTask(const TWeakObjectPtr<UObject> Owner, EBTNodeResult::Type TaskResult) const
{
	if (const APawn* AIPawn = Cast<APawn>(Owner.Get()))
	{
		if (const AAIController* AIController = Cast<AAIController>(AIPawn -> GetController()))
		{
			if (UBehaviorTreeComponent* BehaviorTreeComponent = Cast<UBehaviorTreeComponent>(AIController -> BrainComponent))
			{
				FinishLatentTask(*BehaviorTreeComponent, TaskResult);

				if (TaskResult == EBTNodeResult::Aborted || TaskResult == EBTNodeResult::Failed)
				{
					UE_LOG(LogTemp, Error, TEXT("UTDS_BTTask_FindNextLocationPoint::FinishTask: Task failed on %s"), *GetNameSafe(Owner.Get()));
				}
			}
		}
	}
}

UBlackboardComponent* UTDS_BTTask_FindNextLocationPoint::GetBlackboardComponent(const TWeakObjectPtr<UObject> Owner) const
{
	if (const APawn* AIPawn = Cast<APawn>(Owner.Get()))
	{
		if (AAIController* AIController = Cast<AAIController>(AIPawn -> GetController()))
		{
			return AIController -> GetBlackboardComponent();
		}
	}

	return nullptr;
}
