﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_BTTask_ExecuteAttack.h"

#include "TDS_AIController.h"
#include "TDS_StringLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"

void UTDS_BTTask_ExecuteAttack::OnInstanceCreated(UBehaviorTreeComponent& OwnerComp)
{
	Super::OnInstanceCreated(OwnerComp);
}

EBTNodeResult::Type UTDS_BTTask_ExecuteAttack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	//UE_LOG(LogTemp, Warning, TEXT("UTDS_BTTask_ExecuteAttack::ExecuteTask: Starting Attack"));

	if (OwnerComp.GetAIOwner() -> GetBlackboardComponent() -> GetValueAsBool(UBlackboardKeyLibrary::CanAttackKey))
	{
		if (ATDS_AIController* AIController = Cast<ATDS_AIController>(OwnerComp.GetAIOwner()))
		{
			AIController -> AttackTarget();
			return EBTNodeResult::Succeeded;
		}
		
		UE_LOG(LogTemp, Error, TEXT("UTDS_BTTask_ExecuteAttack::ExecuteTask: Controller is NULL"));
	
		return EBTNodeResult::Failed;
	}
	
	return EBTNodeResult::Failed;
}


