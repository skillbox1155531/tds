﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boss_Weapon/TDS_Boss_Rocket.h"
#include "NiagaraComponent.h"
#include "NiagaraSystem.h"
#include "TDS_StringLibrary.h"
#include "Boss_Weapon/TDS_Boss_HomingActor.h"
#include "Components/AudioComponent.h"
#include "Components/BoxComponent.h"
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"


// Sets default values
ATDS_Boss_Rocket::ATDS_Boss_Rocket()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	if (!BoxComp)
	{
		BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));
		BoxComp -> SetCollisionProfileName(FCollisionProfileLibrary::ProjectileProfile);
		RootComponent = BoxComp;
	}
	
	if (!MeshComp)
	{
		MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
		MeshComp -> SetupAttachment(RootComponent);
	}
	
	if (!RocketMovementComp)
	{
		RocketMovementComp = CreateDefaultSubobject<UProjectileMovementComponent>("RocketMovement");

		if (RocketMovementComp)
		{
			//vars
		}
	}

	if (!RocketTrailComp)
	{
		RocketTrailComp = CreateDefaultSubobject<UNiagaraComponent>(TEXT("RocketTrail"));
		RocketTrailComp -> SetupAttachment(MeshComp);
	}

	if (!AudioComp)
	{
		AudioComp = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComp"));
		AudioComp -> SetupAttachment(MeshComp);
	}

	HeightTrackRate = 1.f;
	HeightThreshold = 2000.f;

	SpiralUpdateRate = 0.01f;
	BaseSpiralSpeed = 10.f;
	SpiralRadiusMin = 40.f;
	SpiralRadiusMax = 60.f;
}

// Called when the game starts or when spawned
void ATDS_Boss_Rocket::BeginPlay()
{
	Super::BeginPlay();

	LoadStaticAssets();

	GetWorld() -> GetTimerManager().SetTimer(HeightTrack_TimerHandle, this, &ThisClass::SetHomingActor, HeightTrackRate, true, 0.f);
	GetWorld() -> GetTimerManager().SetTimer(Spiral_TimerHandle, this, &ThisClass::HandleSpiralMovement, SpiralUpdateRate, true, 0.f);
}

void ATDS_Boss_Rocket::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
}

void ATDS_Boss_Rocket::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (BoxComp)
	{
		BoxComp -> OnComponentHit.AddDynamic(this, &ATDS_Boss_Rocket::OnHit);
	}
}

void ATDS_Boss_Rocket::LoadStaticAssets()
{
	FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
	const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnStaticAssetsLoaded);
	
	TArray<FSoftObjectPath> ObjectsToLoadArr;
	ObjectsToLoadArr.Emplace(RocketInfo.Trail.ToSoftObjectPath());
	ObjectsToLoadArr.Emplace(RocketInfo.TrailSound.ToSoftObjectPath());

	Manager.RequestAsyncLoad(ObjectsToLoadArr, Delegate);
}

void ATDS_Boss_Rocket::OnStaticAssetsLoaded()
{
	if (RocketInfo.Trail.IsValid())
	{
		if (UNiagaraSystem* Trail = RocketInfo.Trail.Get())
		{
			RocketTrailComp -> SetAsset(Trail);
		}
	}

	if (RocketInfo.TrailSound.IsValid())
	{
		if (USoundCue* TrailSound = RocketInfo.TrailSound.Get())
		{
			// AudioComp -> SetSound(TrailSound);
			// AudioComp -> Play();
		}
	}
}

void ATDS_Boss_Rocket::SetHomingActor()
{
	const float Distance = FVector::Distance(SpawnLocation, GetActorLocation());

	if (Distance >= HeightThreshold)
	{
		RocketMovementComp -> bIsHomingProjectile = true;
		RocketMovementComp -> HomingTargetComponent = TargetActor -> GetRootComponent();

		GetWorld() -> GetTimerManager().ClearTimer(HeightTrack_TimerHandle);
	}
}

void ATDS_Boss_Rocket::HandleSpiralMovement()
{
	const float Time = GetWorld() -> GetTimeSeconds() * BaseSpiralSpeed;
	const float LocY = FMath::Cos(Time) * FMath::RandRange(SpiralRadiusMin, SpiralRadiusMax);
	const float LocZ = FMath::Sin(Time) * FMath::RandRange(SpiralRadiusMin, SpiralRadiusMax);

	MeshComp -> SetRelativeLocation(FVector(0.f, LocY, LocZ));
}

void ATDS_Boss_Rocket::InitRocket(ATDS_Boss_HomingActor* Target)
{
	if (Target)
	{
		Target -> SetRadius(RocketInfo.OuterRadius);
		TargetActor = Target;
		SpawnLocation = GetActorLocation();
	}
}


void ATDS_Boss_Rocket::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	TArray<AActor*> IgnoredActors;
	IgnoredActors.Emplace(GetOwner() -> GetOwner());
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(), RocketInfo.Damage, RocketInfo.Damage / 2.f, TargetActor -> GetActorLocation(), RocketInfo.InnerRadius, RocketInfo.OuterRadius, RocketInfo.DamageFalloff, nullptr, IgnoredActors);

	// DrawDebugSphere(GetWorld(), TargetActor -> GetActorLocation(), RocketInfo.OuterRadius, 20, FColor::Yellow, true, 5.f);
	// DrawDebugSphere(GetWorld(), TargetActor -> GetActorLocation(), RocketInfo.InnerRadius, 20, FColor::Red, true, 5.f);

	TargetActor -> LoadAssets();
	SetActorEnableCollision(false);
	SetActorHiddenInGame(true);
	SetLifeSpan(5.f);

	GetWorld() -> GetTimerManager().ClearTimer(Spiral_TimerHandle);
}



