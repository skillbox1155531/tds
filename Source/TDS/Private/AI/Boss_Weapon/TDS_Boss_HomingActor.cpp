﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boss_Weapon/TDS_Boss_HomingActor.h"

#include "NiagaraFunctionLibrary.h"
#include "Components/DecalComponent.h"
#include "Engine/AssetManager.h"
#include "Engine/StreamableManager.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Sound/SoundCue.h"


// Sets default values
ATDS_Boss_HomingActor::ATDS_Boss_HomingActor(): OuterRadius(0)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	if (!MeshComp)
	{
		MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComp"));
		RootComponent = MeshComp;
	}
	
	if (!HitDecalComp)
	{
		HitDecalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("HitDecal"));
		HitDecalComp -> DecalSize = FVector(0.f);
		HitDecalComp -> SetWorldRotation(FRotator(90.f,0.f,0.f));
		HitDecalComp -> SetHiddenInGame(true);
		HitDecalComp -> SetupAttachment(RootComponent);
	}
}

// Called when the game starts or when spawned
void ATDS_Boss_HomingActor::BeginPlay()
{
	Super::BeginPlay();

	FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
	const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnSpawnDecal);

	Manager.RequestAsyncLoad(HitDecal.ToSoftObjectPath(), Delegate);
}

void ATDS_Boss_HomingActor::BeginDestroy()
{
	Super::BeginDestroy();
}

void ATDS_Boss_HomingActor::LoadAssets()
{
	FStreamableManager& Manager = UAssetManager::GetIfInitialized() -> GetStreamableManager();
	const FStreamableDelegate Delegate = FStreamableDelegate::CreateUObject(this, &ThisClass::OnAssetsLoaded);
	
	TArray<FSoftObjectPath> ObjectsToLoadArr;

	Explosion.IsValid() ? ObjectsToLoadArr.Emplace(Explosion.ToSoftObjectPath()) : ObjectsToLoadArr.Emplace(Explosion_Old.ToSoftObjectPath());
	ObjectsToLoadArr.Emplace(ExplosionSound.ToSoftObjectPath());

	Manager.RequestAsyncLoad(ObjectsToLoadArr, Delegate);
}

void ATDS_Boss_HomingActor::OnAssetsLoaded()
{
	if (Explosion.IsValid())
	{
		if (UNiagaraSystem* ExplosionNS = Explosion.Get())
		{
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), ExplosionNS, GetActorLocation(), FQuat::Identity.Rotator());
		}
	}
	else
	{
		if (UParticleSystem* ExplosionPS = Explosion_Old.Get())
		{
			const FRotator Rotation = GetActorLocation().GetSafeNormal().Rotation();
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ExplosionPS, GetActorLocation(), Rotation);
		}
	}

	if (ExplosionSound.IsValid())
	{
		if (USoundCue* ExplosionSC = ExplosionSound.Get())
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ExplosionSC, GetActorLocation());
		}
	}

	if (HitDecalComp)
	{
		HitDecalComp -> SetHiddenInGame(true);
	}
	
	SetLifeSpan(5.f);
}

void ATDS_Boss_HomingActor::OnSpawnDecal()
{
	if (HitDecal.IsValid())
	{
		if (UMaterialInstance* HitDecalMI = HitDecal.Get())
		{
			if (HitDecalComp)
			{
				UE_LOG(LogTemp, Warning, TEXT("Hit Decal"));
				HitDecalComp -> SetDecalMaterial(HitDecalMI);
			}
		}
	}
}

void ATDS_Boss_HomingActor::SetRadius(const float& Radius)
{
	if (HitDecalComp)
	{
		UE_LOG(LogTemp, Display, TEXT("Setting radius"));
		HitDecalComp -> DecalSize = FVector(Radius * 2);
		HitDecalComp -> SetHiddenInGame(false);
	}
}