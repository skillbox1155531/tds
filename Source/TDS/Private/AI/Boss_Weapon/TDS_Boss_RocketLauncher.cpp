﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Boss_Weapon/TDS_Boss_RocketLauncher.h"

#include "Boss_Weapon/TDS_Boss_HomingActor.h"
#include "Boss_Weapon/TDS_Boss_Rocket.h"
#include "Components/ArrowComponent.h"
#include "Engine/StaticMeshActor.h"
#include "EnvironmentQuery/EnvQueryInstanceBlueprintWrapper.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ATDS_Boss_RocketLauncher::ATDS_Boss_RocketLauncher()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	if (!MeshComp)
	{
		MeshComp = CreateDefaultSubobject<UStaticMeshComponent>("MeshComp");
		RootComponent = MeshComp;
	}

	if (!SpawnLocation)
	{
		SpawnLocation = CreateDefaultSubobject<UArrowComponent>("SpawnLocation");
		SpawnLocation -> SetupAttachment(RootComponent);
	}
	
	RocketsAmount = 5;
	SpawnedRocketsAmount = 0;
	SpawnRocketsDelay = 1.f;
}

// Called when the game starts or when spawned
void ATDS_Boss_RocketLauncher::BeginPlay()
{
	Super::BeginPlay();
}

void ATDS_Boss_RocketLauncher::SetSpawnRocketsTimer(AActor* Target)
{
	GetWorld() -> GetTimerManager().SetTimer(SpawnRockets_TimerHandle, this, &ThisClass::RunEQS, SpawnRocketsDelay, true, 0.f);
}

void ATDS_Boss_RocketLauncher::RunEQS()
{
	if (ensureMsgf(RocketTargetQuery, TEXT("ATDS_GameMode::SpawnEnemies: SpawnBotQuery is NULL")))
	{
		if (UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(this, RocketTargetQuery, this, EEnvQueryRunMode::RandomBest25Pct, nullptr))
		{
			QueryInstance -> GetOnQueryFinishedEvent().AddDynamic(this, &ThisClass::OnRocketTargetQueryCompleted);
		}
	}
}

void ATDS_Boss_RocketLauncher::OnRocketTargetQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance,
	EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		return;
	}

	TArray<FVector> LocationsArr;
	QueryInstance -> GetQueryResultsAsLocations(LocationsArr);

	if (LocationsArr.IsEmpty())
	{
		return;
	}

	if (ATDS_Boss_HomingActor* Target = GetWorld() -> SpawnActor<ATDS_Boss_HomingActor>(HomingTarget, LocationsArr[0], FQuat::Identity.Rotator()))
	{
		UE_LOG(LogTemp, Display, TEXT("RocketLauncher: EmptyActor Spawned"));
		SpawnRocket(Target);
	}
}

void ATDS_Boss_RocketLauncher::SpawnRocket(ATDS_Boss_HomingActor* Target)
{
	if (ensure(RocketClass))
	{
		FVector StartLocation = SpawnLocation -> GetComponentLocation();
		const FVector EndLocation = StartLocation + FMath::VRandCone(SpawnLocation -> GetForwardVector() * 3000.f, 20.f * PI / 180.f);
		const FVector ShotDirection = EndLocation - StartLocation;
		const FRotator ShotRotation = ShotDirection.Rotation();

		//DrawDebugCone(GetWorld(), StartLocation, ShotDirection, 2000.f, 10.f * PI / 180.f, 10.f * PI / 180.f, 32, FColor::Emerald, false, 5.f, (uint8)'\000', 1.0f);

		FActorSpawnParameters SpawnParameters;
		SpawnParameters.Owner = this;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
			
		if (ATDS_Boss_Rocket* Rocket = GetWorld() -> SpawnActor<ATDS_Boss_Rocket>(RocketClass, StartLocation, ShotRotation, SpawnParameters))
		{
			Rocket -> InitRocket(Target);
			SpawnedRocketsAmount++;

			if (SpawnedRocketsAmount == RocketsAmount)
			{
				SpawnedRocketsAmount = 0;
				GetWorld() -> GetTimerManager().ClearTimer(SpawnRockets_TimerHandle);
			}
		}
	}
}
