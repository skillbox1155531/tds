﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_EnemyCharacter.h"
#include "AIController.h"
#include "TDS_AmmoPickup.h"
#include "TDS_EffectsHandlerComponent.h"
#include "TDS_EnemyCombatData.h"
#include "TDS_GameState.h"
#include "TDS_Player.h"
#include "TDS_PlayerHealthComponent.h"
#include "TDS_StringLibrary.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/BoxComponent.h"
#include "Components/CapsuleComponent.h"
#include "EnvironmentQuery/EnvQueryInstanceBlueprintWrapper.h"
#include "EnvironmentQuery/EnvQueryManager.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Perception/AISense_Damage.h"

// Sets default values
ATDS_EnemyCharacter::ATDS_EnemyCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetupComponents();

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	HeavyAttackChance = 0.2f;
	AttackOffset = 100.f;
	AmmoDropChance = 1.f;
	Tags.Emplace(FTagLibrary::DamageableTag);
}

void ATDS_EnemyCharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (GetMesh() && GetMesh() -> GetAnimInstance())
	{
		if (UAnimInstance* AnimInstance = GetMesh() -> GetAnimInstance())
		{
			AnimInstance -> OnMontageEnded.AddDynamic(this, &ThisClass::StopAttackTarget_Implementation);
			AnimInstance -> OnPlayMontageNotifyBegin.AddDynamic(this, &ThisClass::NotifyBegin);
			AnimInstance -> OnPlayMontageNotifyEnd.AddDynamic(this, &ThisClass::NotifyEnd);
		}
	}
}

bool ATDS_EnemyCharacter::GetIsAliveInt_Implementation()
{
	return HealthComponent -> GetIsAlive();
}

void ATDS_EnemyCharacter::SetActorMovementStateInt_Implementation(const EMovementState& State)
{
	SetMovementState(State);
}

void ATDS_EnemyCharacter::StartAttackTarget_Implementation(AActor* TargetActor)
{
	if (AAIController* AIController = Cast<AAIController>(GetController()))
	{
		AIController -> GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::CanAttackKey, false);
	}
	
	if (UKismetMathLibrary::RandomBoolWithWeight(HeavyAttackChance))
	{
		// UE_LOG(LogTemp, Warning, TEXT("ATDS_EnemyCharacter::StartAttackTarget: Rolled Heavy Attack"));
		
		if (ensure(HeavyAttack))
		{
			CurrentAttack = HeavyAttack;
			// UE_LOG(LogTemp, Warning, TEXT("ATDS_EnemyCharacter::StartAttackTarget: Playing Heavy Attack Anim"));
			GetCharacterMovement() -> StopActiveMovement();
			GetCharacterMovement() -> MovementMode = MOVE_None;

			if (ensure(HeavyAttack -> AttackMontage))
			{
				PlayAnimMontage(HeavyAttack -> AttackMontage);
				return;
			}
		}

		UE_LOG(LogTemp, Error, TEXT("ATDS_EnemyCharacter::StartAttackTarget: Cant Play Heavy Attack Anim, it's NULL"));
	}

	// UE_LOG(LogTemp, Warning, TEXT("ATDS_EnemyCharacter::StartAttackTarget: Rolled Light Attack"));
	
	if (ensure(LightAttack))
	{
		CurrentAttack = LightAttack;
		// UE_LOG(LogTemp, Warning, TEXT("ATDS_EnemyCharacter::StartAttackTarget: Playing Light Attack Anim"));
		GetCharacterMovement() -> StopActiveMovement();

		if (ensure(LightAttack -> AttackMontage))
		{
			PlayAnimMontage(LightAttack -> AttackMontage);
			return;
		}
	}

	UE_LOG(LogTemp, Error, TEXT("ATDS_EnemyCharacter::StartAttackTarget: Cant Play Light Attack Anim, it's NULL"));
}

void ATDS_EnemyCharacter::InterruptAttackTarget_Implementation()
{
	if (CurrentAttack.IsValid())
	{
		StopAnimMontage(CurrentAttack.Get() -> AttackMontage);
	}
}

void ATDS_EnemyCharacter::NotifyBegin(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
	if (NotifyName == FAnimNotifyLibrary::AttackNotify)
	{
		// UE_LOG(LogTemp, Warning, TEXT("ATDS_EnemyCharacter::NotifyBegin: Attack Trace"));

		DrawDebugBox(GetWorld(), GetActorLocation() + (GetActorForwardVector() * AttackOffset), FVector(25.f), FColor::Red, false, 3.f);

		TArray<FHitResult> HitResults;
		GetWorld() -> SweepMultiByChannel(HitResults, GetActorLocation(), GetActorLocation() + (GetActorForwardVector() * AttackOffset), FQuat::Identity, ECC_Visibility,
			FCollisionShape::MakeBox(FVector(25.f)));

		if (HitResults.IsEmpty())
		{
			return;
		}

		for (FHitResult HitResult : HitResults)
		{
			if (HitResult.GetActor() && HitResult.GetActor() -> ActorHasTag(FTagLibrary::PlayerTag))
			{
				// UE_LOG(LogTemp, Warning, TEXT("ATDS_EnemyCharacter::NotifyBegin: Dealing dmg to %s"), *GetNameSafe(HitResult.GetActor()));
				UGameplayStatics::ApplyDamage(HitResult.GetActor(), CurrentAttack.Get() -> AttackDamage, GetController(), this, nullptr);
			}
		}
	}
}

void ATDS_EnemyCharacter::NotifyEnd(FName NotifyName, const FBranchingPointNotifyPayload& BranchingPointPayload)
{
}

void ATDS_EnemyCharacter::StopAttackTarget_Implementation(UAnimMontage* Montage, bool bInterrupted)
{
	if (!CurrentAttack.IsValid())
	{
		return;
	}
	
	if (Montage == CurrentAttack.Get() -> AttackMontage)
	{
		UE_LOG(LogTemp, Warning, TEXT("ATDS_EnemyCharacter::StopAttackTarget: Attack was stopped"));
		
		CurrentAttack.Reset();
		
		if (AAIController* AIController = Cast<AAIController>(GetController()))
		{
			AIController -> GetBlackboardComponent() -> SetValueAsBool(UBlackboardKeyLibrary::CanAttackKey, true);
		}

		if (GetCharacterMovement() -> MovementMode == MOVE_None)
		{
			GetCharacterMovement() -> MovementMode = MOVE_Walking;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("ATDS_EnemyCharacter::StopAttackTarget: Montage isn't A weak ptr"));
	}
}

// Called when the game starts or when spawned
void ATDS_EnemyCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

void ATDS_EnemyCharacter::SetupComponents()
{
	if (!HealthComponent)
	{
		HealthComponent = CreateDefaultSubobject<UTDS_PlayerHealthComponent>(TEXT("Player Health Compomnent"));

		if (HealthComponent)
		{
			AddOwnedComponent(HealthComponent);
		}
	}
}

float ATDS_EnemyCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent,
                                      class AController* EventInstigator, AActor* DamageCauser)
{
	if (HealthComponent)
	{
		const float Value = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
		HealthComponent -> DecreaseHealth(Value);

		UAISense_Damage::ReportDamageEvent(GetWorld(), this, DamageCauser, DamageAmount, GetActorLocation(),GetActorLocation());

		if (!HealthComponent -> GetIsAlive())
		{
			DeathLogic(DamageCauser);
		}
	}

	return 0.f;
}

void ATDS_EnemyCharacter::DeathLogic(AActor* DamageCauser)
{
	if (EffectsHandlerComponent)
	{
		EffectsHandlerComponent -> InterruptAllEffects();
	}

	GetCapsuleComponent() -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
	HitBoxComponent -> SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetMesh() -> SetAllBodiesSimulatePhysics(true);
	GetMesh() -> SetCollisionProfileName(TEXT("Ragdoll"));
	GetCharacterMovement() -> DisableMovement();

	if (ATDS_GameState* GS = Cast<ATDS_GameState>(UGameplayStatics::GetGameState(this)))
	{
		GS -> OnActorKilled(this, DamageCauser);
	}
	
	if (DamageCauser && DamageCauser -> ActorHasTag(FTagLibrary::PlayerTag))
	{
		SpawnAmmoDrop(ITDS_Player::Execute_GetOwnedAmmoTypes(DamageCauser));
	}


	FTimerHandle Destroy_TimerHandle;
	GetWorld() -> GetTimerManager().SetTimer(Destroy_TimerHandle, [this] { Destroy(); }, 3.f, false);
}

void ATDS_EnemyCharacter::SpawnAmmoDrop(const TArray<EWeaponAmmoType>& AmmoTypes)
{
	if (AmmoTypes.IsEmpty() || !UKismetMathLibrary::RandomBoolWithWeight(AmmoDropChance))
	{
		return;
	}

	const EWeaponAmmoType AmmoType = AmmoTypes[FMath::RandRange(0, AmmoTypes.Num() - 1)];

	for (int i = 0; i < AmmoDropsArr.Num(); ++i)
	{
		FPickupInfo& PickupInfo = AmmoDropsArr[i];
			
		if (FAmmoInfo* AmmoInfo = PickupInfo.GetInfo().GetRow<FAmmoInfo>(PickupInfo.GetInfo().RowName.ToString()))
		{
			if (AmmoInfo -> GetAmmoType() == AmmoType)
			{
				if (ensureMsgf(AmmoSpawnQuery, TEXT("ATDS_GameMode::SpawnEnemies: AmmoSpawnQuery is NULL")))
				{
					if (UEnvQueryInstanceBlueprintWrapper* QueryInstance = UEnvQueryManager::RunEQSQuery(this, AmmoSpawnQuery, this, EEnvQueryRunMode::RandomBest25Pct, nullptr))
					{
						AmmoIndex = i;
						QueryInstance -> GetOnQueryFinishedEvent().AddDynamic(this, &ThisClass::OnAmmoSpawnQueryCompleted);
					}
				}
			}
		}	
	}
}

void ATDS_EnemyCharacter::OnAmmoSpawnQueryCompleted(UEnvQueryInstanceBlueprintWrapper* QueryInstance,
	EEnvQueryStatus::Type QueryStatus)
{
	if (QueryStatus != EEnvQueryStatus::Success)
	{
		return;
	}

	TArray<FVector> LocationsArr;

	QueryInstance -> GetQueryResultsAsLocations(LocationsArr);
	
	if (!LocationsArr.IsEmpty())
	{
		if (AmmoDropsArr.IsValidIndex(AmmoIndex))
		{
			const FPickupInfo& PickupInfo = AmmoDropsArr[AmmoIndex];

			if (FAmmoInfo* AmmoInfo = PickupInfo.GetInfo().GetRow<FAmmoInfo>(PickupInfo.GetInfo().RowName.ToString()))
			{
				const FVector Location = LocationsArr[0];
				const FRotator Rotation = FRotator(0.f, FMath::RandRange(0.f, 360.f), 0.f);
				
				FActorSpawnParameters SpawnParameters;
				SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
						
				if (ATDS_AmmoPickup* Pickup = GetWorld() -> SpawnActor<ATDS_AmmoPickup>(Location, Rotation, SpawnParameters))
				{
					Pickup -> InitComponents(AmmoInfo, PickupInfo.GetPickupMesh(), PickupInfo.GetPickupDecal());
				}
			}
		}
	}
}