﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapons/TDS_WeaponBase.h"
#include "NiagaraComponent.h"
#include "TDS_ProjectileBase.h"
#include "TDS_ProjectilePoolComponent.h"
#include "TDS_StringLibrary.h"
#include "TDS_WeaponData.h"
#include "Components/AudioComponent.h"
#include "Engine/StaticMeshActor.h"

ATDS_WeaponBase::ATDS_WeaponBase()
{
	SetupComponents();
	SetDefaultWeaponVariables();
}

void ATDS_WeaponBase::SetupComponents()
{
	if (!Root)
	{
		Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
		RootComponent = Root;
	}

	if (!StaticMeshComponent)
	{
		StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");

		if (StaticMeshComponent)
		{
			StaticMeshComponent -> SetGenerateOverlapEvents(false);
			StaticMeshComponent -> SetCollisionProfileName(FCollisionProfileLibrary::NoCollisionProfile);
			StaticMeshComponent -> SetupAttachment(RootComponent);
		}
	}

	if (!MuzzleFlashComponent)
	{
		MuzzleFlashComponent = CreateDefaultSubobject<UNiagaraComponent>("Muzzle Flash");

		if (MuzzleFlashComponent)
		{
			MuzzleFlashComponent -> SetAutoActivate(false);
			MuzzleFlashComponent -> SetAutoDestroy(false);
			MuzzleFlashComponent -> SetupAttachment(RootComponent);
		}
	}

	if (!AudioComponent)
	{
		AudioComponent = CreateDefaultSubobject<UAudioComponent>("Audio");

		if (AudioComponent)
		{
			AudioComponent -> SetAutoActivate(false);
			AudioComponent -> SetupAttachment(RootComponent);
		}
	}

	if (!ProjectilePoolComponent)
	{
		ProjectilePoolComponent = CreateDefaultSubobject<UTDS_ProjectilePoolComponent>(TEXT("Projectile Pool Component"));

		if (ProjectilePoolComponent)
		{
			AddOwnedComponent(ProjectilePoolComponent);
		}
	}
}

void ATDS_WeaponBase::SetDefaultWeaponVariables()
{
	bIsReadyToFire = true;
	bIsReloading = false;
	ReloadTime = 0.f;
	CurrentDispersion = 0.f;
	ShellMesh.Reset();
	ClipMesh.Reset();
	FireMontage.Reset();
	ReloadMontage.Reset();
	ProjectileHitFXDic.Empty();
}

void ATDS_WeaponBase::BeginPlay()
{
	Super::BeginPlay();
}

void ATDS_WeaponBase::InitWeapon(const FWeaponInfo& WeaponInfo, UTDS_WeaponData* WeaponData)
{
	if (ProjectilePoolComponent && StaticMeshComponent)
	{
		ProjectilePoolComponent -> SpawnProjectilePool(WeaponInfo, WeaponData, StaticMeshComponent -> GetSocketLocation(FSocketLibrary::WeaponProjectileSpawn));
		SetWeaponParameters(WeaponInfo, WeaponData);
	}
}

void ATDS_WeaponBase::ResetWeapon()
{
	if (GetWorld() && GetWorld() -> GetTimerManager().TimerExists(FireTimerHandle))
	{
		GetWorld() -> GetTimerManager().ClearTimer(FireTimerHandle);
	}

	if (GetWorld() && GetWorld() -> GetTimerManager().TimerExists(ReloadTimerHandle))
	{
		GetWorld() -> GetTimerManager().ClearTimer(ReloadTimerHandle);
	}

	if (GetWorld() && GetWorld() -> GetTimerManager().TimerExists(DispersionFallTimerHandle))
	{
		GetWorld() -> GetTimerManager().ClearTimer(DispersionFallTimerHandle);
	}
	
	if (MuzzleFlashComponent && MuzzleFlashComponent -> GetAsset())
	{
		MuzzleFlashComponent -> Deactivate();
		MuzzleFlashComponent -> SetAsset(nullptr);
	}

	if (AudioComponent)
	{
		AudioComponent -> SetSound(nullptr);
	}

	if (ProjectilePoolComponent)
	{
		ProjectilePoolComponent -> ResetProjectilePool();
	}

	SetDefaultWeaponVariables();
}

void ATDS_WeaponBase::SetWeaponParameters(const FWeaponInfo& WeaponInfo, const UTDS_WeaponData* WeaponData)
{
	ID = WeaponInfo.GetID();
	WeaponParameters = WeaponInfo.GetWeaponParameters();
	WeaponDispersionParameters = WeaponInfo.GetWeaponDispersionParameters();

	if (WeaponData)
	{
		InitWeaponData(WeaponData);
	}
} 

void ATDS_WeaponBase::InitWeaponData(const UTDS_WeaponData* WeaponData)
{
	if (StaticMeshComponent && WeaponData -> WeaponStaticMesh)
	{
		StaticMeshComponent -> SetStaticMesh(WeaponData -> WeaponStaticMesh);
	}

	if (MuzzleFlashComponent && WeaponData -> WeaponMuzzleFlashFX)
	{
		if (StaticMeshComponent && StaticMeshComponent -> GetStaticMesh())
		{
			MuzzleFlashComponent -> SetWorldLocation(StaticMeshComponent -> GetSocketLocation(FSocketLibrary::WeaponProjectileSpawn));
		}
		
		MuzzleFlashComponent -> SetAsset(WeaponData -> WeaponMuzzleFlashFX);
	}

	if(AudioComponent && WeaponData -> WeaponFireSoundFX)
	{
		AudioComponent -> SetSound(WeaponData -> WeaponFireSoundFX);
	}

	if (WeaponData -> WeaponShellMeshFX)
	{
		ShellMesh = WeaponData -> WeaponShellMeshFX;
	}

	if (WeaponData -> WeaponClipMeshFX)
	{
		ClipMesh = WeaponData -> WeaponClipMeshFX;
	}

	if (WeaponData -> WeaponFireAnim)
	{
		FireMontage = WeaponData -> WeaponFireAnim;
	}

	if (WeaponData -> WeaponReloadAnim)
	{
		ReloadMontage = WeaponData -> WeaponReloadAnim;
		ReloadTime = ReloadMontage.Get() -> CalculateSequenceLength();
	}

	if (!WeaponData -> ProjectileHitFXDic.IsEmpty())
	{
		ProjectileHitFXDic.Append(WeaponData -> ProjectileHitFXDic);
	}
}

void ATDS_WeaponBase::HandleStartFire(const bool& bFire)
{
	if (bFire)
	{
		if (WeaponParameters.GetWeaponFireType() == EWeaponFireType::WFT_Auto)
		{
			SetAutoFireTimer();
		}
		else if(WeaponParameters.GetWeaponFireType() == EWeaponFireType::WFT_Single)
		{
			SetSingleFireTimer();
		}
		else if (WeaponParameters.GetWeaponFireType() == EWeaponFireType::WFT_SingleRound)
		{
			SetSingleRoundFireTimer();
		}
	}
	else
	{
		if (WeaponParameters.GetWeaponFireType() == EWeaponFireType::WFT_Auto)
		{
			HandleStopFire();
		}
	}
}

void ATDS_WeaponBase::HandleStopFire()
{
	if (GetWorld() && GetWorld() -> GetTimerManager().TimerExists(FireTimerHandle))
	{
		GetWorld() -> GetTimerManager().ClearTimer(FireTimerHandle);
	}

	if (FireMontage.IsValid())
	{
		OnStopFireWeapon.Broadcast(FireMontage);
	}
	
	SetDispersionReductionTimer();
	
	if (MuzzleFlashComponent && MuzzleFlashComponent -> GetAsset())
	{
		MuzzleFlashComponent -> Deactivate();
	}
}

void ATDS_WeaponBase::SetAutoFireTimer()
{
	if (IsValid(GetWorld()))
	{
		if (!GetWorld() -> GetTimerManager().TimerExists(FireTimerHandle))
		{
			ExecuteFire();
			GetWorld() -> GetTimerManager().SetTimer(FireTimerHandle, this, &ThisClass::ExecuteFire, WeaponParameters.GetRateOfFire(), true);
		}
	}
}

void ATDS_WeaponBase::SetSingleFireTimer()
{
	if (IsValid(GetWorld()))
	{
		if (!GetWorld() -> GetTimerManager().TimerExists(FireTimerHandle))
		{
			if (bIsReadyToFire)
			{
				ExecuteFire();
				bIsReadyToFire = false;

				GetWorld() -> GetTimerManager().SetTimer(FireTimerHandle, [&] { bIsReadyToFire = true; }, WeaponParameters.GetRateOfFire(), false);
			}
		}
	}
}

void ATDS_WeaponBase::SetSingleRoundFireTimer()
{
	if (IsValid(GetWorld()))
	{
		if (!GetWorld() -> GetTimerManager().TimerExists(FireTimerHandle))
		{
			ExecuteFire();
			bIsReadyToFire = false;

			GetWorld() -> GetTimerManager().SetTimer(FireTimerHandle, [&] { bIsReadyToFire = true; }, ReloadTime + 0.2f, false);
		}
	}
}

void ATDS_WeaponBase::ExecuteFire()
{
	if (GetCanFire())
	{
		if (ProjectilePoolComponent)
		{
			OnStartFireWeapon.Broadcast(FireMontage);
			PlayFX();
			LaunchStaticMesh(ShellMesh, FSocketLibrary::WeaponEjectableMeshSpawn, FCollisionProfileLibrary::EjectableMeshProfile);
			WeaponParameters.SetCurrentRoundsInClip(WeaponParameters.GetCurrentRoundsInClip() - 1);
			OnUpdateWeaponInfo.Broadcast(ID, WeaponParameters);
			
			for (int i = 0; i < WeaponParameters.GetRoundsPerShot(); ++i)
			{
				TWeakObjectPtr<ATDS_ProjectileBase> Projectile = ProjectilePoolComponent -> GetProjectileFromPool();
				
				if (Projectile.IsValid())
				{
					Projectile.Get() -> OnGetProjectileHitFX.AddUObject(this, &ThisClass::SetProjectileHitFX);
					
					if (LaunchProjectile(Projectile))
					{
						AddDispersion();
					}
				}
			}
				
			if (WeaponParameters.GetCurrentRoundsInClip() == 0)
			{
				HandleReload();
			}
		}
	}
}

bool ATDS_WeaponBase::LaunchProjectile(TWeakObjectPtr<ATDS_ProjectileBase> Projectile)
{
	if (StaticMeshComponent)
	{
		const FVector StartLocation = StaticMeshComponent -> GetSocketLocation(FSocketLibrary::WeaponProjectileSpawn);
		const FVector EndLocation = StartLocation + FMath::VRandCone(GetActorForwardVector() * WeaponParameters.GetMaxFireDistance(), CurrentDispersion * PI / 180.f);
		const FVector ShotDirection = EndLocation - StartLocation;
		//DrawDebugCone(GetWorld(), StartLocation, ShotDirection, 2000.f, CurrentDispersion * PI / 180.f, CurrentDispersion * PI / 180.f, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		
		Projectile.Get() -> SetActorLocation(StartLocation);
		Projectile.Get() -> Launch(ShotDirection.GetSafeNormal());

		return true;
	}

	return false;
}

void ATDS_WeaponBase::HandleReload()
{
	if (WeaponParameters.GetCurrentRoundsInClip() != WeaponParameters.GetMaxRoundsInClip() && WeaponParameters.GetCurrentRoundsInReserve() > 0)
	{
		bIsReloading = true;
		HandleStopFire();
		
		if (GetWorld())
		{
			if (!GetWorld() -> GetTimerManager().TimerExists(ReloadTimerHandle))
			{
				OnStartReloadWeapon.Broadcast(ReloadMontage);
				
				LaunchStaticMesh(ClipMesh, FSocketLibrary::WeaponEjectableMeshSpawn, FCollisionProfileLibrary::EjectableMeshProfile);
				GetWorld() -> GetTimerManager().SetTimer(ReloadTimerHandle, this, &ThisClass::ExecuteReload, ReloadTime, false);
			}
		}
	}
}

void ATDS_WeaponBase::AddAmmoToClip(const int32& AmmoAmount)
{
	WeaponParameters.SetCurrentRoundsInClip(AmmoAmount);
}

void ATDS_WeaponBase::AddAmmoToReserve(const int32& AmmoAmount)
{
	WeaponParameters.SetCurrentRoundsInReserve(AmmoAmount);
}



void ATDS_WeaponBase::ExecuteReload()
{
	if (GetWorld() && GetWorld() -> GetTimerManager().TimerExists(ReloadTimerHandle))
	{
		GetWorld() -> GetTimerManager().ClearTimer(ReloadTimerHandle);
	}

	if (ReloadMontage.IsValid())
	{
		OnStopReloadWeapon.Broadcast(ReloadMontage);
	}

	const int32 NeededAmount = WeaponParameters.GetMaxRoundsInClip() - WeaponParameters.GetCurrentRoundsInClip();
	int32 Amount = WeaponParameters.GetCurrentRoundsInReserve() - NeededAmount;
	
	Amount < 0 ? Amount = WeaponParameters.GetCurrentRoundsInReserve() : Amount = NeededAmount;
	
	WeaponParameters.SetCurrentRoundsInReserve(WeaponParameters.GetCurrentRoundsInReserve() - Amount);
	WeaponParameters.SetCurrentRoundsInClip(WeaponParameters.GetCurrentRoundsInClip() + Amount);

	OnUpdateWeaponInfo.Broadcast(ID, WeaponParameters);
	
	bIsReloading = false;
	bIsReadyToFire = true;
}

void ATDS_WeaponBase::AddDispersion()
{
	if(IsValid(GetWorld()))
	{
		if (GetWorld() -> GetTimerManager().TimerExists(DispersionFallTimerHandle))
		{
			GetWorld() -> GetTimerManager().ClearTimer(DispersionFallTimerHandle);
		}
			
		CurrentDispersion = FMath::Min(CurrentDispersion + WeaponDispersionParameters.GetDispersionGrowCoefficient(), WeaponDispersionParameters.GetMaxDispersion());
	}
}

void ATDS_WeaponBase::SetDispersionReductionTimer()
{
	if (IsValid(GetWorld()))
	{
		GetWorld() -> GetTimerManager().SetTimer(DispersionFallTimerHandle, this, &ATDS_WeaponBase::ReduceDispersion, GetWorld() -> GetDeltaSeconds(), true);
	}
}

void ATDS_WeaponBase::PlayFX()
{
	if (AudioComponent && AudioComponent -> GetSound())
	{
		AudioComponent -> Activate(true);
	}

	if (MuzzleFlashComponent && MuzzleFlashComponent -> GetAsset())
	{
		MuzzleFlashComponent -> Activate(true);
	}
}

void ATDS_WeaponBase::LaunchStaticMesh(const TWeakObjectPtr<UStaticMesh>& StaticMesh, const FName& Socket, const FName& CollisionProfile)
{
	if (StaticMeshComponent)
	{
		if (StaticMesh.IsValid() && GetWorld())
		{
			const FVector Location = StaticMeshComponent -> GetSocketLocation(Socket);
			const FRotator Rotation = GetActorRotation();
			FActorSpawnParameters ActorSpawnParameters;
			ActorSpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			ActorSpawnParameters.Owner = this;

			TWeakObjectPtr<AStaticMeshActor> Shell = GetWorld() -> SpawnActor<AStaticMeshActor>(Location, Rotation, ActorSpawnParameters);

			if (Shell.IsValid())
			{
				Shell.Get() -> GetStaticMeshComponent() -> SetMobility(EComponentMobility::Movable);
				Shell.Get() -> GetStaticMeshComponent() -> SetCollisionProfileName(CollisionProfile);
				Shell.Get() -> GetStaticMeshComponent() -> SetStaticMesh(StaticMesh.Get());
				Shell.Get() -> GetStaticMeshComponent() -> SetSimulatePhysics(true);
				Shell.Get() -> GetStaticMeshComponent() -> SetEnableGravity(true);
				Shell.Get() -> SetLifeSpan(5.f);
				
				FVector Direction = (GetActorRightVector() * FMath::RandRange(50.f, 80.f)) + (GetActorUpVector() * FMath::RandRange(150.f, 200.f));
				
				Shell.Get() -> GetStaticMeshComponent() -> AddImpulse(Direction, NAME_None, true);
			}
		}
	}
}

void ATDS_WeaponBase::SetProjectileHitFX(const TEnumAsByte<EPhysicalSurface>& Surface, FProjectileHitFX& HitFX)
{
	if (!ProjectileHitFXDic.IsEmpty())
	{
		const FProjectileHitFX* FXs = ProjectileHitFXDic.Find(Surface);

		if (FXs)
		{
			HitFX.SetHitSoundFX(FXs -> GetHitSoundFX());
			HitFX.SetHitVisualFX(FXs -> GetHitVisualFX());

			if (!FXs -> GetHitDecalFXArr().IsEmpty())
			{
				const TWeakObjectPtr<UMaterialInstance> Decal = FXs -> GetHitDecalFXArr()[FMath::RandRange(0, FXs -> GetHitDecalFXArr().Num() -1)];

				if (Decal.IsValid())
				{
					HitFX.SetHitDecalFX(Decal);
				}
			}
		}
	}
}

bool ATDS_WeaponBase::GetCanFire() const
{
	return !bIsReloading && WeaponParameters.GetCurrentRoundsInClip() > 0;
}

float ATDS_WeaponBase::GetReloadTime() const
{
	return ReloadTime;
}

void ATDS_WeaponBase::ReduceDispersion()
{
	CurrentDispersion = FMath::Max(CurrentDispersion - WeaponDispersionParameters.GetDispersionFallCoefficient(), WeaponDispersionParameters.GetMinDispersion());

	if (CurrentDispersion == WeaponDispersionParameters.GetMinDispersion())
	{
		if (IsValid(GetWorld()))
		{
			GetWorld() -> GetTimerManager().ClearTimer(DispersionFallTimerHandle);
		}
	}
}

TWeakObjectPtr<UAnimMontage> ATDS_WeaponBase::GetFireMontage() const
{
	if (FireMontage.IsValid())
	{
		return FireMontage;
	}

	return TWeakObjectPtr<UAnimMontage>(nullptr);
}

TWeakObjectPtr<UAnimMontage> ATDS_WeaponBase::GetReloadMontage() const
{
	if (ReloadMontage.IsValid())
	{
		return ReloadMontage;
	}

	return TWeakObjectPtr<UAnimMontage>(nullptr);
}

bool ATDS_WeaponBase::GetIsReloading() const
{
	return bIsReloading;
}

FGuid ATDS_WeaponBase::GetID() const
{
	return ID;
}

TWeakObjectPtr<UTDS_ProjectilePoolComponent> ATDS_WeaponBase::GetProjectilePoolComponent() const
{
	return ProjectilePoolComponent;
}

FWeaponParameters ATDS_WeaponBase::GetWeaponParameters() const
{
	return WeaponParameters;
}
