﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Effect_DamageOverTime.h"

#include "TDS_EffectsHandlerComponent.h"
#include "TDS_StringLibrary.h"
#include "Kismet/GameplayStatics.h"


void UTDS_Effect_DamageOverTime::OnEffectDataLoaded(const FEffectInfo EffectInfo, FPrimaryAssetId Data, AActor* Instigator)
{
	Super::OnEffectDataLoaded(EffectInfo, Data, Instigator);

	TWeakObjectPtr<UTDS_EffectsHandlerComponent> EHC = GetOwningComponent();

	if (EHC.IsValid())
	{
		SpawnEffectVFX(EHC.Get() -> GetOwner(), FSocketLibrary::CharacterBodySocket);
	}
}

void UTDS_Effect_DamageOverTime::ApplyPeriodicEffect(AActor* Instigator)
{
	TWeakObjectPtr<UTDS_EffectsHandlerComponent> EHC = GetOwningComponent();

	if (EHC.IsValid())
	{
		UGameplayStatics::ApplyDamage(EHC.Get() -> GetOwner(), Info.GetEffectValue(), nullptr, Instigator, UDamageType::StaticClass());
	}
}
