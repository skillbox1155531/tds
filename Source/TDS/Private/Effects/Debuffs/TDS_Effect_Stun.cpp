﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_Effect_Stun.h"
#include "TDS_CharacterBase.h"
#include "TDS_EffectsHandlerComponent.h"
#include "TDS_StringLibrary.h"

void UTDS_Effect_Stun::OnEffectDataLoaded(const FEffectInfo EffectInfo, FPrimaryAssetId Data, AActor* Instigator)
{
	Super::OnEffectDataLoaded(EffectInfo, Data, Instigator);

	TWeakObjectPtr<UTDS_EffectsHandlerComponent> EHC = GetOwningComponent();

	if (EHC.IsValid())
	{
		SpawnEffectVFX(EHC -> GetOwner(), FSocketLibrary::CharacterHeadSocket);
	}
}