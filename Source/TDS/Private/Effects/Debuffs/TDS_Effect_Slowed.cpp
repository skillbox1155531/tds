﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_Effect_Slowed.h"
#include "TDS_CharacterBase.h"
#include "TDS_EffectsHandlerComponent.h"
#include "TDS_StringLibrary.h"
#include "GameFramework/CharacterMovementComponent.h"

void UTDS_Effect_Slowed::OnEffectDataLoaded(const FEffectInfo EffectInfo, FPrimaryAssetId Data, AActor* Instigator)
{
	TWeakObjectPtr<UTDS_EffectsHandlerComponent> EHC = GetOwningComponent();

	if (EHC.IsValid() && Cast<ATDS_CharacterBase>(EHC -> GetOwner()))
	{
		TWeakObjectPtr<UCharacterMovementComponent> CharacterMovementComponent = Cast<ATDS_CharacterBase>(EHC -> GetOwner()) -> GetCharacterMovementComponent();

		if (CharacterMovementComponent.IsValid())
		{
			WalkSpeed = CharacterMovementComponent.Get() -> MaxWalkSpeed;
		}

		Super::OnEffectDataLoaded(EffectInfo, Data, Instigator);
		SpawnEffectVFX(EHC -> GetOwner(), FSocketLibrary::CharacterGroundSocket);
	}
}

void UTDS_Effect_Slowed::ApplyInstantEffect(AActor* Instigator)
{
	TWeakObjectPtr<UTDS_EffectsHandlerComponent> EHC = GetOwningComponent();

	if (EHC.IsValid() && Cast<ATDS_CharacterBase>(EHC -> GetOwner()))
	{
		TWeakObjectPtr<UCharacterMovementComponent> CharacterMovementComponent = Cast<ATDS_CharacterBase>(EHC -> GetOwner()) -> GetCharacterMovementComponent();
	
		if (CharacterMovementComponent.IsValid())
		{
			CharacterMovementComponent.Get() -> MaxWalkSpeed = Info.GetEffectValue();
		}
	}
}

void UTDS_Effect_Slowed::StopEffect(AActor* Instigator)
{
	TWeakObjectPtr<UTDS_EffectsHandlerComponent> EHC = GetOwningComponent();

	if (EHC.IsValid() && Cast<ATDS_CharacterBase>(EHC -> GetOwner()))
	{
		TWeakObjectPtr<UCharacterMovementComponent> CharacterMovementComponent = Cast<ATDS_CharacterBase>(EHC -> GetOwner()) -> GetCharacterMovementComponent();
	
		if (CharacterMovementComponent.IsValid())
		{
			CharacterMovementComponent.Get() -> MaxWalkSpeed = WalkSpeed;
		}
	}
	
	Super::StopEffect(Instigator);
}
