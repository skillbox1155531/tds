﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WTDS_DamageVisual.h"

#include "Components/TextBlock.h"

void UWTDS_DamageVisual::NativeConstruct()
{
	Super::NativeConstruct();
	
	if (DamageFadeOutAnim)
	{
		OnDamageFadeOutAnimationFinished.BindDynamic(this, &ThisClass::OnDamageVisualCompletedCallback);
		BindToAnimationFinished(DamageFadeOutAnim, OnDamageFadeOutAnimationFinished);
	}
}

void UWTDS_DamageVisual::OnDamageVisualCompletedCallback()
{
	if (!OnDamageVisualCompleted.ExecuteIfBound())
	{
		UE_LOG(LogTemp, Error, TEXT("UWTDS_DamageVisual::OnDamageVisualCompletedCallback: OnDamageVisualCompleted isn't bound"));
	}
}

void UWTDS_DamageVisual::PlayDamageVisual(const float& Value)
{
	if (TextBlock)
	{
		TextBlock -> SetText(FText::AsNumber(FMath::RoundToInt(Value)));

		if (DamageFadeOutAnim)
		{
			if (!PlayAnimation(DamageFadeOutAnim))
			{
				UE_LOG(LogTemp, Error, TEXT("UWTDS_DamageVisual::PlayDamageVisual: Error accured"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Error, TEXT("UWTDS_DamageVisual::PlayDamageVisual: Animation is NULL"));
		}
	}
}

void UWTDS_DamageVisual::SetShieldDamageColor()
{
	if (TextBlock)
	{
		TextBlock -> SetColorAndOpacity(FSlateColor(FColor::Cyan));
	}
}
