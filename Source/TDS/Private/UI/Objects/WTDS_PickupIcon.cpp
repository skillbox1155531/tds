﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "WTDS_PickupIcon.h"
#include "Components/Image.h"

void UWTDS_PickupIcon::NativePreConstruct()
{
	Super::NativePreConstruct();

	// if (Icon && IconTexture)
	// {
	// 	Icon -> SetBrushFromTexture(IconTexture);
	// }
}

void UWTDS_PickupIcon::SetIcon(const TWeakObjectPtr<UTexture2D>& Texture)
{
	if (Texture.IsValid() && Icon)
	{
		Icon -> SetBrushFromTexture(Texture.Get());
	}
}

bool UWTDS_PickupIcon::GetIsIconSet() const
{
	if (Icon)
	{
		return Icon -> GetBrush().GetResourceObject() != nullptr;
	}

	return false;
}
