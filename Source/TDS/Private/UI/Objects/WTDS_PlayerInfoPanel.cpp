﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "WTDS_PlayerInfoPanel.h"

#include "WTDS_ProgressBar.h"
#include "Animation/UMGSequencePlayer.h"
#include "Components/ProgressBar.h"

void UWTDS_PlayerInfoPanel::NativeConstruct()
{
	Super::NativeConstruct();
}

void UWTDS_PlayerInfoPanel::SetHealthBarValue(const float& Value)
{
	if (HealthBar)
	{
		HealthBar -> SetBarValue(Value);
		ModifyHealthBarColor(Value);
	}
}

void UWTDS_PlayerInfoPanel::SetShieldBarValue(const float& Value)
{
	if (ShieldBar)
	{
		ShieldBar -> SetBarValue(Value);
		Value > 0.f ? ShieldBar -> SetVisibility(ESlateVisibility::Visible) : ShieldBar -> SetVisibility(ESlateVisibility::Hidden);
	}
}

void UWTDS_PlayerInfoPanel::HandleModifyHealthBarValue(const float& Value)
{
	HealthBar -> ModifyBarValue(Value);
	ModifyHealthBarColor(Value);
}

void UWTDS_PlayerInfoPanel::HandleModifyShieldBarValue(const float& Value)
{
	ShieldBar -> ModifyBarValue(Value);
}

void UWTDS_PlayerInfoPanel::ModifyHealthBarColor(const float& Value)
{
	if (HealthBar)
	{
		if (Value > MaxPercent)
		{
			HealthBar -> SetWidgetStyle(BarStyleMax);

			if (SequencePlayer.IsValid())
			{
				StopAnimation(LowHealthAlertAnim);
				SequencePlayer.Reset();
			}
		}
		else if (Value > MidPercent)
		{
			HealthBar -> SetWidgetStyle(BarStyleMid);

			if (SequencePlayer.IsValid())
			{
				StopAnimation(LowHealthAlertAnim);
				SequencePlayer.Reset();
			}
		}
		else if (Value <= 0.f)
		{
			if (SequencePlayer.IsValid())
			{
				StopAnimation(LowHealthAlertAnim);
				SequencePlayer.Reset();
			}
		}
		else
		{
			HealthBar -> SetWidgetStyle(BarStyleMin);

			if (!SequencePlayer.IsValid())
			{
				SequencePlayer = PlayAnimation(LowHealthAlertAnim, 0.f, 0, EUMGSequencePlayMode::Forward, 1.f, true);
			}
		}
	}
}
