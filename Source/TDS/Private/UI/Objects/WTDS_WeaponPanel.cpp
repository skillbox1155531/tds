﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WTDS_WeaponPanel.h"
#include "Components/Border.h"
#include "Components/Image.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"

void UWTDS_WeaponPanel::SetBorder(const TWeakObjectPtr<UTexture2D>& Texture)
{
	if (Border && Texture.IsValid())
	{
		Border -> SetBrushFromTexture(Texture.Get());
	}
}

void UWTDS_WeaponPanel::SetWeaponIcon(const TWeakObjectPtr<UTexture2D>& Texture)
{
	if (WeaponIcon && Texture.IsValid())
	{
		WeaponIcon -> SetBrushFromTexture(Texture.Get());
	}
}

void UWTDS_WeaponPanel::SetBorderColor(const bool& bIsEmpty)
{
	if (Border)
	{
		Border -> SetBrushColor(bIsEmpty ? FLinearColor::Red : FLinearColor::White);
	}
}

void UWTDS_WeaponPanel::SetReloadProgressBarValue(const float& Value)
{
	if (ReloadProgressBar)
	{
		ReloadProgressBar -> SetPercent(FMath::Min(ReloadProgressBar -> GetPercent() + Value, 1.f));
	}
}

void UWTDS_WeaponPanel::ResetReloadProgressBarValue()
{
	if (ReloadProgressBar)
	{
		ReloadProgressBar -> SetPercent(0.f);
	}
}

void UWTDS_WeaponPanel::SetIsReloadProgressBarVisible(const bool& Value)
{
	if (ReloadProgressBar)
	{
		Value ? ReloadProgressBar -> SetVisibility(ESlateVisibility::Visible) : ReloadProgressBar -> SetVisibility(ESlateVisibility::Hidden);
	}
}

void UWTDS_WeaponPanel::SetAmmoText(const FText& Text)
{
	if (AmmoText)
	{
		AmmoText -> SetText(Text);
	}
}

void UWTDS_WeaponPanel::SetAmmoText(const FText& Text1, const FText& Text2)
{
	if (AmmoText)
	{
		const FText FormatPattern = FText::FromString("{0} / {1}");
		const FText FormattedText = FText::Format(FormatPattern,Text1, Text2);

		AmmoText -> SetText(FormattedText);
	}
}

void UWTDS_WeaponPanel::SetWeaponInfo(const FWeaponInfo& Info)
{
	WeaponInfo = Info;

	if (WeaponIcon)
	{
		SetBorderColor(WeaponInfo.GetWeaponParameters().GetCurrentRoundsInReserve() == 0 && WeaponInfo.GetWeaponParameters().GetCurrentRoundsInClip() == 0);
	}
}

void UWTDS_WeaponPanel::SetWeaponData(UTDS_WeaponData* Data)
{
	if (Data)
	{
		WeaponData = Data;
	}
}

FWeaponInfo& UWTDS_WeaponPanel::GetWeaponInfo()
{
	return WeaponInfo;
}

UTDS_WeaponData* UWTDS_WeaponPanel::GetWeaponData()
{
	return WeaponData;
}

float UWTDS_WeaponPanel::GetReloadProgressBarValue() const
{
	if (ReloadProgressBar)
	{
		return ReloadProgressBar -> GetPercent();
	}

	return -1;
}
