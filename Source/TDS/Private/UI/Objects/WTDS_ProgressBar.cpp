﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WTDS_ProgressBar.h"

#include "Components/ProgressBar.h"

void UWTDS_ProgressBar::SetBarValue(const float Value)
{
	if (Bar)
	{
		Bar -> SetPercent(Value);
	}
}

void UWTDS_ProgressBar::ModifyBarValue(const float Value)
{
	GetWorld() -> GetTimerManager().SetTimer(BarUpdate_TimerHandle, [this, Value] { UpdateBarValue(Value, .01f); }, .01f, true);
}

void UWTDS_ProgressBar::UpdateBarValue(const float Value, const float Speed)
{
	if (Bar)
	{
		Bar -> SetPercent(FMath::FInterpConstantTo(Bar -> GetPercent(), Value, Speed, InterpSpeed));
		//UE_LOG(LogTemp, Log, TEXT("UWTDS_ProgressBar::ModifyBarValue: Current: %f, Target: %f"), Bar -> GetPercent(), Value);
		if (FMath::IsNearlyEqual(Bar -> GetPercent(), Value))
		{
			Bar -> SetPercent(Value);
			GetWorld() -> GetTimerManager().ClearTimer(BarUpdate_TimerHandle);
		}
	}
}

void UWTDS_ProgressBar::SetWidgetStyle(const FProgressBarStyle NewStyle)
{
	if (Bar)
	{
		Bar -> SetWidgetStyle(NewStyle);
	}
}
