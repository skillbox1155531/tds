﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "WTDS_InventoryPanel.h"
#include "Components/WrapBox.h"
#include "WTDS_WeaponPanel.h"
#include "WTDS_EffectsPanel.h"
#include "Components/VerticalBox.h"
#include "Components/WrapBoxSlot.h"

void UWTDS_InventoryPanel::NativeConstruct()
{
	Super::NativeConstruct();

	if(ensure(SelectedWeaponPanelClass))
	{
		CurrentWeaponPanel = CreateWidget<UWTDS_WeaponPanel>(this, SelectedWeaponPanelClass);

		if (CurrentWeaponPanel)
		{
			InventoryBox -> AddChildToWrapBox(CurrentWeaponPanel);
			WeaponPanelsArr.Emplace(CurrentWeaponPanel);
		}
	}
}

void UWTDS_InventoryPanel::UpdateCurrentWeaponPanel(const FWeaponInfo& Info, UTDS_WeaponData* WeaponData)
{
	if (CurrentWeaponPanel)
	{
		CurrentWeaponPanel -> SetWeaponIcon(WeaponData -> WeaponThumbnail);
		CurrentWeaponPanel -> SetAmmoText(FText::AsNumber(Info.GetWeaponParameters().GetCurrentRoundsInClip()), FText::AsNumber(Info.GetWeaponParameters().GetCurrentRoundsInReserve()));
		CurrentWeaponPanel -> SetWeaponInfo(Info);
		CurrentWeaponPanel -> SetWeaponData(WeaponData);
		CurrentWeaponPanel -> ResetReloadProgressBarValue();
		CurrentWeaponPanel -> SetIsReloadProgressBarVisible(false);
	}
}

void UWTDS_InventoryPanel::UpdateWeaponPanels(const FWeaponInfo& Info, UTDS_WeaponData* WeaponData, const bool& bIsIncrease)
{
	UpdateUnselectedPanels(bIsIncrease ? 0 : WeaponPanelsArr.Num() - 2,  Info, WeaponData, bIsIncrease);
}

void UWTDS_InventoryPanel::UpdateUnselectedPanels(const int32 Index, const FWeaponInfo& Info, UTDS_WeaponData* WeaponData, const bool& bIsIncrease)
{
	if (WeaponPanelsArr.IsValidIndex(Index))
	{
		FWeaponInfo TempInfo = WeaponPanelsArr[Index] -> GetWeaponInfo();
		UTDS_WeaponData* TempData = WeaponPanelsArr[Index] -> GetWeaponData();
		
		if (WeaponPanelsArr[Index])
		{
			WeaponPanelsArr[Index] -> SetWeaponIcon(WeaponData -> WeaponThumbnail);
			WeaponPanelsArr[Index] -> SetAmmoText(FText::AsNumber(Info.GetWeaponParameters().GetCurrentRoundsInReserve()));
			WeaponPanelsArr[Index] -> SetWeaponInfo(Info);
			WeaponPanelsArr[Index] -> SetWeaponData(WeaponData);

			UpdateUnselectedPanels(bIsIncrease ? Index + 1 : Index - 1, TempInfo, TempData, bIsIncrease);
		}
	}
}

TWeakObjectPtr<UWTDS_WeaponPanel> UWTDS_InventoryPanel::AddWeaponPanel()
{
	if (ensure(UnselectedWeaponPanelClass))
	{
		if (UWTDS_WeaponPanel* WeaponPanel = CreateWidget<UWTDS_WeaponPanel>(this, UnselectedWeaponPanelClass))
		{
			WeaponPanelsArr.Insert(WeaponPanel, WeaponPanelsArr.Num() - 1);
			RebuildInventoryBox();

			return WeaponPanel;
		}
	}

	return TWeakObjectPtr<UWTDS_WeaponPanel>(nullptr);
}

void UWTDS_InventoryPanel::SetInfoInUnselectedWeaponPanel(const TWeakObjectPtr<UWTDS_WeaponPanel>& WeaponPanel,
	const FWeaponInfo& Info, UTDS_WeaponData* WeaponData)
{
	if (WeaponPanel.IsValid())
	{
		WeaponPanel -> SetWeaponIcon(WeaponData -> WeaponThumbnail);
		WeaponPanel -> SetAmmoText(FText::AsNumber(Info.GetWeaponParameters().GetCurrentRoundsInReserve()));
		WeaponPanel -> SetWeaponInfo(Info);
		WeaponPanel -> SetWeaponData(WeaponData);
	}
}

void UWTDS_InventoryPanel::RebuildInventoryBox()
{
	if (InventoryBox)
	{
		TArray<UWidget*> WidgetsArr = InventoryBox -> GetAllChildren();

		if (!WidgetsArr.IsEmpty())
		{
			for (auto Widget : WidgetsArr)
			{
				Widget -> RemoveFromParent();
			}

			WidgetsArr.Empty();
		}

		if (!WeaponPanelsArr.IsEmpty())
		{
			for (auto Widget : WeaponPanelsArr)
			{
				TWeakObjectPtr<UWrapBoxSlot> WrapBoxSlot = InventoryBox -> AddChildToWrapBox(Widget);

				if (WrapBoxSlot.IsValid())
				{
					WrapBoxSlot.Get() -> SetHorizontalAlignment(HAlign_Left);
					WrapBoxSlot.Get() -> SetVerticalAlignment(VAlign_Center);
				}
			}
		}
	}
}

void UWTDS_InventoryPanel::UpdateInfoInInventoryBox(const FWeaponInfo& Info, const UTDS_WeaponData* WeaponData)
{
	for (UWTDS_WeaponPanel* WeaponPanel : WeaponPanelsArr)
	{
		if (WeaponPanel && WeaponPanel -> GetWeaponInfo().GetWeaponParameters().GetWeaponAmmoType() == Info.GetWeaponParameters().GetWeaponAmmoType())
		{
			WeaponPanel == CurrentWeaponPanel ?
				WeaponPanel -> SetAmmoText(FText::AsNumber(Info.GetWeaponParameters().GetCurrentRoundsInClip()), FText::AsNumber(Info.GetWeaponParameters().GetCurrentRoundsInReserve())) :
				WeaponPanel -> SetAmmoText(FText::AsNumber(Info.GetWeaponParameters().GetCurrentRoundsInReserve()));

			WeaponPanel -> SetWeaponIcon(WeaponData -> WeaponThumbnail);
			WeaponPanel -> SetBorderColor(Info.GetWeaponParameters().GetCurrentRoundsInReserve() == 0 && Info.GetWeaponParameters().GetCurrentRoundsInClip() == 0);
			WeaponPanel -> SetWeaponInfo(Info);
		}
	}
}

void UWTDS_InventoryPanel::StartCurrentWeaponReloadUI(const float& ReloadTime)
{
	if(CurrentWeaponPanel)
	{
		CurrentWeaponPanel -> SetIsReloadProgressBarVisible(true);
		
		if (GetWorld())
		{
			GetWorld() -> GetTimerManager().SetTimer(ReloadUITimerHandle, [&, ReloadTime]{ ExecuteCurrentWeaponReloadUI(ReloadTime); }, .01f, true);
		}
	}
}

void UWTDS_InventoryPanel::ExecuteCurrentWeaponReloadUI(const float& ReloadTime)
{
	CurrentWeaponPanel -> SetReloadProgressBarValue(.01f/ ReloadTime);

	if (CurrentWeaponPanel -> GetReloadProgressBarValue() >= 1.f)
	{
		CurrentWeaponPanel -> ResetReloadProgressBarValue();
		CurrentWeaponPanel -> SetIsReloadProgressBarVisible(false);

		if (GetWorld())
		{
			GetWorld() -> GetTimerManager().ClearTimer(ReloadUITimerHandle);
		}
	}
}

TWeakObjectPtr<UWTDS_WeaponPanel> UWTDS_InventoryPanel::GetCurrentWeaponPanel() const
{
	return CurrentWeaponPanel;
}
