﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "WTDS_EffectIcon.h"
#include "TDS_StructLibrary.h"
#include "Components/Image.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"

void UWTDS_EffectIcon::NativeConstruct()
{
	Super::NativeConstruct();

	Stacks = 1;
	EffectStacksTextBlock -> SetVisibility(ESlateVisibility::Hidden);
}

void UWTDS_EffectIcon::NativeTick(const FGeometry& MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	//UE_LOG(LogTemp, Log, TEXT("UWTDS_EffectIcon::NativeTick: Alive"));
}

void UWTDS_EffectIcon::EnableEffectIcon(const FEffectInfo& Info, UTexture2D* Texture)
{
	if (EffectTimerTextBlock && EffectImage && EffectStacksTextBlock)
	{
		ID = Info.GetID();
		EffectDuration = Info.GetEffectDuration();
		CurrentEffectTime = EffectDuration;

		if (Texture)
		{
			EffectImage -> SetBrushFromTexture(Texture, true);
		}

		EffectTimerTextBlock -> SetText(FText::AsNumber(Info.GetEffectDuration()));
		
		GetWorld() -> GetTimerManager().SetTimer(EffectBar_TimerHandle, this, &ThisClass::UpdateBar, .01f, true);
		GetWorld() -> GetTimerManager().SetTimer(EffectText_TimerHandle, this, &ThisClass::UpdateText, 1.f, true);
	}
}

void UWTDS_EffectIcon::EnableEffectIcon(const FWeaponEffectInfo& Info, UTexture2D* Texture)
{
	if (EffectTimerTextBlock && EffectImage && EffectStacksTextBlock)
	{
		//UE_LOG(LogTemp, Warning, TEXT("UWTDS_EffectIcon::EnableEffectIcon: Duration %f"), Info.GetEffectDuration());
		
		ID = Info.GetID();
		EffectDuration = Info.GetEffectDuration();
		CurrentEffectTime = EffectDuration;

		if (Texture)
		{
			EffectImage -> SetBrushFromTexture(Texture, true);
		}

		EffectTimerTextBlock -> SetText(FText::AsNumber(Info.GetEffectDuration()));
		
		GetWorld() -> GetTimerManager().SetTimer(EffectBar_TimerHandle, this, &ThisClass::UpdateBar, .01f, true);
		GetWorld() -> GetTimerManager().SetTimer(EffectText_TimerHandle, this, &ThisClass::UpdateText, 1.f, true);
	}
}

void UWTDS_EffectIcon::IncreaseStacks()
{
	if (EffectStacksTextBlock)
	{
		Stacks++;
		EffectStacksTextBlock -> SetText(FText::AsNumber(Stacks));
		EffectStacksTextBlock -> SetVisibility(ESlateVisibility::Visible);
	}
}

void UWTDS_EffectIcon::Restart()
{
	if (EffectTimerTextBlock && EffectProgressBar)
	{
		CurrentEffectTime = EffectDuration;
		EffectTimerTextBlock -> SetText(FText::AsNumber(EffectDuration));
		EffectProgressBar -> SetPercent(0.f);
	}
}

FGuid UWTDS_EffectIcon::GetID() const
{
	return ID;
}

void UWTDS_EffectIcon::UpdateBar()
{
	if (EffectProgressBar)
	{
		EffectProgressBar -> SetPercent(FMath::Min(EffectProgressBar -> GetPercent() + (.01f / EffectDuration), 1.f));
	}
}

void UWTDS_EffectIcon::UpdateText()
{
	if (EffectTimerTextBlock)
	{
		CurrentEffectTime--;
		EffectTimerTextBlock -> SetText(FText::AsNumber(CurrentEffectTime));
	}
}

void UWTDS_EffectIcon::RemoveEffectIcon()
{
	if (GetWorld() -> GetTimerManager().GetTimerRemaining(EffectText_TimerHandle) < KINDA_SMALL_NUMBER)
	{
		UpdateText();
	}
	if (GetWorld() -> GetTimerManager().GetTimerRemaining(EffectBar_TimerHandle) < KINDA_SMALL_NUMBER)
	{
		UpdateBar();
	}
	
	GetWorld() -> GetTimerManager().ClearTimer(EffectText_TimerHandle);
	GetWorld() -> GetTimerManager().ClearTimer(EffectBar_TimerHandle);

	if (GetWorld() -> GetTimerManager().TimerExists(EffectText_TimerHandle))
	{
		UE_LOG(LogTemp, Error, TEXT("UWTDS_EffectIcon::RemoveEffectIcon: EffectText_TimerHandle wasn't cleared"));
	}
	
	if (GetWorld() -> GetTimerManager().TimerExists(EffectText_TimerHandle))
	{
		UE_LOG(LogTemp, Error, TEXT("UWTDS_EffectIcon::RemoveEffectIcon: EffectBar_TimerHandle wasn't cleared"));
	}
	
	UE_LOG(LogTemp, Warning, TEXT("UWTDS_EffectIcon::RemoveEffectIcon"));
}
