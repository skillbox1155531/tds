﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WTDS_RespawnCountDown.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"

void UWTDS_RespawnCountDown::SetTimer_Implementation(const float& Value)
{
	Timer = Value;
	GetWorld() -> GetTimerManager().SetTimer(TimerBar_TimerHandle, [&, Value] { UpdateTimerBar(Value); }, 0.01f, true);
	GetWorld() -> GetTimerManager().SetTimer(TimerText_TimerHandle, [&, Value] { UpdateTimerText(Value); }, 1.f, true);
}

void UWTDS_RespawnCountDown::UpdateTimerBar(const float& Value)
{
	if (TimerProgressBar)
	{
		float CurrentValue = TimerProgressBar -> GetPercent();

		TimerProgressBar -> SetPercent(CurrentValue + 0.01f / Value);
	}
}

void UWTDS_RespawnCountDown::UpdateTimerText(const float& Value)
{
	if (TimerText)
	{
		Timer--;
		TimerText -> SetText(FText::AsNumber(Timer));
	}
}

void UWTDS_RespawnCountDown::ClearTimer()
{
	GetWorld() -> GetTimerManager().ClearTimer(TimerBar_TimerHandle);
	GetWorld() -> GetTimerManager().ClearTimer(TimerText_TimerHandle);
}
