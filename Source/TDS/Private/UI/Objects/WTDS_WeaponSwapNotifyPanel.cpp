﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "WTDS_WeaponSwapNotifyPanel.h"
#include "TDS_StructLibrary.h"
#include "TDS_WeaponData.h"
#include "WTDS_CustomButton.h"
#include "Animation/WidgetAnimation.h"
#include "Components/Image.h"
#include "Components/SizeBox.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"

void UWTDS_WeaponSwapNotifyPanel::NativePreConstruct()
{
	Super::NativePreConstruct();

	if (SwapButton)
	{
		if (SwapButton -> GetTextBlock().IsValid())
		{
			SwapButton -> GetTextBlock().Get() -> SetText(SwapButtonName);
		}

		if (SwapButton -> GetSizeBox().IsValid())
		{
			SwapButton -> GetSizeBox().Get() -> SetWidthOverride(ButtonSize.X);
			SwapButton -> GetSizeBox().Get() -> SetHeightOverride(ButtonSize.Y);
		}

		if (SwapButton -> GetButton().IsValid() && SwapButtonTextureNormal)
		{
			FButtonStyle ButtonStyle = SwapButton -> GetButton().Get() -> GetStyle();
			FSlateBrush SlateBrush;
			SlateBrush.SetResourceObject(SwapButtonTextureNormal);
			ButtonStyle.SetNormal(SlateBrush);
			ButtonStyle.SetPressed(SlateBrush);
			SlateBrush.SetResourceObject(SwapButtonTextureHovered);
			ButtonStyle.SetHovered(SlateBrush);
			SwapButton -> GetButton().Get() -> SetStyle(ButtonStyle);
		}
	}

	if (CancelButton)
	{
		if (CancelButton -> GetTextBlock().IsValid())
		{
			CancelButton -> GetTextBlock().Get() -> SetText(CancelButtonName);
		}
		
		if (CancelButton -> GetSizeBox().IsValid())
		{
			CancelButton -> GetSizeBox().Get() -> SetWidthOverride(ButtonSize.X);
			CancelButton -> GetSizeBox().Get() -> SetHeightOverride(ButtonSize.Y);
		}

		if (CancelButton -> GetButton().IsValid() && SwapButtonTextureNormal)
		{
			FButtonStyle ButtonStyle = CancelButton -> GetButton().Get() -> GetStyle();
			FSlateBrush NormalSlateBrush;
			NormalSlateBrush.SetResourceObject(CancelButtonTextureNormal);
			ButtonStyle.SetNormal(NormalSlateBrush);
			ButtonStyle.SetPressed(NormalSlateBrush);
			NormalSlateBrush.SetResourceObject(CancelButtonTextureHovered);
			ButtonStyle.SetHovered(NormalSlateBrush);
			CancelButton -> GetButton().Get() -> SetStyle(ButtonStyle);
		}
	}
}

void UWTDS_WeaponSwapNotifyPanel::NativeConstruct()
{
	Super::NativeConstruct();

	if (SwapButton)
	{
		SwapButton -> OnUIButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}

	if (CancelButton)
	{
		CancelButton -> OnUIButtonClicked.AddUObject(this, &ThisClass::HandleButtonClicked);
	}

	if (ConfirmedSwapAnimation)
	{
		OnConfirmedSwapAnimationFinished.BindDynamic(this, &ThisClass::ConfirmedSwapFinishedCallback);
		BindToAnimationFinished(ConfirmedSwapAnimation, OnConfirmedSwapAnimationFinished);
	}

	if (CanceledSwapAnimation)
	{
		OnCanceledSwapAnimationFinished.BindDynamic(this, &ThisClass::CanceledSwapFinishedCallback);
		BindToAnimationFinished(CanceledSwapAnimation, OnCanceledSwapAnimationFinished);
	}

	SetVisibility(ESlateVisibility::Hidden);
}

void UWTDS_WeaponSwapNotifyPanel::HandleButtonClicked(const TWeakObjectPtr<UWTDS_CustomButton>& Button)
{
	if (Button.Get() == SwapButton)
	{
		if (ConfirmedSwapAnimation)
		{
			SwapButton -> SetIsEnabled(false);
			CancelButton -> SetIsEnabled(false);
			PlayAnimation(ConfirmedSwapAnimation, 0.f, 1, EUMGSequencePlayMode::Forward, 1.f, true);
		}
	}
	else if(Button.Get() == CancelButton)
	{
		if (CanceledSwapAnimation)
		{
			SwapButton -> SetIsEnabled(false);
			CancelButton -> SetIsEnabled(false);
			PlayAnimation(CanceledSwapAnimation, 0.f, 1, EUMGSequencePlayMode::Forward, 1.f, true);
		}
	}
}

void UWTDS_WeaponSwapNotifyPanel::ConfirmedSwapFinishedCallback()
{
	FWeaponsInfo WeaponsInfo = MakeTuple(WeaponInfo2, WeaponInfo1);

	if (!OnWeaponsInfoSwapCompleted.ExecuteIfBound(WeaponsInfo))
	{
		UE_LOG(LogTemp, Error, TEXT("UWTDS_WeaponExchangeNotifyPanel::AnimationFinished: OnWeaponsInfoSwapCompleted isn't Bound"));
	}
	else
	{
		SetVisibility(ESlateVisibility::Hidden);
	}
}

void UWTDS_WeaponSwapNotifyPanel::CanceledSwapFinishedCallback()
{
	if (!OnWeaponsInfoSwapCanceled.ExecuteIfBound())
	{
		UE_LOG(LogTemp, Error, TEXT("UWTDS_WeaponSwapNotifyPanel::CanceledSwapFinishedCallback: OnWeaponsInfoSwapCanceled isn't Bound"));
	}
	else
	{
		SetVisibility(ESlateVisibility::Hidden);
	}
}

void UWTDS_WeaponSwapNotifyPanel::HandleWeaponSwapNotification(TTuple<FWeaponInfo, UTDS_WeaponData*> Info1,
                                                               TTuple<FWeaponInfo, UTDS_WeaponData*> Info2)
{
	SetIsFocusable(true);
	SwapButton -> SetIsEnabled(true);
	CancelButton -> SetIsEnabled(true);
	SetVisibility(ESlateVisibility::Visible);
	WeaponInfo1 = Info1;
	WeaponInfo2 = Info2;
	
	if (CurrentWeaponThumbnail && Info1.Value -> WeaponThumbnail)
	{
		CurrentWeaponThumbnail -> SetBrushFromTexture(Info1.Value -> WeaponThumbnail);
	}
	
	if (PickupWeaponThumbnail && Info2.Value -> WeaponThumbnail)
	{
		PickupWeaponThumbnail -> SetBrushFromTexture(Info2.Value -> WeaponThumbnail);
	}
}
