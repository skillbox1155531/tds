﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "WTDS_CustomButton.h"
#include "Components/SizeBox.h"
#include "Components/Button.h"
#include "Components/TextBlock.h"

void UWTDS_CustomButton::NativeConstruct()
{
	Super::NativeConstruct();

	if (MainButton)
	{
		MainButton -> OnClicked.AddDynamic(this, &ThisClass::OnButtonClicked);
	}
}

TWeakObjectPtr<USizeBox> UWTDS_CustomButton::GetSizeBox() const
{
	return TWeakObjectPtr<USizeBox>(SizeBox);
}

TWeakObjectPtr<UButton> UWTDS_CustomButton::GetButton() const
{
	return TWeakObjectPtr<UButton>(MainButton);
}

TWeakObjectPtr<UTextBlock> UWTDS_CustomButton::GetTextBlock() const
{
	return TWeakObjectPtr<UTextBlock>(ButtonText);
}

void UWTDS_CustomButton::OnButtonClicked()
{
	OnUIButtonClicked.Broadcast(TWeakObjectPtr<UWTDS_CustomButton>(this));
}
