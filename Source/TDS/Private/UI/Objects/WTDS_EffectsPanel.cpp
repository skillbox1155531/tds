﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "WTDS_EffectsPanel.h"
#include "TDS_StructLibrary.h"
#include "WTDS_EffectIcon.h"
#include "Components/WrapBox.h"

void UWTDS_EffectsPanel::AddEffect(const FEffectInfo& Info, UTexture2D* Texture)
{
	if (ensure(EffectIconClass))
	{
		UWTDS_EffectIcon* Icon = CreateWidget<UWTDS_EffectIcon>(this, EffectIconClass);

		if (Icon && Panel)
		{
			Icon -> EnableEffectIcon(Info, Texture);

			if (!Panel -> AddChild(Icon))
			{
				UE_LOG(LogTemp, Error, TEXT("UWTDS_EffectsPanel::AddEffect: Icon wasn't Added"))
			}
			else
			{
				EffectIconsArr.Emplace(Icon);
			}
		}
	}
}

void UWTDS_EffectsPanel::AddEffect(const FWeaponEffectInfo& Info, UTexture2D* Texture)
{
	if (ensure(EffectIconClass))
	{
		UWTDS_EffectIcon* Icon = CreateWidget<UWTDS_EffectIcon>(this, EffectIconClass);

		if (Icon && Panel)
		{
			Icon -> EnableEffectIcon(Info, Texture);

			if (!Panel -> AddChild(Icon))
			{
				UE_LOG(LogTemp, Error, TEXT("UWTDS_EffectsPanel::AddEffect: Icon wasn't Added"))
			}
			else
			{
				EffectIconsArr.Emplace(Icon);
			}
		}
	}
}

void UWTDS_EffectsPanel::ModifyEffect(const FGuid& ID)
{
	for (UWTDS_EffectIcon* Icon : EffectIconsArr)
	{
		if (Icon && Icon -> GetID() == ID)
		{
			Icon -> IncreaseStacks();
			Icon -> Restart();
		}
	}
}

void UWTDS_EffectsPanel::RestartEffect(const FGuid& ID)
{
	for (UWTDS_EffectIcon* Icon : EffectIconsArr)
	{
		if (Icon && Icon -> GetID() == ID)
		{
			Icon -> Restart();
		}	
	}
}

void UWTDS_EffectsPanel::RemoveEffect(const FGuid& ID)
{
	for (UWTDS_EffectIcon* Icon : EffectIconsArr)
	{
		if (Icon && Icon -> GetID() == ID)
		{
			Icon -> RemoveEffectIcon();
			Icon -> RemoveFromParent();
			EffectIconsArr.Remove(Icon);
			return;
		}	
	}
}

void UWTDS_EffectsPanel::ResetPanel()
{
	for (UWTDS_EffectIcon* Icon : EffectIconsArr)
	{
		if (Icon)
		{
			Icon -> RemoveEffectIcon();
			Icon -> RemoveFromParent();
		}
	}

	EffectIconsArr.Empty();
}
