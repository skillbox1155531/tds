﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "WTDS_PlayerHUD.h"
#include "WTDS_InventoryPanel.h"
#include "WTDS_PlayerInfoPanel.h"
#include "WTDS_WeaponSwapNotifyPanel.h"
#include "WTDS_EffectsPanel.h"

TWeakObjectPtr<UWTDS_InventoryPanel> UWTDS_PlayerHUD::GetInventoryPanel() const
{
	return TWeakObjectPtr<UWTDS_InventoryPanel>(InventoryPanel);
}

TWeakObjectPtr<UWTDS_PlayerInfoPanel> UWTDS_PlayerHUD::GetPlayerInfoPanel() const
{
	return TWeakObjectPtr<UWTDS_PlayerInfoPanel>(PlayerInfoPanel);
}

TWeakObjectPtr<UWTDS_WeaponSwapNotifyPanel> UWTDS_PlayerHUD::GetWeaponSwapNotifyPanel() const
{
	return TWeakObjectPtr<UWTDS_WeaponSwapNotifyPanel>(WeaponSwapNotifyPanel);
}

TWeakObjectPtr<UWTDS_EffectsPanel> UWTDS_PlayerHUD::GetPlayerEffectsPanel() const
{
	return TWeakObjectPtr<UWTDS_EffectsPanel>(PlayerEffectsPanel);
}

TWeakObjectPtr<UWTDS_EffectsPanel> UWTDS_PlayerHUD::GetWeaponEffectsPanel() const
{
	return TWeakObjectPtr<UWTDS_EffectsPanel>(WeaponEffectsPanel);
}