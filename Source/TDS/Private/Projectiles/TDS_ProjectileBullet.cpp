﻿// Fill out your copyright notice in the Description page of Project Settings.

#include "TDS_ProjectileBullet.h"

#include "TDS_BaseEffect.h"
#include "TDS_CharacterBase.h"
#include "TDS_DamageVisualBase.h"
#include "TDS_EffectsHandlerComponent.h"
#include "TDS_StringLibrary.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATDS_ProjectileBullet::ATDS_ProjectileBullet()
{
	ATDS_ProjectileBullet::SetupComponents();
}

void ATDS_ProjectileBullet::SetupComponents()
{
	Super::SetupComponents();
	
	if (BoxComponent)
	{
		BoxComponent -> GetBodyInstance() -> bLockZTranslation = true;
		BoxComponent -> SetEnableGravity(false);
		BoxComponent -> OnComponentHit.AddDynamic(this, &ThisClass::OnHit);
	}
}

void ATDS_ProjectileBullet::Activate(const FVector& ShotDirection)
{
	Super::Activate(ShotDirection);
	
	if (GetWorld())
	{
		GetWorld() -> GetTimerManager().SetTimer(ProjectileTrackerTimerHandle, [&]() -> float { return TrackProjectile(); }, GetWorld() -> GetDeltaSeconds(), true);
	}
}

float ATDS_ProjectileBullet::TrackProjectile()
{
	const float Distance = Super::TrackProjectile();

	if (Distance >= MaxRange)
	{
		Deactivate();
	}

	return 0.f;
}

void ATDS_ProjectileBullet::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	FVector NormalImpulse, const FHitResult& Hit)
{
	//Super::OnHit(HitComponent, OtherActor, OtherComp, NormalImpulse, Hit);

	if (OtherActor && OtherActor -> ActorHasTag(FTagLibrary::DamageableTag))
	{
		//UE_LOG(LogTemp, Display, TEXT("ATDS_ProjectileBullet::OnHit: %s got Hit"), *GetNameSafe(OtherActor));
		
		UGameplayStatics::ApplyPointDamage(OtherActor, Damage, GetActorLocation(), FHitResult(), nullptr, GetOwner() -> GetOwner(), UDamageType::StaticClass());

		if (EffectClass)
		{
			TWeakObjectPtr<UTDS_EffectsHandlerComponent> EffectsHandlerComponent = UTDS_EffectsHandlerComponent::GetEffectsHandlerComponent(OtherActor);

			if (EffectsHandlerComponent.IsValid())
			{
				const FEffectInfo* EffectInfoPtr = &EffectInfo;
				EffectsHandlerComponent -> AddEffect(GetOwner() -> GetOwner(), EffectInfoPtr, EffectClass);
			}
		}
	}
	
	if (Hit.PhysMaterial.IsValid())
	{
		PlayProjectileFX(Hit.PhysMaterial.Get() -> SurfaceType, Hit);
	}
	
	Deactivate();
}