﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_ProjectileGrenade.h"

#include "NiagaraFunctionLibrary.h"
#include "TDS_BaseEffect.h"
#include "TDS_EffectsHandlerComponent.h"
#include "TDS_WeaponData.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
ATDS_ProjectileGrenade::ATDS_ProjectileGrenade()
{
	ATDS_ProjectileGrenade::SetupComponents();
}

void ATDS_ProjectileGrenade::SetupComponents()
{
	Super::SetupComponents();

	if (BoxComponent)
	{
		BoxComponent -> GetBodyInstance() -> bLockZTranslation = false;
		BoxComponent -> SetEnableGravity(true);
		BoxComponent -> OnComponentHit.AddDynamic(this, &ThisClass::OnHit);
	}
}

void ATDS_ProjectileGrenade::SetupProjectileParameters(const FWeaponParameters& WeaponParameters, const UTDS_WeaponData* WeaponData)
{
	Super::SetupProjectileParameters(WeaponParameters, WeaponData);
	
	MinDamage = WeaponParameters.GetMinDamage();
	DamageFallOff = WeaponParameters.GetDamageFallOff();
	DamageInnerRadius = WeaponParameters.GetDamageInnerRadius();
	DamageOuterRadius = WeaponParameters.GetDamageOuterRadius();
}

float ATDS_ProjectileGrenade::TrackProjectile()
{
	float Distance = Super::TrackProjectile();

	if (Distance >= MaxRange)
	{
		UGameplayStatics::ApplyRadialDamageWithFalloff(this, Damage, MinDamage, GetActorLocation(), DamageInnerRadius, DamageOuterRadius, DamageFallOff, nullptr, TArray<AActor*>(), GetOwner());

		FProjectileHitFX HitFX;
		OnGetProjectileHitFX.Broadcast(SurfaceType_Default ,HitFX);
	
		if (HitFX.GetHitSoundFX().IsValid())
		{
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitFX.GetHitSoundFX().Get(), GetActorLocation());
		}
		
		if (HitFX.GetHitVisualFX().IsValid())
		{
			UNiagaraFunctionLibrary::SpawnSystemAtLocation(GetWorld(), HitFX.GetHitVisualFX().Get(), GetActorLocation());
		}
		
		Deactivate();
	}

	return 0.f;
}

void ATDS_ProjectileGrenade::ApplyRadialEffect(AActor* OtherActor)
{
	TArray<FOverlapResult> OverlapResultsArr;
	FCollisionObjectQueryParams QueryParams;
	QueryParams.AddObjectTypesToQuery(ECC_WorldDynamic);
		
	if(GetWorld() -> OverlapMultiByObjectType(OverlapResultsArr, GetActorLocation(), FQuat::Identity, QueryParams, FCollisionShape::MakeSphere(DamageOuterRadius)))
	{
		TArray<TWeakObjectPtr<UTDS_EffectsHandlerComponent>> EffectsHandlerComponents;
		
		for (FOverlapResult HitResult : OverlapResultsArr)
		{
			if (HitResult.GetActor())
			{
				TWeakObjectPtr<UTDS_EffectsHandlerComponent> EffectsHandlerComponent = UTDS_EffectsHandlerComponent::GetEffectsHandlerComponent(HitResult.GetActor());

				if (EffectsHandlerComponent.IsValid())
				{
					UE_LOG(LogTemp, Warning, TEXT("ATDS_ProjectileGrenade::ApplyRadialEffect: Found %s"), *GetNameSafe(HitResult.GetActor()));
					EffectsHandlerComponents.AddUnique(EffectsHandlerComponent);
				}
			}
		}

		if (!EffectsHandlerComponents.IsEmpty())
		{
			for (TWeakObjectPtr<UTDS_EffectsHandlerComponent> EffectsHandlerComponent : EffectsHandlerComponents)
			{
				if (EffectsHandlerComponent.IsValid())
				{
					if (EffectClass)
					{
						UE_LOG(LogTemp, Warning, TEXT("ATDS_ProjectileGrenade::ApplyRadialEffect: Applying to %s"), *GetNameSafe(EffectsHandlerComponent.Get() -> GetOwner() ));
						const FEffectInfo* EffectInfoPtr = &EffectInfo;
						EffectsHandlerComponent -> AddEffect(GetOwner() -> GetOwner(), EffectInfoPtr, EffectClass);
					}
				}
			}
		}
	}
}

void ATDS_ProjectileGrenade::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor,
                                   UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!bIsHit)
	{
		//UE_LOG(LogTemp, Log, TEXT("%s got hit"), *OtherActor -> GetName())
		DrawDebugSphere(GetWorld(), Hit.Location, DamageInnerRadius, 10, FColor::Red, false, 10.f);
		DrawDebugSphere(GetWorld(), Hit.Location, DamageOuterRadius, 10, FColor::Orange, false, 10.f);

		UGameplayStatics::ApplyRadialDamageWithFalloff(this, Damage, MinDamage, GetActorLocation(), DamageInnerRadius, DamageOuterRadius, DamageFallOff, nullptr, TArray<AActor*>(), GetOwner() -> GetOwner());
		PlayProjectileFX(SurfaceType_Default, Hit);

		if (EffectClass)
		{
			ApplyRadialEffect(OtherActor);
		}
		
		Deactivate();
	}
}
